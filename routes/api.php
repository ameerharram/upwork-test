<?php
//api routes

Route::get('heartbeat', 'Controller@heartbeat');
Route::get('userheartbeat', 'UserController@heartbeat');

Route::post('signup', 'UserController@userSignUp');
Route::post('send_login_verification', 'UserController@sendLoginVerificationCode');
//Route::post('login_verify_code', 'UserController@provideLoginToken');
Route::post('verify_login_code', 'UserController@verifyLoginCode');
Route::delete('leave_swarm', 'SwarmController@leaveSwarm');
Route::get('email', 'Notification@sendEmail');
Route::get('event_invite', 'EventController@sendInvite');
Route::get('get_bg_color', 'BackgroundColorController@getNewBgColor');

// Route::post('notification', 'https://onesignal.com/api/v1/notifications');
// Route::post('create_phone_event', 'EventController@createEvent');


Route::get('activate_account', 'UserController@activateUserAccount');
Route::group(['middleware' => 'auth:api'], function () {

	//users
	Route::post('logout', 'UserController@logout');
	Route::post('block_user', 'UserController@blockAUser');
	Route::any('get_all_users', 'UserController@getAllUsers');
	Route::post('unblock_user', 'UserController@unBlockAUser');
	Route::get('get_my_profile', 'UserController@getMyProfile');
	Route::get('get_user_profile', 'UserController@getUserProfile');
	Route::post('update_player_id', 'UserController@updatePlayerId');
	Route::post('edit_user_profile', 'UserController@editUserProfile');
	Route::get('search_bizee_users', 'UserController@searchBizeeUsers');
	Route::get('get_blocked_users', 'UserController@getBlockedAccounts');
	Route::post('complete_profile', 'UserController@completeUserDetails');
	Route::post('upload_user_profile', 'UserController@uploadProfilePicture');
	Route::get('search_blocked_users', 'UserController@searchBlockedAccounts');


	//user contacts
	Route::post('add_contact', 'ContactController@addContact');
	Route::post('change_contact', 'ContactController@changeContact');
	Route::post('sync_user_contacts', 'ContactController@syncContacts');
	Route::get('get_user_contacts', 'ContactController@getUSerContacts');
	Route::get('search_phone_contact', 'ContactController@searchPhoneContacts');
	Route::get('get_user_bizee_contacts', 'ContactController@getUserBizeeContacts');
	Route::get('get_user_phone_contacts', 'ContactController@getUserPhoneContacts');

	//Favorites
	Route::get('all_favorites', 'FavouriteController@userFavorites');
	Route::post('favorites', 'FavouriteController@favorites');
	Route::delete('un_favorite', 'FavouriteController@unFavorite');
	
	//Swarms
	Route::get('get_swarm', 'SwarmController@getSwarms');
	Route::post('edit_swarm', 'SwarmController@editSwarm');
	Route::get('search_swarm', 'SwarmController@searchSwarm');
	Route::delete('leave_swarm', 'SwarmController@leaveSwarm');
	Route::post('create_swarm', 'SwarmController@createSwarm');
	Route::delete('leave_swarm', 'SwarmController@leaveSwarm');
	Route::get('get_favorites', 'SwarmController@getFavorites');
	Route::delete('delete_swarm', 'SwarmController@deleteSwarm');
	Route::get('get_swarm_owner', 'SwarmController@getSwarmOwner');
	Route::get('get_swarm_by_id', 'SwarmController@getSwarmsByid');
	Route::post('add_swarm_member', 'SwarmController@addSwarmMember');
	Route::get('get_swarm_members', 'SwarmController@getSwarmMembers');
	Route::post('remove_swarm_member', 'SwarmController@removeSwarmMember');

	//Hive
	Route::get('get_my_hive', 'HiveController@getMyHive');
	Route::get('create_hive', 'HiveController@createHive');
	Route::post('add_to_hive', 'HiveController@addToHive');
	Route::get('get_other_hive', 'HiveController@getOtherHive');
	Route::get('search_my_hive', 'HiveController@searchInMyHive');
	Route::post('send_hive_request', 'HiveController@sendRequest');
	Route::post('remove_from_hive', 'HiveController@removeFromHive');
	Route::get('search_other_hive', 'HiveController@searchInOtherHive');

	//Settings
	Route::post('set_settings', 'SettingsController@setSettings');
	Route::get('get_hive_settings', 'SettingsController@getSettings');
	Route::post('set_account_privacy', 'SettingsController@setAccountPrivacy');
	Route::post('set_notification_settings', 'SettingsController@setNotificationSettings');
	//Social
	Route::get('get_score', 'SocialHourController@suggestScore');
	Route::get('get_social_hours', 'SocialHourController@getSocialHours');
	Route::post('set_social_hours', 'SocialHourController@setSocialHours');
	//Events
	Route::get('get_event', 'EventController@getEvent');
	Route::post('send_invite', 'EventController@sendInvite');
	Route::post('add_comment', 'EventController@addComment');
	Route::get('search_event', 'EventController@searchEvent');
	Route::post('create_event', 'EventController@createEvent');
	Route::post('sync_calender', 'EventController@syncCalender');
	Route::get('get_user_event_guests', 'EventController@getGuests');
	Route::delete('delete_comment', 'EventController@deleteComment');
	Route::get('get_event_by_date', 'EventController@getEventByDate');
	Route::get('get_user_calender', 'EventController@getUserCalender');
	Route::get('get_bizee_events', 'EventController@getAllBizeeEvents');
	Route::get('get_phone_events', 'EventController@getAllPhoneEvents');
	Route::get('get_upcoming_event', 'EventController@getUpcomingEvent');
	Route::get('get_event_response', 'EventController@getEventResponse');
	Route::post('set_event_response', 'EventController@setEventResponse');
	Route::get('check_availability', 'EventController@checkAvailability');
	Route::post('add_member_to_event', 'EventController@addMemberToEvent');
	Route::get('get_mutual_events', 'UserController@getMutualEventsWithAnotherUser');
	Route::delete('remove_member_from_event', 'EventController@removeMemberFromEvent');
	Route::get('get_mutual_event_with_swarm', 'SwarmController@getMutualEventWithSwarm');
	//notifications
	Route::get('get_all_notification', 'NotificationController@getAllNotifications');
});

//admin routes
Route::get('/slugGet/get/{testSlug}', [
	'as' => 'admin.cms', 'uses' => 'Admin\CmsController@getPageSlug',
]);

Route::group(['prefix' => 'auth'], function () {
	Route::post('login', 'AuthController@authenticate')->name('login');
	Route::post('forgot', 'AuthController@forgotPassword');
	Route::post('verify', 'AuthController@verify');
	Route::post('reset', 'AuthController@reset');
	Route::post('register', 'AuthController@authenticate');
	Route::get('logout', 'AuthController@logout');
	Route::get('check', 'AuthController@check');
});

// session route
Route::post('phone-exist', 'UserController@phoneExist');
Route::post('phone-existId/{Id}',  'UserController@phoneExistWithId');
Route::post('passwordcheck', 'UserController@currentPassword');

// admin route
Route::group(['prefix' => 'admin', 'middleware' => ['api.auth']], function () {
	Route::group(['prefix' => 'adminprofile'], function () {
		Route::post('/resetPass', 'UserController@resetPassAdmin');
		Route::post('update', 'UserController@adminProfileUpdate');
		Route::post('updatepimage', 'Admin\UserController@adminProfileImage');
		Route::post('upload_user_profile', 'UserController@uploadProfilePicture');
	});


	Route::group(['prefix' => 'users'], function () {
		Route::get('/getUsers', 'UserController@allUsers');
		Route::delete('/{id}', 'UserController@destroyUser');
		Route::put('/{id}', 'UserController@block');
		Route::put('/unblock/{id}', 'UserController@unblock');
		Route::get('/edit/{id}', 'UserController@editUser');
		Route::post('/update', 'UserController@updateUser');
		Route::get('/userBucketList/{id}', 'UserController@userBucketList');
		Route::post('/updateBucketList', 'UserController@updateBucketList');
		Route::get('/userHive', 'HiveController@getMyHive');
		Route::post('remove_from_hive', 'HiveController@removeFromHive');
		Route::get('/usercountget', 'UserController@userCount');
		Route::get('/recent/get', 'UserController@recentUsers');
		Route::get('/graph', 'UserController@graph');
		Route::get('get_social_hours', 'SocialHourController@getSocialHours');
		Route::post('set_social_hours', 'SocialHourController@setSocialHours');
		Route::get('get_settings', 'SettingsController@getSettings');
		Route::post('set_settings', 'SettingsController@setSettings');
		Route::get('get_user_contacts', 'UserController@getUserContacts');
		Route::get('/getNonBizeeUsers', 'UserController@allNonBizeeUsers');

		// Route::get('user_favorites', 'UserController@userFavorites');
		// Route::delete('un_favorite', 'EventController@unFavorite');
	});

	Route::group(['prefix' => 'employees'], function () {
		Route::get('/employeecount', 'UserController@employeeCount');
		Route::get('/recent/get', 'UserController@recentEmployee');
		Route::get('/get', 'UserController@allEmployees');
		Route::get('/edit/{id}', 'UserController@EditGetEmployees');
		Route::post('/EmployeeUpdate', 'UserController@EmployeeUpdate');
		Route::post('/EmployeeAdd', 'UserController@addEmployee');
	});

	Route::group(['prefix' => 'hive'], function () {
		Route::get('/getAllHive', 'HiveController@getAllHive');
		Route::get('/getUsers', 'HiveController@notInHiveUsers');
		Route::post('add_to_hive', 'HiveController@sendRequest');
	});

	Route::group(['prefix' => 'swarm'], function () {
		Route::get('/swarmTotal', 'SwarmController@swarmTotal');
		Route::get('/swarmGraph', 'SwarmController@swarmGraph');
		Route::get('get_swarm', 'SwarmController@getSwarms');
		Route::get('get_swarm_members', 'SwarmController@getSwarmMembers');
		Route::delete('delete_swarm', 'SwarmController@deleteSwarm');
		Route::get('/getUsers', 'SwarmController@notInSwarmUsers');
		Route::post('add_swarm_member', 'SwarmController@addSwarmMember');
		Route::post('remove_swarm_member', 'SwarmController@removeSwarmMember');
		Route::get('get_all_swarm', 'SwarmController@getAllSwarms');
		Route::get('get_swarm_byid', 'SwarmController@getSwarmsByid');
	});

	Route::group(['prefix' => 'events'], function () {
		Route::get('get_bizee_events', 'EventController@getAllBizeeEvents');
		Route::get('get_phone_events', 'EventController@getAllPhoneEvents');
		Route::get('get_event', 'EventController@getEvent');
		Route::delete('remove_member_from_event', 'EventController@removeMemberFromEvent');
		Route::delete('delete_event', 'EventController@deleteEvent');
		Route::post('create_event', 'EventController@createEvent');
		Route::post('invitation_response', 'EventController@responseEventInvite');
	});

	Route::group(['prefix' => 'settings'], function () {
		Route::post('add_bg_color', 'BackgroundColorController@addNewBgColor');
		Route::get('get_bg_color', 'BackgroundColorController@getNewBgColor');
		Route::delete('delete_bg_color', 'BackgroundColorController@deleteNewBgColor');
	});
});
