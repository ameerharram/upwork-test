<?php

return [
    'user' => [
        'user_not_active' => 0,
        'user_Active' => 1,
        'email_verify'=>1,
        'user_is_bizee' => 1,
        'contact_synced' => 1,
        'contact_not_synced'=>0,
        'details_added'=> 1,
        'added_details_none'=>0
    ],
    'roles' => [
        'admin' => 1,
        'employee' => 2,
        'user'=>3,
    ],
    'swarm_roles' => [
        'owner' => 1,
        'member' => 2,
    ],
    'orders' => [
        'pending_status' => 1,
        'complete_status' => 2,
        'cancelled_status'=>3,
    ],
    'notifications' => [
        'read' => 1,
        'unread' => 2,
        'seen'=>3,
    ],
    'response' => [
        'ResponseCode_fail' => 201,
        'ResponseCode_success' => 200,
        'ResponseCode_non_authoritative_information' => 203,
        'ResponseCode_no_content' => 204,
        'ResponseCode_created' => 200,
        'ResponseCode_not_authenticated' => 401,
        'ResponseCode_bad_request' => 400,
        'ResponseCode_not_found' => 404,
        'ResponseCode_precondition_required'=>428,
    ],

    'RSVP'=>[
        'not_going' =>0, //dark_blue
        'going'=>1,      //yellow
        'may_be'=>2,     //light grey
        'no_response'=>3 //white
    ],
    'hive_request_statuses' => [
        'request_sent' => 0, 
        'accept' => 1,      
        'decline' => 2,     
     
    ],
    'notification_types'=>[
        'hive_request'=>0,
        'event_invite'=>1,
        'new_message'=>2,
        'event_cancel'=>3,
        'add_in_swarm'=>4
    ],
    'contact'=>[
     'is_phone'=>1,
     'is_bizee'=>1,
     'not_bizee' => 0
    ],
    'hive_user_types'=>[
        'is_active'=>1,
        'not_active'=>0
    ],
];