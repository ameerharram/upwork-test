<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Permissions extends Model
{
    protected $table = 'permissions';
    protected $fillable = [

        'Emp_id',
        'Read',
        'Write',
        'Delete',
    ];

    public function employeesNmaes()
    {

        return $this->hasMany(User::class, 'id', 'Emp_id');
    }
}
