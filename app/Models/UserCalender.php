<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCalender extends Model
{
    protected $table = 'user_calendars';

    protected $fillable = [
        'event_owner', 'events_type', 'event_id', 'is_swarm', 'swarm_id', 'duration', 'comments'
    ];
    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }
    public function eventbizee()
    {
        return $this->hasMany(UserEventBizee::class, 'id', 'event_id');
    }
    public function eventphone()
    {
        return $this->hasMany(UserEventPhone::class, 'id', 'event_id');
    }
    public function event()
    {
        return $this->hasMany(EventComment::class, 'event_id', 'event_id');
    }
    public function events()
    {
        return $this->hasMany(UserEvent::class, 'event_id', 'event_id')->select('id','guest_id','event_id','response_status','type');
    }
    public function EventOwner()
    {
        return $this->hasMany(User::class, 'id', 'event_owner');
    }
    public function EventDetail()
    {
        return $this->hasMany(UserEventBizee::class, 'id', 'event_id');
    }
    public function EventComment()
    {
        return $this->hasMany(EventComment::class, 'event_id', 'event_id')
        ->select('id','event_id','comment_from_user','comment','image');
    }
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'event_owner');
    }
}
