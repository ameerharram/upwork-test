<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavorites extends Model
{
    protected $table = 'user_favorites';

    protected $fillable = [
        'user_id',   'type_id', 'type'
    ];

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'type_id')->select('id', 'first_name', 'last_name', 'profile_picture');
    }
    public function swarms()
    {
        return $this->hasOne(Swarm::class,'id', 'type_id')->select('id','creator_id','title','about_us');
    }
    
}
