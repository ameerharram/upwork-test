<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEventBizee extends Model
{
    protected $table = 'user_events_bizee';

    protected $fillable = [
        'date', 'from_time', 'location', 'host_notes', 'to_time', 'title', 'deleted_at'
    ];
    public function bizee()
    {
        return $this->hasMany(User::class, 'id', 'creator_id');
    }
    public function event(){
        return $this->hasMany(UserEvent::class, 'event_id','id');
    }
    public function events(){
        return $this->hasMany(UserEvent::class, 'event_id','id')
        ->select('id','event_id','response_status');
       
    }
    
}
