<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\HiveMember;

class Hive extends Model
{

    protected $guarded = [];

    public function hive_owner()
    {
        return $this->hasOne(User::class, 'id', 'hive_owner');
    }
    public function my_hive()
    {
        return $this->hasOne(HiveMember::class, 'hive_id', 'id');
    }

    public function hiveMembers()
    {
        return $this->hasMany(HiveMember::class, 'hive_id', 'id')->select('id', 'hive_member', 'is_active', 'hive_id');;
    }
    public function hiveMemberForEvent()
    {
        return $this->hasMany(HiveMember::class, 'hive_id', 'id')
            ->select('id', 'hive_member', 'is_favorite', 'is_active', 'hive_id');
    }
    public function users()
    {
        return $this->hasMany(User::class, 'id', 'hive_owner');
    }
}
