<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Swarm;
class SwarmMembers extends Model
{
    protected $table = 'swarm_members';
    protected $fillable = [
        'swarm_id', 'user_id', 'role'
    ];
    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id')
        ->select('id', 'first_name', 'last_name', 'activation_code', 'is_activate', 'role_id', 'player_id', 'profile_picture', 'phone');
    }
    public function SwarmM()
    {
        return $this->belongsTo(Swarm::class, 'swarm_id', 'id');
    }
    public function users()
    {
        return $this->hasMany(User::class, 'id', 'creator_id');
    }
    public function swarmusers()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function media()
    {
        return $this->hasMany(SwarmMedia::class, 'swarm_id', 'swarm_id')
        ->select('id', 'swarm_id', 'background_color', 'foreground_color', 'profile_picture');
    }
    public function userswarms(){
        return $this->hasMany(Swarm::class,'id','swarm_id');
    }
    public function swarms(){
        return $this->hasMany(Swarm::class,'creator_id','user_id')->select('id','creator_id','title','about_us');
    }
    public function creator()
    {
        return $this->hasMany(Swarm::class, 'creator_id', 'user_id');
    }
    public function Swarm(){
        return $this->hasMany(Swarm::class, 'id', 'swarm_id')->select('id', 'creator_id', 'title', 'about_us');
    }
    public function members()
    {
        return $this->hasMany(SwarmMembers::class, 'swarm_id', 'id')->select('id','swarm_id', 'user_id', 'role');
    }
    public function userFavorites()
    {
        return $this->hasMany(UserFavorites::class, 'type_id', 'swarm_id')->where('type', 'swarm');
    }
}
