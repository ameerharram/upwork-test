<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SwarmMedia extends Model
{
    protected $table = 'swarm_media';
    protected $fillable = [
        'swarm_id', 'background_color', 'profile_picture'
    ];
}
