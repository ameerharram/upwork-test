<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocialHour extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function slot()
    {
    return $this->hasOne(SocialHourSlot::class, 'id', 'slot');
    }

}
