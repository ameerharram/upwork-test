<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackgroundColor extends Model
{
    protected $table = 'background_colors';

    protected $fillable = [
        'color_code'
    ];
}
