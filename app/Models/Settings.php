<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'user_settings';
    protected $fillable = ['user', 'in_hive_view_members', 'out_hive_view_members', 'out_hive_invite_event', 'out_hive_view_my_profile', 'out_hive_view_my_hive_members'];
    public function user()
    {
        return $this->hasOne(User::class,'id','user');
    }
}
