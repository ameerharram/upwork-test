<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    protected $table = 'user_events';

    protected $fillable = [
        'guest_id', 'event_id', 'response_status', 'type'
    ];
    public function guest(){

        return $this->hasOne(User::class, 'id','guest_id');
    }
    public function phoneguest(){

        return $this->hasMany(UserContact::class, 'id','guest_id');
    }
    public function event(){
        return $this->hasMany(UserEventBizee::class, 'id','event_id');
    }
    public function users($id){
        return $this->hasMany(UserEvent::class, 'guest_id', 'id');
    }
    public function hiveMembers(){
        return $this->hasMany(HiveMember::class, 'hive_member','guest_id');
    }
    public function guest_id()
    {
        return $this->hasOne(User::class, 'id', 'guest_id');
    }
    public function events()
    {
        return $this->hasMany(UserEvent::class, 'event_id', 'event_id')->select('id','guest_id','event_id','response_status','type');
    }
}
