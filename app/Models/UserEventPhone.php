<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEventPhone extends Model
{
    protected $table = 'user_events_phone';

    protected $fillable = [
        'date',  'from_time', 'to_time', 'location', 'host_notes', 'title', 'deleted_at'
    ];
}
