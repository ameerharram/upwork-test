<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventComment extends Model
{
    protected $table = 'event_comments';

    protected $fillable = [
        'comment_from_user', 'event_id', 'comment', 'image'
    ];
    public function commentfromuser(){
        return $this->hasMany(User::class, 'id','comment_from_user');
    }
}
