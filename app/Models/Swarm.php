<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\SwarmMembers;
use App\Models\User;
use App\Models\UserFavorites;
class Swarm extends Model
{
    protected $table = 'swarms';

    protected $fillable = [
        'creator_id', 'title', 'about_us'
    ];
    public function user(){
        return $this->belongsTo(User::class)->withTimeStamp();
    }
    public function memberUser()
    {
        return $this->hasMany(SwarmMembers::class, 'user_id', 'creator_id');
    }
 
    public function members()
    {
        return $this->hasMany(SwarmMembers::class, 'swarm_id', 'id')->select('id','swarm_id','user_id','role');
    }
    public function creator()
    {
        return $this->hasMany(User::class, 'id', 'creator_id');
    }
    public function swarmMedia()
    {
        return $this->hasMany(SwarmMedia::class, 'swarm_id', 'id');
    }
    public function swarmMembers()
    {
        return $this->hasMany(SwarmMembers::class, 'swarm_id', 'id')
            ->select('id', 'swarm_id', 'role', 'user_id');
    }
    public function swarmEvents()
    {
        return $this->hasMany(UserCalender::class, 'swarm_id', 'id');
    }
    public function media()
    {
        return $this->hasMany(SwarmMedia::class, 'swarm_id', 'id');
    }
    public function favourites()
    {
        return $this->hasOne(UserFavorites::class, 'type_id', 'id');
    }
    public function userFavorites()
    {
        return $this->hasMany(UserFavorites::class, 'type_id', 'id')->where('type', 'swarm');
    }
}
