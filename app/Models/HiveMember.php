<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Hive;
use App\Models\User;
use App\Models\UserFavorites;

class HiveMember extends Model
{

    protected $guarded = [];

    public function hive()
    {
        return $this->hasOne(Hive::class, 'id', 'hive_id');
    }
    public function hive_member()
    {
        return $this->hasOne(User::class, 'id', 'hive_member');
    }
    public function hiveMemberRanks()
    {
        return $this->hasMany(HiveMemberRank::class);
    }
    public function users()
    {
        return $this->hasOne(User::class,'id','hive_member');
    }
    public function hiveMembers()
    {
        return $this->hasMany(HiveMember::class, 'hive_member','guest_id');
    }
    public function favorites()
    {
        return $this->hasMany(UserFavorites::class,'type_id','hive_member')->select('id','type_id','user_id');
    }
    public function hive_owner()
    {
        return $this->hasOne(User::class, 'id', 'hive_owner');
    }
}
