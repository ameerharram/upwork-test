<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UsersBlocked extends Model
{
    protected $guarded = [];
    protected $table = 'users_blocked';

    public function to_user()
    {
        return $this->hasMany(User::class, 'id', 'to_user');
    }
    public function from_user()
    {
        return $this->hasOne(User::class, 'id', 'from_user');
    }
}
