<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\Models\Hive;
use App\Models\UsersBlocked;
use App\Models\Settings;
use App\Models\HiveMember;

class User extends Authenticatable
{

    use HasApiTokens,
        Notifiable;

    protected $table = 'users';
    // protected $appends = ['score'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_name', 'first_name', 'last_name', 'activation_code', 'is_activate', 'password', 'role_id', 'login_type', 'social_id', 'social_access_token', 'address', 'city',
        'profile_picture', 'social_profile_picture_url', 'is_blocked', 'biography', 'phone', 'recovery_code', 'player_id'
    ];
    // public function setScoreAttribute($score)
    // {
    //     return $this->attributes['score'] = $score;
    // }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at'
    ];

    public function hive()
    {
        return $this->hasOne(HiveMember::class,'hive_owner', 'id');
    }
    public function hive_members()
    {
        return $this->hasMany(HiveMember::class, 'hive_member', 'id')
            ->select('id', 'hive_member', 'is_active', 'hive_owner','is_sender');
    }
    public function userMedia()
    {
        return $this->hasOne(UserEvent::class, 'guest_id', 'id');
    }
    public function swarm()
    {
        return $this->hasMany(Swarm::class, 'user_id')->withTimeStamp();
    }
    public function SwarmMembers()
    {
        return $this->hasOne(SwarmMembers::class, 'user_id', 'id')->withTimeStamp();
    }
    public function contacts()
    {
        return $this->hasMany('App\Models\UserContact', 'my_user_id', 'id');
    }
    public function bucketItems()
    {
        return $this->hasMany('App\Models\BucketItem', 'user_id', 'id')
            ->select('id', 'user_id', 'value');
    }

    //    public function getswarm(){
    //        return $this->hasMany();
    //    }

    public function role()
    {
        return $this->hasOne('App\Models\Role');
    }

    public function comment()
    {
        return $this->hasMany(EventComment::class, 'comment_from_user', 'id');
    }
    public function from_user()
    {
        return $this->hasOne(UsersBlocked::class, 'from_user', 'id');
    }
    public function to_user()
    {
        return $this->hasMany(UsersBlocked::class, 'to_user', 'id');
    }
    public function settings()
    {
        return $this->hasOne(Settings::class, 'user', 'id');
    }
    public function favorites()
    {
        return $this->hasMany(UserFavorites::class, 'type_id', 'id');
    }
    public function notifications()
    {
        return $this->hasMany(UserNotification::class);
    }
}
