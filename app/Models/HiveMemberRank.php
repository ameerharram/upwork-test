<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiveMemberRank extends Model
{
    protected $table = 'hive_member_rank';
}
