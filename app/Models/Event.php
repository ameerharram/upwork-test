<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class)->withTimeStamp();
    }
    public function eventMembers()
    {
        return $this->hasMany(EventMember::class);
    }
}
