<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialHourSlot extends Model
{
    protected $table = "social_hours_slots";
    protected $guarded=[];
}
