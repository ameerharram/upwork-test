<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{

    protected $table = 'user_contacts';
    protected $fillable = ['my_user_id', 'if_contact_id', 'name', 'phone_number', 'is_bizee', 'is_phone', 'profile_picture'];
    protected $softDelete = true;

    public function my_user_id()
    {
        return $this->hasOne(User::class, 'id', 'my_user_id');
    }
    public function usermember()
    {
        return $this->hasMany(User::class, 'id', 'my_user_id')
            ->select('id');
    }
}
