<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Services\UserService;
use App\Models\User;

/*
*  A controller class of user which will perform User related process. 
*/
class UserController extends Controller
{
    public $user_service;

    public function __construct(UserService $user_service)
    {
        $this->user_service = $user_service;
    }


    /**
     * signup api will create new user for bizee app
     */
    public function userSignUp(UserRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();
        $created_user = $this->user_service->userSignUp($input);

        if ($created_user) {
            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.user_registered'), 'User', $created_user);
        } else {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.user_registered_fail'), 'User', new \stdClass());
        }
       
    }

    /*
    *  A logout api will end access to the bizee app until again login. 
    */
    public function logout(UserRequest $request)
    {
        $request->validated();
        $logout_user = $this->user_service->logOutUserProfile();

        if ($logout_user) {
            return responseMsg(\Config::get('constants.response.ResponseCode_success'), __('user.logout_success'), 'User', $logout_user);
        } else {
            return responseMsg(\Config::get('constants.response.ResponseCode_fail'), __('user.logout_none'), 'User', $logout_user);
        }
    }

    /**
     * this api is handling activating user account if user has done with signup
     */
    public function activateUserAccount(UserRequest $request)
    {
        $input = $request->all();
        $validated = $request->validated();
        $response_object = $this->user_service->activateUserAccount($input);

        return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
       
    }

    /**
     * edit user profile is handling updation of user's basic details with bucket list
     */
    public function editUserProfile(UserRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();
        $created_user = $this->user_service->editUserProfile($input);
        if ($created_user) {
            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.profile_update_success'), 'User', $created_user);
        } else {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.activate_account'), 'User', new \stdClass());
        }
    }

    /**
     * edit user profile is handling updation of user's basic details with bucket list
    */
    public function completeUserDetails(UserRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();
        $user = $this->user_service->completeUserDetails($input);
        if ($user) {
            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.details_added'), 'User', $user);
        } else {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.details_added_fail'), 'User', new \stdClass());
        }
    }

    /**
     * this api is handling sending login verification code in the process of user login
     */
    public function sendLoginVerificationCode(UserRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();

        $response_object = $this->user_service->sendLoginVerificationCode($input);
        return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
    }

    /**
     * this api is providing login token in the process of user login , if user has entered correct verification code
     */
    public function verifyLoginCode(UserRequest $request)
    {
        $validated = $request->validated();
        $input = $request->all();

        $response_object = $this->user_service->verifyLoginCode($input);
        return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
       
    }

    /**
     * this api handling getting other user's profile
     */
    public function getUserProfile(UserRequest $request)
    {
        $input = $request->validated();
        $response_object = $this->user_service->getUserProfile($input);

        return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
    }

    /**
     * this api handling getting user's profile
     */
    public function getMyProfile(UserRequest $request)
    {
        $input = $request->validated();
        $response_object = $this->user_service->getMyProfile($input);

        return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
        
    }

    /**
     * block user api will block a user against auth user
     */
    public function blockAUser(UserRequest $request)
    {

        
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->user_service->blockAUser($input);

            return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
        
    }

    /**
     * this api handling un blocking a user
    */
    public function unBlockAUser(UserRequest $request)
    {
      
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->user_service->unBlockAUser($input);
            return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
       
    }

    /**
     * this api is returning all user's blocked accounts
     */
    public function getBlockedAccounts(UserRequest $request)
    {
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->user_service->getBlockedAccounts($input);
            return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
    }

    /**
     * this api handling uploading profile picture
     */
    public function uploadProfilePicture(UserRequest $request)
    {
     
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->user_service->uploadProfilePicture($input);
            return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'],  $response_object['response_values']);
       
    }

    /**
     * api is returning all mutual events with given user id
     */
    public function getMutualEventsWithAnotherUser(UserRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();

            $mutual_events = $this->user_service->getMutualEventsWithAnotherUser($input);
            if ($mutual_events) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.get_mutual_events_success'), 'User', $mutual_events);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.get_mutual_events_fail'), 'User', new \stdClass());
            }
       
    }

    /**
     * api is returning all bizee users
     */
    public function getAllUsers(UserRequest $request)
    {
      
            $validated = $request->validated();
            $input = $request->all();

            $all_users = $this->user_service->getAllUsers($input);
            if ($all_users) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.get_all_users_success'), 'User', $all_users);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.get_all_users_fail'), 'User', new \stdClass());
            }
      
    }

    /**
     * this api is returning all searched blocked users 
    */
    public function searchBlockedAccounts(UserRequest $request)
    {
      
            $validated = $request->validated();
            $input = $request->all();

            $searched_users = $this->user_service->searchBlockedAccounts($input);
            if (count($searched_users) > 0) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.get_searched_users_success'), 'User', $searched_users);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.get_searched_users_fail'), 'User', new \stdClass());
            }
      
    }

    /**
     * api is returning searched bizee 
     * users by their name
     */
    public function searchBizeeUsers(UserRequest $request)
    {

            $validated = $request->validated();
            $input = $request->all();

            $searched_users = $this->user_service->searchBizeeUsers($input);
            if (count($searched_users) > 0) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.get_searched_users_success'), 'User', $searched_users);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.get_searched_users_fail'), 'User', new \stdClass());
            }
     
    }

    /**
     * api is updating player id
     */
    public function updatePlayerId(UserRequest $request)
    {
    
            $validated = $request->validated();
            $input = $request->all();
            $updated_player_user = $this->user_service->updatePlayerId($input);

            if (!empty($updated_player_user)) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.player_id_updated_success'), 'User', new \stdClass());
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.player_id_updated_fail'), 'User', new \stdClass());
            }
    
    }

    /* api is returning user's favourites 
     */
    // public function userFavorites(UserRequest $request)
    //     {
    //             $validated = $request->validated();
    //             $input = $request->all();

    //             $searched_users = $this->user_service->userFavorites($input);

    //             if (count($searched_users) > 0) {
    //                 return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.get_searched_users_success'), 'User', $searched_users);
    //             } else {
    //                 return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.get_searched_users_fail'), 'User', new \stdClass());
    //             }
    //     }

    /******** Admin *******/
    public function userCount()
    {
        return $this->user_service->userCount();
    }

    public function employeeCount()
    {
        return $this->user_service->employeeCount();
    }

    public function recentUsers()
    {
        return $this->user_service->recentUsers();
    }

    public function recentEmployee()
    {
        return $this->user_service->recentEmployee();
    }

    public function allUsers()
    {
        return $this->user_service->allUsers();
    }

    public function destroyUser($id)
    {
        return $this->user_service->destroyUser($id);
    }

    public function editUser($id)
    {
        return $this->user_service->editUser($id);
    }

    public function phoneExistWithId($id = null, Request $request)
    {
        return $this->user_service->phoneExistWithId($id, $request->phone);
    }

    public function phoneExist(Request $request)
    {
        return $this->user_service->phoneExist($request->phone);
    }

    public function updateUser(Request $request)
    {
        $req = $request->data;
        return $this->user_service->updateUser($req);
    }

    public function userBucketList($id)
    {
        return $this->user_service->userBucketList($id);
    }

    public function updateBucketList(Request $request)
    {
        $bucketList = $request->data;
        return $this->user_service->updateBucketList($bucketList);
    }

    public function block($id)
    {
        return $this->user_service->block($id);
    }

    public function unblock($id)
    {
        return $this->user_service->unblock($id);
    }

    public function allEmployees()
    {
        return $this->user_service->allEmployees();
    }

    public function EditGetEmployees($id)
    {
        return $this->user_service->EditGetEmployees($id);
    }

    public function EmployeeUpdate(Request $request)
    {
        $req = $request->all(['data']);
        return $this->user_service->EmployeeUpdate($req);
    }

    public function addEmployee(Request $request)
    {
        $req = $request->data;
        return $this->user_service->addEmployee($req);
    }

    public function graph()
    {
        return $this->user_service->graph();
    }

    public function currentPassword(Request $request)
    {
        $req = $request->all();
        return $this->user_service->currentPassword($req);
    }

    public function resetPassAdmin(Request $request)
    {
        $req = $request->all();
        return $this->user_service->resetPassAdmin($req);
    }

    public function adminProfileUpdate(Request $request)
    {
        $req = $request->all();
        return $this->user_service->adminprofileUpdate($req);
    }

    public function getUserContacts(Request $request)
    {
        $req = $request->all();
        return $this->user_service->getUserContacts($req);
    }
    
    public function allNonBizeeUsers()
    {
        return $this->user_service->allNonBizeeUsers();
    }
}
