<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Http\Services\EventService;
use Exception;
use Mockery\CountValidator\Exception as MockeryException;

class EventController extends Controller
{

    protected $service;

    public function __construct(EventService $service)
    {
        $this->service = $service;
    }
    /**
     * This funtion will create an new event 
     */
    public function createEvent(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();
            $data = $this->service->createEvent($input);
    
            if ($data) {
                $value = $this->service->checkCalender($data);           
            }
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_created'), 'event', true);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.event_failed'), 'event', new \stdClass());
            }
       
    }
    /**
     * This funtion will get list of events 
     */
    public function getUserCalender(EventRequest $request)
    {
      
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getUserCalender($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.event_details_none'), 'event', new \stdClass());
            }
      
    }
    /**
     * This funtion will get list of all bizee events 
     */
    public function getAllBizeeEvents(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getAllBizeeEvents($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            }
            else {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.content'), 'event', $data);
            }
      
    }
    /**
     * This funtion will get list of all phone events 
     */
    public function getAllPhoneEvents(EventRequest $request)
    {
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getAllPhoneEvents($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    /**
     * This funtion will get list of all events 
     */
    public function getEvent(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    public function getEventByDate(EventRequest $request)
    {
      
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getEventByDate($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
        
    }
    public function getUpcomingEvent(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getUpcomingEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       

    }
    /**
     * This funtion will adds member to event
     */
    public function addMemberToEvent(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->addMemberToEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.member_added'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       

    }
    /**
     * This funtion will remove member from event
     */
    public function removeMemberFromEvent(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->removeMemberFromEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.member_removed'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
      
    }
    /**
     * This funtion will remove event
     */
    public function deleteEvent(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->deleteEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.member_removed'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    /**
     * This funtion will allow member to comment on an event 
     */
    public function addComment(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->addComment($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.comment_added'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
        
    }
    /**
     * This funtion will delete comment 
     */
    public function deleteComment(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->deleteComment($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.delete_comment'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    public function getGuests(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getGuests();
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    /**
     * This will allow user to search for a event.
     */
    public function searchEvent(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->searchEvent($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.event_details'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
        
    }
    public function checkAvailability(EventRequest $request)
    {
        
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->checkAvailability($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    /**
     * This function will sync user calendars
     */
    public function syncCalender(EventRequest $request)
    {
       
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->syncCalender($input);
    
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.sync_event'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.sync_fail'), 'event', new \stdClass());
            }
   
    }
    /**
     * This function will send invites to the non bizee users
     */
    public function sendInvite(EventRequest $request)
    {
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->sendInvite($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
       
    }
    public function responseEventInvite(EventRequest $request)
    {
     
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->responseEventInvite($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
    }
    /**
     * This function will allow user to response on event
     */
    public function getEventResponse(EventRequest $request)
    {
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->getEventResponse($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
      
    }
    /**
     * This function will allow user to set their event arrival status
     */
    public function setEventResponse(EventRequest $request)
    {
     
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->service->setEventResponse($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
    }
    
}
