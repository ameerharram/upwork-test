<?php

namespace App\Http\Controllers;

use App\Http\Requests\SocialHourRequest;
use App\Http\Services\SocialHourService;
class SocialHourController extends Controller
{

    protected $social_service;
    
    public function __construct(SocialHourService $social_service)
    {
        $this->social_service = $social_service;
    }
     
    public function setSocialHours(SocialHourRequest $request)
    {
       
           $validated = $request->validated();
        $input = $request->all();
        $social_hours = $this->social_service->setSocialHours($input);

        if ($social_hours) {
            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('social_hour.added_social_hour'), 'Social Hour', $social_hours);
        } else {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('social_hour.added_social_hour_failed'), 'Social Hour', new \stdClass());
        }
    
    }
      public function suggestScore(SocialHourRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->social_service->suggestScore($input);
            return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'],  $response_object['response_values']);
  
        }
    public function getSocialHours(SocialHourRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();
            $social_hours = $this->social_service->getSocialHours($input);

            if (count($social_hours) > 0) {

                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('social_hour.get_hours_success'), 'Social Hour', $social_hours);
            } else {

                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('social_hour.get_hours_fail'), 'Social Hour', new \stdClass());
            }
    
     }
}
