<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SettingsRequest;
use App\Http\Services\SettingService;

class SettingsController extends Controller
{
    //

    protected $setting_service;
    public function __construct(SettingService $setting_service)
    {
     $this->setting_service = $setting_service;
    }

    /**
     * set settings api will save user settings
     */
    public function setSettings(SettingsRequest $request)
    {
      
            $validated = $request->validated();
            $input = $request->all();
            $saved_settings = $this->setting_service->setSettings($input);

            if ($saved_settings) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('settings.setting_details'), 'settings', $saved_settings);
            } else {

                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('settings.setting_details_none'), 'settings', new \stdClass());
            }
    }
    public function setAccountPrivacy(SettingsRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();
            $saved_settings = $this->setting_service->setAccountPrivacy($input);

            if ($saved_settings) {

                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('settings.set_privacy_success'), 'settings', $saved_settings);
            } else {

                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('settings.set_privacy_fail'), 'settings', new \stdClass());
            }
  
       }
    public function getSettings(SettingsRequest $request)
    {
      
            $validated = $request->validated();
            $input = $request->all();
            $settings = $this->setting_service->getSettings($input);

            if ($settings) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('settings.setting_details'), 'settings', $settings);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('settings.setting_details_none'), 'settings', new \stdClass());
            }
    
      }
      public function setNotificationSettings(SettingsRequest $request)
      {
        $validated = $request->validated();
        $input = $request->all();
        $saved_settings = $this->setting_service->setNotificationSettings($input);

        if ($saved_settings) {

            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('settings.set_notif_setting_success'), 'notifictaion setting', new \stdClass());
        } else {

            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('settings.set_notif_setting_fail'), 'notifictaion setting', new \stdClass());
        }
      }
}
