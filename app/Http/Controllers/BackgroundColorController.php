<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\BackgroundColorService;


class BackgroundColorController extends Controller
{

    protected $service;

    public function __construct(BackgroundColorService $service)
    {
        $this->BGservice = $service;
    }
    /**
     * This funtion will create an new bg color 
     */
    public function addNewBgColor(Request $request)
    {
        $input = $request->all();
        return $this->BGservice->addNewBgColor($input);
    }
    
    /**
     * This funtion will get all bg color
     */
    public function getNewBgColor()
    {
        $bg = $this->BGservice->getNewBgColor();
        return responseMessage(\Config::get('constants.response.ResponseCode_success'),'', 'background', $bg);
    }

    /**
     * This funtion will delete a bg color 
     */
    public function deleteNewBgColor(Request $request)
    {
        $input = $request->all();
        return $this->BGservice->deleteNewBgColor($input);
    }
}
