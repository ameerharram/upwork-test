<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests\NotificationRequest;
use App\Http\Services\NotificationService;
use App\Repositories\NotificationRepository;


class NotificationController extends Controller
{
	protected $notificationRepo;
	protected $notificationService;
  
    function __construct(NotificationRepository $notificationsRepository,NotificationService $notificationService) {
     
		 $this->notificationRepo=$notificationsRepository;
		 $this->notificationService = $notificationService;
 	 
 	}
   /*
     * get user notifications
    */
   	public function getNotifications(){
   		
   		$userId=Auth::id();
   		$data=$this->notificationRepo->getAll($userId);

   		if($data){
		
			$ResponseCode = \Config::get('constants.response.ResponseCode_success');
			$ResponseMessage = __('notify.list');
			$param = 'Data';
			$values = $data;

		}else{
			$ResponseCode =\Config::get('constants.response.ResponseCode_fail');
			$ResponseMessage =  __('notify.fail');
			$param = 'Data';
			$values = new \stdClass();
		}
		return responseMsg($ResponseCode, $ResponseMessage, $param, $values);
   	}
/*
     * make notifications read
    */
    public function readNotifications(NotificationRequest $request){
   		
   		$input = $request->all();
        $validated = $request->validated();

        $data=$this->notificationRepo->makeRead($input['notification_id']);

   		if($data){
		
			$ResponseCode = \Config::get('constants.response.ResponseCode_success');
			$ResponseMessage = __('notify.updated');
			$param = 'Data';
			$values = $data;

		}else{
			$ResponseCode =\Config::get('constants.response.ResponseCode_fail');
			$ResponseMessage =  __('notify.fail');
			$param = 'Data';
			$values = new \stdClass();
		}
		return responseMsg($ResponseCode, $ResponseMessage, $param, $values);
   	}
   	/*
     * make notification seen,
    */
   	public function seenNotifications(NotificationRequest $request){

   		$data=$this->notificationRepo->makeSeen();

		$ResponseCode = \Config::get('constants.response.ResponseCode_success');
		$ResponseMessage = __('notify.updated');
		$param = 'Data';
		$values = new \stdClass();

		return responseMsg($ResponseCode, $ResponseMessage, $param, $values);
	   }
	   public function getAllNotifications(NotificationRequest $request)
	   {
		   $validated = $request->validated();
		   $input = $request->all();
		   $notifications = $this->notificationService->getAllNotifications($input);
		   if ($notifications){
			return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('notification.notifications_get'), 'notifications', $notifications);
		   }else{
			return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('notification.notifications_get_fail'), 'notifications',new \stdClass());
		   }
		}
}
