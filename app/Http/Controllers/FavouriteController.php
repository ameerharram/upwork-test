<?php

namespace App\Http\Controllers;

use App\Http\Requests\FavouriteRequest;
use App\Http\Services\FavouriteService;


class FavouriteController extends Controller
{

    protected $service;

    public function __construct(FavouriteService $service)
    {
        $this->service = $service;
    }

    /**
     * This function will adds data to favorites
     */
    public function favorites(FavouriteRequest $request)
    {
     
            $input     = $request->all();
            $data      = $this->service->favorites($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.favorites'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
      
    }
    /**
     * This funtion will unfav data 
     */
    public function unFavorite(FavouriteRequest $request)
    {
            $input     = $request->all();
            $data      = $this->service->unFavorite($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('event.unfav'), 'event', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('event.content'), 'event', new \stdClass());
            }
    }
    /**
     * This funtion will return user favourites
     */
    public function userFavorites(FavouriteRequest $request)
    {
            $input     = $request->all();
            $data      = $this->service->userFavorites($input);
            if ($data) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('favourite.user_favourites'), 'favorites', $data);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('favourite.user_favourites_fail'), 'favorites', new \stdClass());
            }
    }
}
