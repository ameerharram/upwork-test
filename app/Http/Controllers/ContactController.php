<?php

namespace App\Http\Controllers;
use App\Http\Requests\ContactRequest;
use App\Http\Services\ContactService;

class ContactController extends Controller
{
    protected $contact_service;

    public function __construct(ContactService $contact_service)
    {
        $this->contact_service = $contact_service;
    }
    /*
*  A sync api will will sync all phone contacts. 
*/
    public function syncContacts(ContactRequest $request)
    {
        
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->contact_service->syncContacts($input);
            return responseMessage($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
 
     }
    /*
*  A get user function will will get user contacts. 
*/
    public function getUserContacts(ContactRequest $request)
    {
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->contact_service->getUserContacts($input);
            return responseMessage($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
    
     }
    /*
*  A get user phone function will will get user's phone contacts. 
*/
    public function getUserPhoneContacts(ContactRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->contact_service->getUserPhoneContacts($input);
            return responseMessage($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
     
     }
    /*
*  A get user phone function will will get user's bizee contacts. 
*/
    public function getUserBizeeContacts(ContactRequest $request)
    {
       
            $validated = $request->validated();
            $input = $request->all();

            $response_object = $this->contact_service->getUserBizeeContacts($input);
            return responseMessage($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
     
    }
    
           /*
*  A get user phone function will implement search functionality in users contacts. 
*/
    public function searchPhoneContacts(ContactRequest $request)
    {
    
            $validated = $request->validated();
            $input = $request->all();

            $searched_contact = $this->contact_service->searchPhoneContacts($input);
            if (count($searched_contact) > 0) {
                return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('contact.searched_contact'), 'Contact', $searched_contact);
            } else {
                return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('contact.searched_contact_failed'), 'Contact', new \stdClass());
            }  
       
    }
}
