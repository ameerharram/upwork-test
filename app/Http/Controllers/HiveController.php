<?php

namespace App\Http\Controllers;

use App\Http\Requests\HiveRequest;
use App\Http\Services\HiveService;

class HiveController extends Controller
{

  protected $hive_service;

  public function __construct(HiveService $hive_service)
  {

    $this->hive_service = $hive_service;
  }
  
  /**
   * add tohive api request 
   */
  public function sendRequest(HiveRequest $request)
  {
  
      $validated = $request->validated();
      $input = $request->all();
      $response_object = $this->hive_service->sendRequest($input);
      if ($response_object)
        return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('hive.hive_notify_success'), 'hive_notify_success', '');
      else
        return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('hive.hive_notify_fail'), 'hive_notify_fail', '');
   
  }
  /**
   * add tohive api being used to add hive members in hive 
   */
  public function addToHive(HiveRequest $request)
  {
  
      $validated = $request->validated();
      $input = $request->all();
      $response_object = $this->hive_service->addToHive($input);

      return responseMsg($response_object['response_code'], $response_object['response_message'],  $response_object['response_param'], $response_object['response_values']);
    
   
  }
  /**
   * remove from hive api being used to remove hive members from hive 
   */
  public function removeFromHive(HiveRequest $request)
  {
      $validated = $request->validated();
      $input = $request->all();
      $response_object = $this->hive_service->removeFromHive($input);

      return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
 
   }
  /**
   * get hive api being used to get hive data  
   */
  public function getOtherHive(HiveRequest $request)
  {
    
      $validated = $request->validated();
      $input = $request->all();
      $response_object = $this->hive_service->getOtherHive($input);
      
      return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
  
  }
  public function getMyHive(HiveRequest $request)
  {

    $validated = $request->validated();
    $input = $request->all();
    $response_object = $this->hive_service->getMyHive($input);
    if(!empty($response_object))
      return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('hive.get_hive_success'), 'User', $response_object);
    else
    return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('hive.get_hive_fail'), 'User', new \stdClass());
  }
 
  /**
   * get all hive api being used to get All hive data for admin 
   */
  public function getAllHive()
  {
    $response_object = $this->hive_service->getAllHive();

    return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
  }
  /**
   * get all hive api being used to get All hive data for admin 
   */
  public function notInHiveUsers(HiveRequest $request)
  {
    $input = $request->all();
    $response_object = $this->hive_service->notInHiveUsers($input);

    if (empty($response_object)) {
      return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.no_swarms_exist'), 'User', $response_object);
    }
    if ($response_object) {
      return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.your_swarms'), 'User', $response_object);
    } else {
      return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.account_inactive'), 'User', new \stdClass());
    }
  }
  public function searchInOtherHive(HiveRequest $request)
  {
    $validated = $request->validated();
    $input = $request->all();
    $response_object = $this->hive_service->searchInOtherHive($input);

    return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
  }
  public function searchInMyHive(HiveRequest $request)
  {
    $validated = $request->validated();
    $input = $request->all();
    $response_object = $this->hive_service->searchInMyHive($input);

    return responseMsg($response_object['response_code'], $response_object['response_message'], $response_object['response_param'], $response_object['response_values']);
  }
}
