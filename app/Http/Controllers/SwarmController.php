<?php

namespace App\Http\Controllers;

use App\Http\Requests\SwarmRequest;
use App\Http\Services\SwarmService;


class SwarmController extends Controller
{
  public $SService;

    public function __construct(SwarmService $ssservice)
    {
      $this->SService = $ssservice;
    }

    /**
     * This function will create swarm
     */
    public function createSwarm(SwarmRequest $request)
    {
     
          $data = "";
          if (isset($_FILES['profile_picture'])) {
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->SService->createSwarm($input);
          }
          if (isset($request['color'])) {
            $validated = $request->validated();
            $input     = $request->all();
            $data      = $this->SService->createSwarm($input);
          }
          if (empty($data)) {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.swarm_option'), 'swarm', $data);
          }
          if ($data) {
            return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.swarm_created'), 'swarm', $data);
          } else {
            return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.swarm_failed'), 'swarm', new \stdClass());
          }
       
    }
    /**
     * This will return swarm owner details
     */
    public function getSwarmOwner(SwarmRequest $request)
    {
    
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->getSwarmCreator($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.your_swarms'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.account_inactive'), 'swarm', new \stdClass());
        }
    
    }
    /**
     * This will return swarm details
     */
    public function getSwarms(SwarmRequest $request)
    {
     
        $input     = $request->all();
        $value     = $this->SService->getSwarms($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.your_swarms'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.swarm_not_found'), 'swarm', new \stdClass());
          }
      
    }
    public function getSwarmsByid(SwarmRequest $request)
    {
    
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->getSwarmsByid($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'User', $value);
        }
        if (count($value)>0) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.your_swarm'), 'User', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.account_inactive'), 'User', new \stdClass());
        }
      
    }
    /**
     * This function will adds as user to swarm
     */
    public function addSwarmMember(SwarmRequest $request)
    {
     
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->addSwarmMember($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.member_added'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.content'), 'swarm', new \stdClass());
        }
     
    }
    /**
     * This function will remove swarm member details
     */
    public function removeSwarmMember(SwarmRequest $request)
    {
    
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->removeSwarmMember($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.member_removed'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.content'), 'swarm', new \stdClass());
        }
     
    }
    /**
     * This function will delete swarm details
     */
    public function deleteSwarm(SwarmRequest $request)
    {    
   
        $input     = $request->all();
        $value     = $this->SService->deleteSwarm($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.swarm_deleted'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', new \stdClass());
        }
     
    }
    /**
     * This function will allow member to leave swarm 
     */
    public function leaveSwarm(SwarmRequest $request)
    {
     
        $input     = $request->all();
        $value     = $this->SService->leaveSwarm($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.member_leave'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.content'), 'swarm', new \stdClass());
        }
     
    }
    public function getSwarmMembers(SwarmRequest $request)
    {
     
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->getSwarmMembers($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.no_swarms_exist'), 'User', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.your_swarms'), 'User', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.account_inactive'), 'User', new \stdClass());
        }
     
    }
    public function notInSwarmUsers(SwarmRequest $request)
    {
   
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->notInSwarmUsers($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.no_swarms_exist'), 'User', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('user.your_swarms'), 'User', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('user.account_inactive'), 'User', new \stdClass());
        }
      
     
    }
    public function getUserSwarms(SwarmRequest $request)
    {
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->getUserSwarms($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.your_swarms'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.account_inactive'), 'swarm', new \stdClass());
        }
    }
    public function searchSwarm(SwarmRequest $request)
    {
     
        $input     = $request->all();
        $value     = $this->SService->searchSwarm($input);
        if (empty($value)) {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', $value);
        }
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.your_swarms'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.content'), 'swarm', new \stdClass());
        }      
     
    }
    /**
     * Get Mutaul events with swarm
     */
    public function getMutualEventWithSwarm(SwarmRequest $request)
    {
    
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->getMutualEventWithSwarm($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.your_swarms'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', new \stdClass());
        }
    }
    /**
     * Get All Swarms
     */
    public function getAllSwarms()
    {
     
        return $this->SService->getAllSwarms();
    
    }
    public function editSwarm(SwarmRequest $request)
    {
     
        $validated = $request->validated();
        $input     = $request->all();
        $value     = $this->SService->editSwarm($input);
        if (count($value)>0) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.swarm_edit'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', new \stdClass());
        }
     
    }
     /**
     * get favorties
     */
    public function getFavorites(SwarmRequest $request)
    {
        $input     = $request->all();
        $value     = $this->SService->getFavorites($input);
        if ($value) {
          return responseMessage(\Config::get('constants.response.ResponseCode_success'), __('swarm.'), 'swarm', $value);
        } else {
          return responseMessage(\Config::get('constants.response.ResponseCode_fail'), __('swarm.no_swarms_exist'), 'swarm', new \stdClass());
        }
    }

 
  public function swarmTotal()
    {
        return $this->SService->swarmTotal();
    }
  public function swarmGraph()
    {
        return $this->SService->swarmGraph();
    }
}
