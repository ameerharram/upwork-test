<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Carbon\Carbon;
class Notification extends Controller
{
    public function sendEmail()
    {
        // Mail::to('mail@appdividend.com')->send(new SendMailable());
        // dispatch(new SendEmailJob());

        // echo 'email sent';
        // echo 'email sent';
        // die('s');
        $emailJob = (new SendEmailJob())->delay(Carbon::now()->addSeconds(3));
        dispatch($emailJob);
     
        echo 'email sent';
    }
}
