<?php

namespace App\Http\Services;

use App\Repositories\BackgroundColorRepository;

class BackgroundColorService
{
    protected $repo;
    public function __construct(BackgroundColorRepository $repo)
    {
        $this->BGrepo = $repo;
    }
    /**
     * This funtion will create an new bg color 
     */
    public function addNewBgColor($input)
    {
        $created_event = $this->BGrepo->addNewBgColor($input);
        if ($created_event) {

            return $created_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will get all bg color
     */
    public function getNewBgColor()
    {
        $created_event = $this->BGrepo->getNewBgColor();
        if ($created_event) {

            return $created_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will delete a bg color  
     */
    public function deleteNewBgColor($input)
    {
        $created_event = $this->BGrepo->deleteNewBgColor($input);
        if ($created_event) {

            return $created_event;
        } else {
            return false;
        }
    }
   
}
