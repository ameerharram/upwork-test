<?php

namespace App\Http\Services;

use App\Repositories\HiveRepository;

class HiveService
{
    protected $hive_repo;
    public function __construct(HiveRepository $hive_repo)
    {
        $this->hive_repo = $hive_repo;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to send hive request. 
*/
    public function sendRequest($input)
    {

        return $response_object = $this->hive_repo->sendRequest($input);
    }
    /*
*  A service layer will get data from controller and passes it to the repository to create hive. 
*/
    public function createHive()
    {

        $response_object = $this->hive_repo->createHive();
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to add members to hive. 
*/
    public function addToHive($input)
    {

        $response_object = $this->hive_repo->addToHive($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to remove members from hive. 
*/
    public function removeFromHive($input)
    {

        $removed_hive_user = $this->hive_repo->removeFromHive($input);
        return $removed_hive_user;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to get  hive. 
*/
    public function getMyHive($input)
    {
        $response_object = $this->hive_repo->getMyHive($input);
        return $response_object;
    }

    public function getOtherHive($input)
    {
        $response_object = $this->hive_repo->getOtherHive($input);
        return $response_object;
    }

    /*
*  A service layer will get data from controller and passes it to the repository to get  hive. 
*/
    public function getAllHive()
    {
        $response_object = $this->hive_repo->getAllHive();
        return $response_object;
    }
    public function notInHiveUsers($input)
    {
        $response_object = $this->hive_repo->notInHiveUsers($input);
        return $response_object;
    }
    public function searchInOtherHive($input)
    {
        $response_object = $this->hive_repo->searchInOtherHive($input);
        return $response_object;
    }
    public function searchInMyHive($input)
    {
        $response_object = $this->hive_repo->searchInMyHive($input);
        return $response_object;
    }
}
