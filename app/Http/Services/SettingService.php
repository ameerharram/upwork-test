<?php

namespace App\Http\Services;

use App\Repositories\SettingRepository;

class SettingService
{
    protected $setting_repo;
    public function __construct(SettingRepository $setting_repo)
    {
        $this->setting_repo = $setting_repo;
    }
  
    /*
*  A service layer will get data from controller and passes it to the repository to assign hive rank. 
*/
    public function setSettings($input)
    {
        $response_object = $this->setting_repo->setSettings($input);
        return $response_object;
    }
    public function setAccountPrivacy($input)
    {
        $response_object = $this->setting_repo->setAccountPrivacy($input);
        return $response_object;
    }
    public function getSettings($input)
    {
        $response_object = $this->setting_repo->getSettings($input);
        return $response_object;
    }
    public function setNotificationSettings($input)
    {
        $response_object = $this->setting_repo->setNotificationSettings($input);
        return $response_object;
    }
}
