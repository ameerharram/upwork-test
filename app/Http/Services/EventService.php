<?php

namespace App\Http\Services;

use App\Repositories\EventRepository;

class EventService
{
    protected $repo;
    public function __construct(EventRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * This funtion will create an new event 
     */
    public function createEvent($input)
    {
        $created_event = $this->repo->createEvent($input);
        if ($created_event) {

            return $created_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will add events. 
     */
    public function checkCalender($input)
    {
        $created_event = $this->repo->checkCalender($input);
        if ($created_event) {

            return $created_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will return all events with bizee. 
     */
    public function getAllBizeeEvents($input)
    {
        $get_event = $this->repo->getAllBizeeEvents($input);
        if ($get_event) {

            return $get_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will return all events. 
     */
    public function getUserCalender($input)
    {
        $getEvent = $this->repo->getUserCalender($input);
        if ($getEvent) {
            return $getEvent;
        } else {
            return false;
        }
    }
    /**
     * This funtion will return all events with phone. 
     */
    public function getAllPhoneEvents($input)
    {
        $phone_event = $this->repo->getAllPhoneEvents($input);
        if ($phone_event) {

            return $phone_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will return all events. 
     */
    public function getEvent($input)
    {
        $event = $this->repo->getEvent($input);
        if ($event) {

            return $event;
        } else {
            return false;
        }
    }
    public function getEventByDate($input)
    {
        $event = $this->repo->getEventByDate($input);
        if ($event) {

            return $event;
        } else {
            return false;
        }
    }
    public function getUpcomingEvent($input)
    {
        $upcoming_event = $this->repo->getUpcomingEvent($input);
        if ($upcoming_event) {

            return $upcoming_event;
        } else {
            return false;
        }
    }
    /**
     * This funtion will add members to events with bizee. 
     */
    public function addMemberToEvent($input)
    {
        $remove_member = $this->repo->addMemberToEvent($input);
        if ($remove_member) {
            return $remove_member;
        } else {
            return false;
        }
    }
    /**
     * This funtion will remove member from events. 
     */
    public function removeMemberFromEvent($input)
    {
        $remove_member = $this->repo->removeMemberFromEvent($input);
        if ($remove_member) {
            return $remove_member;
        } else {
            return false;
        }
    }
    /**
     * This funtion will remove member from events. 
     */
    public function deleteEvent($input)
    {
        $remove_member = $this->repo->deleteEvent($input);
        if ($remove_member) {
            return $remove_member;
        } else {
            return false;
        }
    }
    /**
     * This funtion will allow member to comment on events. 
     */
    public function addComment($input)
    {
        $add_comment = $this->repo->addComment($input);
        if ($add_comment) {
            return $add_comment;
        } else {
            return false;
        }
    }
    /**
     * This funtion will allow to delete comment on events. 
     */
    public function deleteComment($input)
    {
        $delete_comment = $this->repo->deleteComment($input);
        if ($delete_comment) {
            return $delete_comment;
        } else {
            return false;
        }
    }

    public function getGuests()
    {
        $get_guest = $this->repo->getGuests();
        if ($get_guest) {
            return $get_guest;
        }
    }
    public function searchEvent($input)
    {
        $search_event = $this->repo->searchEvent($input);
        if ($search_event) {
            return $search_event;
        } else {
            return false;
        }
    }
    public function checkAvailability($input)
    {
        $Availability = $this->repo->checkAvailability($input);
        if ($Availability) {
            return $Availability;
        } else {
            return false;
        }
    }
    public function syncCalender($input)
    {
        $sync = $this->repo->syncCalender($input);
        if ($sync) {
            return $sync;
        } else {
            return false;
        }
    }
    public function sendInvite($input)
    {
        $invite = $this->repo->sendInvite($input);
        if ($invite) {
            return $invite;
        } else {
            return false;
        }
    }
    public function responseEventInvite($input)
    {
        $invite = $this->repo->responseEventInvite($input);
        if ($invite) {
            return $invite;
        } else {
            return false;
        }
    }
    public function getEventResponse($input)
    {
        $response = $this->repo->getEventResponse($input);
        if ($response) {
            return $response;
        } else {
            return false;
        }
    }
    public function setEventResponse($input)
    {
        $response = $this->repo->setEventResponse($input);
        if ($response) {
            return $response;
        } else {
            return false;
        }
    }
}
