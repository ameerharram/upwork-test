<?php

namespace App\Http\Services;

use App\Repositories\SwarmRepository;

class SwarmService
{
    protected $repo;
    public function __construct(SwarmRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * A create swarm method will check if data exist or not.
     */
    public function createSwarm($input)
    {
        $createdUser = $this->repo->createSwarm($input);
        if ($createdUser) {

            return $createdUser;
        } else {
            return false;
        }
    }
    /**
     * A get swarm creator fuction will return swarm creator details if exist.
     */
    public function getSwarmCreator($input)
    {
        $swarm_users = $this->repo->getSwarmCreator($input);
        if ($swarm_users) {
            return $swarm_users;
        } else {
            return false;
        }
    }
    /**
     * A get swarm fuction will return swarm details if exist.
     */
    public function getSwarms($input)
    {
        $swarms     = $this->repo->getSwarms($input);
        if ($swarms) {
            return $swarms;
        } else {
            return false;
        }
    }
    public function getSwarmsByid($input)
    {
        $swarms     = $this->repo->getSwarmsByid($input);
        if ($swarms) {
            return $swarms;
        } else {
            return false;
        }
    }
    /**
     * A add swarm member fuction will allow to add swarm member.
     */
    public function addSwarmMember($input)
    {
        $swarm_member = $this->repo->addSwarmMember($input);
        if ($swarm_member) {
            return $swarm_member;
        } else {
            return false;
        }
    }
    /**
     * A delete swarm member fuction will allow to delete swarm member.
     */
    public function removeSwarmMember($input)
    {
        $removeMember = $this->repo->removeSwarmMember($input);
        if ($removeMember) {
            return $removeMember;
        } else {
            return false;
        }
    }
    /**
     * A delete swarm fuction will allow to delete swarm.
     */
    public function deleteSwarm($input)
    {
        $deleteSwarm = $this->repo->deleteSwarm($input);
        if ($deleteSwarm) {
            return $deleteSwarm;
        } else {
            return false;
        }
    }
    /**
     * A leave swarm fuction will allow user to leave swarm.
     */
    public function leaveSwarm($input)
    {
        $leave = $this->repo->leaveSwarm($input);
        if ($leave) {
            return $leave;
        } else {
            return false;
        }
    }
    /**
     * A search swarm function will allow user to search swarm
     */
    public function searchSwarm($input){
        $searchSwarm = $this->repo->searchSwarm($input);
        if($searchSwarm){
            return $searchSwarm;
        }else{
            return false;
        }
    }
    /**
     * Get mutal event with swarm
     */
    public function getMutualEventWithSwarm($input){
        $event = $this->repo->getMutualEventWithSwarm($input);
        if($event){
            return $event;
        }else{
            return false;
        }
    }
    /**
     * edit swarm
     */
    public function editSwarm($input){
        $event = $this->repo->editSwarm($input);
        if($event){
            return $event;
        }else{
            return false;
        }
    }
    /**
     * get favorites
     */
    public function getFavorites($input){
        $favorites = $this->repo->getFavorites($input);
        if($favorites){
            return $favorites;
        }else{
            return false;
        }
    }
    public function getSwarmMembers($input)
    {
        return $this->repo->getSwarmMembers($input);
    }
    public function notInSwarmUsers($input)
    {
        return $this->repo->notInSwarmUsers($input);
    }
    public function getAllSwarms()
    {
    return $this->repo->getAllSwarms();
    }
    public function swarmTotal()
    {
        return $this->repo->swarmTotal();
    }
    public function swarmGraph()
    {
        return $this->repo->swarmGraph();
    }
}
