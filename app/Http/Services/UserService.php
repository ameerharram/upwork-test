<?php

namespace App\Http\Services;

use App\Repositories\UserRepository;

class UserService
{
    protected $user_repo;

    public function __construct(UserRepository $user_repo)
    {

        $this->user_repo = $user_repo;
    }
    /*
*  A service layer will get data from controller and passes it to the repository for further operations. 
*/
    public function userSignUp($input)
    {
        $created_user = $this->user_repo->userSignUp($input);
        // return $created_user;
        if ($created_user) {
            return $created_user;
        } else {
            return false;
        }
    }

    /*
*  A service layer will get data from controller and passes it to the repository to activate user. 
*/

    public function activateUserAccount($input)
    {

        $response_object = $this->user_repo->activateUserAccount($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to edit user. 
*/
    public function editUserProfile($input)
    {

        $response_object = $this->user_repo->editUserProfile($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to send verification code. 
*/
    public function sendLoginVerificationCode($input)
    {

        $response_object = $this->user_repo->sendLoginVerificationCode($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to block user. 
*/
    public function blockAUser($input)
    {

        $response_object = $this->user_repo->blockAUser($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to un block user. 
*/
    public function unBlockAUser($input)
    {

        $response_object = $this->user_repo->unBlockAUser($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to to provide login token. 
*/
    public function verifyLoginCode($input)
    {

        $response_object = $this->user_repo->verifyLoginCode($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to get blocked acccounts. 
*/
    public function getBlockedAccounts($input)
    {
        $response_object = $this->user_repo->getBlockedAccounts($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to upload profile picture. 
*/
    public function uploadProfilePicture($input)
    {
        $response_object = $this->user_repo->uploadProfilePicture($input);
        return $response_object;
    }
    public function completeUserDetails($input)
    {
        $response_object = $this->user_repo->completeUserDetails($input);
        return $response_object;
    }
    public function getMutualEventsWithAnotherUser($input)
    {
        $response_object = $this->user_repo->getMutualEventsWithAnotherUser($input);
        return $response_object;
    }
    public function getAllUsers($input)
    {
        $response_object = $this->user_repo->getAllUsers($input);
        return $response_object;
    }
    public function searchBlockedAccounts($input)
    {
        $response_object = $this->user_repo->searchBlockedAccounts($input);
        return $response_object;
    }
    public function searchBizeeUsers($input)
    {
        $response_object = $this->user_repo->searchBizeeUsers($input);
        return $response_object;
    }
    public function updatePlayerId($input)
    {
        $response_object = $this->user_repo->updatePlayerId($input);
        return $response_object;
    }
    // public function userFavorites($input)
    // {
    //     $response_object = $this->user_repo->userFavorites($input);
    //     return $response_object;
    // }
    /**
     * Admin Services
     */
    public function userCount()
    {
        return $this->user_repo->userCount();
    }

    public function employeeCount()
    {
        return $this->user_repo->employeeCount();
    }
    public function recentUsers()
    {
        return $this->user_repo->recentUsers();
    }

    public function recentEmployee()
    {
        return $this->user_repo->recentEmployee();
    }

    public function allUsers()
    {
        return $this->user_repo->allUsers();
    }
    public function destroyUser($id)
    {
        return $this->user_repo->destroyUser($id);
    }
    public function editUser($id)
    {
        return $this->user_repo->editUser($id);
    }
    public function phoneExistWithId($id, $phone)
    {
        return $this->user_repo->phoneExistWithId($id, $phone);
    }
    public function phoneExist($phone)
    {
        return $this->user_repo->phoneExist($phone);
    }
    public function updateUser($req)
    {
        return $this->user_repo->updateUser($req);
    }
    public function block($id)
    {
        return $this->user_repo->block($id);
    }
    public function unblock($id)
    {
        return $this->user_repo->unblock($id);
    }

    public function allEmployees()
    {
        return $this->user_repo->allEmployees();
    }
    public function EditGetEmployees($id)
    {
        return $this->user_repo->EditGetEmployees($id);
    }
    public function EmployeeUpdate($req)
    {
        return $this->user_repo->EmployeeUpdate($req);
    }
    public function addEmployee($req)
    {
        return $this->user_repo->addEmployee($req);
    }
    public function userBucketList($id)
    {
        return $this->user_repo->userBucketList($id);
    }
    public function updateBucketList($bucketList)
    {
        return $this->user_repo->updateBucketList($bucketList);
    }
    public function getUserProfile($input)
    {
        $response_object = $this->user_repo->getUserProfile($input);
        return $response_object;
    }
    public function getMyProfile()
    {
        $response_object = $this->user_repo->getMyProfile();
        return $response_object;
    }
    public function logOutUserProfile()
    {
        $logout_user = $this->user_repo->logOutUserProfile();
        return $logout_user;
    }
    public function graph()
    {
        return $this->user_repo->graph();
    }
    public function currentPassword($req)
   {
        return $this->user_repo->currentPassword($req);

   }

    public function resetPassAdmin($req)
    {
        return $this->user_repo->resetPassAdmin($req);
    }

    public function adminProfileUpdate($req)
    {
        return $this->user_repo->adminprofileUpdate($req);
    }
    public function getUserContacts($req)
    {
        return $this->user_repo->getUserContacts($req);
    }
    public function allNonBizeeUsers()
    {
        return $this->user_repo->allNonBizeeUsers();
    }
}
