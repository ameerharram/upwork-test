<?php

namespace App\Http\Services;

use App\Repositories\SocialHourRepository;

class SocialHourService
{
    protected $social_repo;

    public function __construct(SocialHourRepository $social_repo)
    {
        $this->social_repo = $social_repo;
    }
    public function setSocialHours($input)
    {

        $response_object = $this->social_repo->setSocialHours($input);
        return $response_object;

    }
    public function suggestScore($input)
    {

        $response_object = $this->social_repo->suggestScore($input);
        return $response_object;
    }
    public function getSocialHours($input)
    {

        $response_object = $this->social_repo->getSocialHours($input);
        return $response_object;
    }
}
