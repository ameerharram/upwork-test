<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\DB;
use App\Repositories\NotificationRepository;
use App\Jobs\SendNotification;
use App\Models\User;
use OneSignal;

class NotificationService
{
    protected $notification_repo;

    public function __construct(NotificationRepository $nitification_repo)
    {
        $this->notification_repo = $nitification_repo;
    }
    public function updateStatus($id, $status)
    {

        $notification = DB::table('user_notifications')->find($id);

        if(!$notification){
            return false;
        }
        if($notification->note_status != $status){
            DB::table('user_notifications')->where('id', $id)->update(['note_status' => $status]);
        }
        $notification->note_status = $status;
        return $notification;
    }

    static public function notify($user,$type = 'new_message')
    {   
        $ifUser = User::where('id', $user)->first();
        $data['content_available'] = true;
        $type = snake_case($type);
        
        $notify_type = '';
        switch ($type) {
            case "hive_request":
                $data['note_body'] = "Hive Request arrived";
                $notify_type = \Config::get('constants.notification_types.hive_request');
                break;
            case "event_invite":
                $data['note_body'] = "you are invited to an event Sent Successfully";
                $notify_type =  \Config::get('constants.notification_types.event_invite');
                break;
            case "new_message":
                $data['note_body'] = "there is new message";
                $notify_type = \Config::get('constants.notification_types.new_message');
                break;
            case "event_cancel":
                $data['note_body'] = "event is cancelled";
                $notify_type =  \Config::get('constants.notification_types.event_cancel');
                break;
            case "add_in_swarm":
        
                $data['note_body'] = "you have been added in swarm";
                $notify_type =  \Config::get('constants.notification_types.add_in_swarm');
                break;
            default:
                $data['note_body'] = "";
                $notify_type = '';
        }
            $data['from_user'] = $ifUser;
            $user_plyerId = $ifUser->player_id;
            if ($user_plyerId && $user_plyerId != '') {
            $response = \OneSignal::sendNotificationToUser(
                $data['note_body'],
                $user_plyerId,
                $url = null,
                $data = $data,
                $buttons = null,
                $schedule = null
            );
            $input=['user_id'=>$ifUser->id,'notify_header' => $type,'notify_body'=>$data['note_body'],'notify_status'=>\Config::get('constants.notifications.unread'), 'notify_type'=>$notify_type];
            NotificationRepository::createNew($input);
            return true;
            }else return false;
    }

    public function reMatchNotify($user, $data, $type = 'rematch_request', $fromUser,$is_unmatched,$is_request) {
    
        $data['type'] = snake_case($type);
        $data['from_id'] = $fromUser->id;
        $data['is_unmatched'] = $is_unmatched;
        $data['is_request'] = $is_request;
        // $plyer_idm='d1dc855d-2e46-4352-9b81-ae7461c4c51b';
        $plyer_idm=$user->plyer_id;
        
        if($plyer_idm !=''){
            return OneSignal::sendNotificationToUser(
                "Mingles: " . $fromUser->user_name . ' has requested with you ',
                $plyer_idm,
                $url = null,    
                $data = $data,
                $buttons = null,
                $schedule = null
 
            );
        }
    }
    public function getAllNotifications($input)
    {
        $response_object = $this->notification_repo->getAllNotifications($input);
        return $response_object;
    }

}
