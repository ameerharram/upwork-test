<?php

namespace App\Http\Services;

use App\Repositories\FavouriteRepository;

class FavouriteService
{
    protected $repo;
    public function __construct(FavouriteRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * This funtion will allow to make content favorites. 
     */
    public function favorites($input)
    {
        $favortires = $this->repo->favorites($input);
        if ($favortires) {
            return $favortires;
        } else {
            return false;
        }
    }
    /**
     * This funtion will allow to make content unfavorites. 
     */
    public function unFavorite($input)
    {
        $un_fav = $this->repo->unFavorite($input);
        if ($un_fav) {
            return $un_fav;
        } else {
            return false;
        }
    }
    public function userFavorites($input)
    {
        $un_fav = $this->repo->userFavorites($input);
        return $un_fav;
    }
}
