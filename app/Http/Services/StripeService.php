<?php

/**
 * Created by PhpStorm.
 * User: rehman
 * Date: 3/20/19
 * Time: 4:10 PM
 */

namespace App\Http\Services;

use Stripe\Plan;
use \Stripe\Stripe;

class StripeService
{
    private $apiKey;
    private $stripe;
    protected $plan;

    public function __construct()
    {
        $this->apiKey = config('services.stripe.secret');
        $this->stripe = new Stripe();
        $this->stripe->setApiKey($this->apiKey);
        $this->plan = new Plan();
    }

    public function allPlans()
    {
        try {

            return $this->plan->all();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function createPlan(array $data)
    {
        try {
            $response = $this->plan->create($data);
            logger($response);
            return ['status' => 1, 'response' => []];
        } catch (\Exception $exception) {
            return ['status' => 0, 'response' => $exception->getMessage()];
        }
    }

    public function updatePlan($stripe_id, array $data)
    {
        try {

            $response = $this->plan->update($stripe_id, $data);
            return ['status' => 1, 'response' => $response];
        } catch (\Exception $exception) {
            return ['status' => 0, 'response' => $exception->getMessage()];
        }
    }

    public function deletePlan($id)
    {
        try {

            $plan = $this->plan->retrieve($id);
            $product_id = $plan->product;
            $response =  $plan->delete();

            $product = \Stripe\Product::retrieve($product_id);
            $productRes = $product->delete();
            return ['status' => 1, 'response' => $productRes];;
        } catch (\Exception $exception) {
            return ['status' => 0, 'response' => $exception->getMessage()];
        }
    }
}
