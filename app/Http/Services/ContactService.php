<?php

namespace App\Http\Services;

use App\Repositories\ContactRepository;

class ContactService
{
    protected $contact_repo;

    public function __construct(ContactRepository $contact_repo)
    {
        $this->contact_repo = $contact_repo;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to get user's contacts. 
*/
    public function getUserContacts($input)
    {
        $response_object = $this->contact_repo->getUserContacts($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to get user phone contacts. 
*/
    public function getUserPhoneContacts($input)
    {
        $response_object = $this->contact_repo->getUserPhoneContacts($input);
        return $response_object;
    }
    /*
*  A service layer will get data from controller and passes it to the repository to user phone contacts. 
*/
    public function getUserBizeeContacts($input)
    {
        $response_object = $this->contact_repo->getUserBizeeContacts($input);
        return $response_object;
    }

    /*
*  A service layer will get data from controller and passes it to the repository to sync user phone contacts. 
*/
    public function syncContacts($input)
    {
       $response_object = $this->contact_repo->syncContacts($input);
        return $response_object;
    }
    public function addContact($input)
    {
        $added_contact = $this->contact_repo->addContact($input);
        return $added_contact;
    }
    public function changeContact($input)
    {
        $updated_contact = $this->contact_repo->changeContact($input);
        return $updated_contact;
    }
    public function searchPhoneContacts($input)
    {
        $searched_contact = $this->contact_repo->searchPhoneContacts($input);
        return $searched_contact;
    }
}
