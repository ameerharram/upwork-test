<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    /**
     * The user defined validation rules for api.
     * To make every single thing secure
     * To create a environment in which
     * everything work perfectly fine.
     */
    public function rules(Request $request)
    {
        $rules = [];

        if ($request->path() == 'api/create_event') {
            $rules = [
                'date'        => 'date_format:Y-m-d|required',
                'from_time'   => 'date_format:H:i|required',
                'to_time'     => 'date_format:H:i|required',
                'title'       => 'required',
                'location'    => 'required',
                'host_notes'  => 'required',

            ];
        }
        if ($request->path() == 'api/get_event') {
            $rules = [
                'event_id'    => 'required',
                'events_type' => 'required',
            ];
        }
        if ($request->path() == 'api/delete_comment') {
            $rules = [
                'event_id'   => 'required',
                'comment_id' => 'required',
            ];
        }

        if ($request->path() == 'api/add_member_to_event') {
            $rules = [
                'member_id'  => 'required',
                'event_id'   => 'required',
            ];
        }
        if ($request->path() == 'api/remove_member_from_event') {
            $rules = [
                'member_id'  => 'required',
                'event_id'   => 'required',
            ];
        }
        if ($request->path() == 'api/add_comment') {
            $rules = [
                'user_id'    => 'required',
                'event_id'   => 'required',
                'comment'    => 'required'
            ];
        }
        if ($request->path() == 'api/get_user_calender') {
            $rules = [
                'page'    => 'required',
                'limit'   => 'required',
            ];
        }
        if ($request->path() == 'api/send_invite') {
            $rules = [
                'event_id'    => 'required',
            ];
        }
        if ($request->path() == 'api/sync_calender') {
            $rules = [
                'event.*.date'       => 'required|date',
                'event.*.from_time'  => 'required|date_format:H:i:s',
                'event.*.to_time'    => 'required|date_format:H:i:s',
                'event.*.title'      => 'required|max:25',
                'event.*.location'   => 'required|max:100',
                'event.*.host_notes' => 'required|max:500',
                'event.*.duration' => 'required|'
            ];
        }
        if ($request->path() == 'api/search_event') {
            $rules = [
                'search'       => 'required',
            ];
        }


        return $rules;
    }
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
