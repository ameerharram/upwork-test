<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;

class UserRequest extends FormRequest
{
    protected $user_repo;
    // public function __construct(UserRepository $user_repo)
    // {
    //     $this->$user_repo = $user_repo;
    // }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    /*
*  A request layer will set the rules of validation. 
*/
    public function rules(Request $request)
    {
        $rules = [];
        if ($request->path() == 'api/signup') {
            $user = User::where('phone', $request['phone'])->first();

            if (!$user || $user->is_activate == \Config::get('constants.user.user_Active')) {

                $rules = [
                    'first_name' => 'required|alpha|regex:/^[a-zA-Z]+$/u|min:3|max:25',
                    'last_name' => 'required|alpha|regex:/^[a-zA-Z]+$/u|min:3|max:25',
                    'phone' =>  'required|unique:users|regex:/^([0-9\s\-\+\(\)]*)$/||between:8,12'
                ];
            } else if ($user->is_activate == \Config::get('constants.user.user_not_active')) {
                $rules = [
                    'first_name' => 'required|alpha|regex:/^[a-zA-Z]+$/u|min:3|max:10',
                    'last_name' => 'required|alpha|regex:/^[a-zA-Z]+$/u|min:3|max:10',
                    'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/||between:8,12'
                ];
            }
        }

        if ($request->path() == 'api/activate_account') {

            $rules = [

                'code' => 'required|numeric|min:6',
                'user_id' => 'required|numeric',
                'player_id' => 'required',
            ];
        }

        if ($request->path() == 'api/send_login_verification') {

            $rules = [
                'phone' => 'required|numeric|regex:/^([0-9\s\-\+\(\)]*)$/',

            ];
        }
        if ($request->path() == 'api/verify_login_code') {

            $rules = [
                'code' => 'required|min:6|numeric',
                'user_id' => 'required|numeric',
                'player_id' => 'required|string',
            ];
        }
        if ($request->path() == 'api/edit_user_profile') {

            $rules = [
                'full_name' => 'required|string|min:3',
                'profile_picture' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
                'bio' => 'required|max:500|string',
                'address' => 'required|min:3|string',
                'bucket_list' => 'required|array',
                "bucket_list.*" => 'required|string|max:50',
               
            ];
        }
        if ($request->path() == 'api/get_user_profile') {

            $rules = [
                'user_id' => 'required|numeric',
            ];
        }

        if ($request->path() == 'api/block_user') {

            $rules = [
                'to_user' => 'required|numeric',
            ];
        }

        if ($request->path() == 'api/unblock_user') {

            $rules = [
                'to_user' => 'required|numeric',
            ];
        }

        if ($request->path() == 'api/upload_user_profile') {

            $rules = [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ];
        }
        if ($request->path() == 'api/get_mutual_events') {

            $rules = [
                'user_id' => 'required|numeric',
                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/complete_profile') {

            $rules = [
                'biography' => 'required',
                'address' => 'required|string|max:100'
            ];
        }
        if ($request->path() == 'api/search_blocked_users') {

            $rules = [
                'search_term' => 'required'
            ];
        }
        if ($request->path() == 'api/search_bizee_users') {

            $rules = [
                'search_term' => 'required',
                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/get_all_users') {

            $rules = [

                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/update_player_id') {

            $rules = [

                'player_id' => 'required|string',

            ];
        }

        return $rules;
    }


    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
    public function messages()
    {
        return [
            'full_name.required' => 'Full Name is required',
            'full_name.min' => 'Full Name cant be less than 3 characters',
            'profile_picture.required' => 'Profile Picture is required',
            'profile_picture.image' => 'Profile Picture must be image',
            'bio.required' => 'Bio Field is required',
            'bio.max' => 'Bio Field cant be greater than 500 characters',
            'address.required' => 'Address is required',
            'address.min' => 'Address cant be less than 3 characters',
            'bucket_list.required' => 'Bucket list is required',
            'bucket_list.array' => 'Bucket list must be array',
            'bucket_list.*.required'=>'Bucket item(s) cant be empty',
            'bucket_list.*.max' => 'Bucket item(s) cant be greater than 50 characters',
            
            'first_name.required'=>'First Name is required',
            'last_name.required' => 'Last Name is required',
            'phone.required' => 'Phone is required',
            'code.required' => 'Code is required',
            'user_id.required' => 'First Name is required',
            'player_id.required' => 'Player id is required',
            'first_name.alpha' => 'First Name must be alphabetical',
            'last_name.alpha' => 'Last Name must be alphabetical',
            'first_name.regex' => 'First Name format is not correct',
            'last_name.regex' => 'Last Name format is not correct',
            'phone.regex' => 'Phone format is not correct',
            'phone.unique' => 'Phone must be unique',
            'code.numeric' => 'Code must be numeric',
            'code.min' => 'Code must be atleast 6 characters',
            'user_id.numeric' => 'User id must be numeric',
            'profile_picture.mimes' => 'Mimes should be one of these jpeg,png,jpg,gif,svg',
            
            
        ];
        
    }
}
