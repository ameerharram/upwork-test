<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class FavouriteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    /**
     * The user defined validation rules for api.
     * To make every single thing secure
     * To create a environment in which
     * everything work perfectly fine.
     */
    public function rules(Request $request)
    {
        $rules = [];

        if ($request->path() == 'api/favorites') {
            $rules = [
                'type'      => 'required',
                'type_id'   => 'required',
            ];
        }
        if ($request->path() == 'api/un_favorite') {
            $rules = [
                'type'      => 'required',
                'type_id'   => 'required',
            ];
        }
        if ($request->path() == 'api/all_favorites') {
            $rules = [
                'limit'      => 'required|numeric',
                'page'       => 'required|numeric',
            ];
        }

        return $rules;
    }
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
