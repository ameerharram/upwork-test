<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class SwarmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * 
     */

    /**
     * The user defined validation rules for api.
     * To make every single thing secure
     * To create a environment in which
     * everything works perfectly fine.
     */
    public function rules(Request $request)
    {
        $rules = [];

        if ($request->path() == 'api/create_swarm') {
            $rules = [
                'title'     => 'required',
                'about_us'  => 'required',
                'members.*'   => 'required',

            ];
        }
        if ($request->path() == 'api/get_swarm') {
            $rules = [
                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/add_swarm_member') {
            $rules = [
                'member'     => 'required',
                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/remove_swarm_member') {
            $rules = [
                'member'     => 'required',
                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/delete_swarm') {
            $rules = [

                'swarm_id'  => 'required',

            ];
        }
        if ($request->path() == 'api/leave_swarm') {
            $rules = [

                'swarm_id'  => 'required',

            ];
        }
        if ($request->path() == 'api/get_swarm_byid') {
            $rules = [

                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/search_swarm') {
            $rules = [
                'search'    => 'required',
                'limit'     => 'required|numeric',
                'page'      => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/get_mutual_event_with_swarm') {
            $rules = [

                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/get_swarm_by_id') {
            $rules = [

                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/get_swarm_members') {
            $rules = [

                'swarm_id'  => 'required',
            ];
        }
        if ($request->path() == 'api/edit_swarm') {
            $rules = [
                'title'     => 'required',
                'about_us'  => 'required',
                'members.*'   => 'required',
            ];
        }

        return $rules;
    }
    
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        throw new \Illuminate\Validation\ValidationException(response()->json($validator->errors(), 422));
    }


}
