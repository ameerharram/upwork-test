<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SocialHourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [];
        if ($request->path() == 'api/set_social_hours') {
            $rules = [

                'slot' => 'required|array',
                'slot.*.slot_id' => 'required|numeric',
                'slot.*.status' => 'required|boolean',
                'slot.*.day' => 'required|string',
            ];
        }
        if ($request->path() == 'api/suggest_score') {
            $rules = [

                'date' => 'required',
                'from_time' => 'required|date_format:H:i:s',
                'to_time' => 'required|date_format:H:i:s',
                'guests' => 'required|array',
            ];
        }
        return $rules;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
