<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(Request $request)
    {
        $rules = [];
        
        if ($request->path() == 'api/sync_user_contacts') {
            
            $rules = [
                'contact'=>'required|array',
                'contact.*.name'=> 'required|string|min:3|max:25|',
                'contact.*.phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|between:8,12',
                'contact.*.is_bizee' => 'required|boolean',
                'contact.*.is_phone' => 'required|boolean'
            ];
        }
       
        if ($request->path() == 'api/get_user_phone_contacts') {

            $rules = [
                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',
            ];
        }
        if ($request->path() == 'api/add_contact') {

            $rules = [
            
                'name'=>'required|alpha_dash',
                'phone_number' => 'required|numeric|',
                'profile_picture'=>'required',
                'is_bizee'=>'required|boolean',
                'is_phone'=>'required|boolean',
                
            ];
        }
        if ($request->path() == 'api/get_user_contacts') {

            $rules = [

                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',

            ];
        }
        if ($request->path() == 'api/change_contact') {

            $rules = [
                'phone' => 'required|numeric'
            ];
        }
        if ($request->path() == 'api/search_phone_contact') {

            $rules = [
                'search_term'=>'required|string',
                'limit'     => 'required|numeric',
                'page'  => 'required|numeric',
            ];
        }
        return $rules;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

}
