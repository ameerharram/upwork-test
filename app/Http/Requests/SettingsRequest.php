<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   $rules= [];


        if ($request->path() == 'api/set_settings') {
            $rules = [

                'in_hive_view_members' => 'required|boolean',
                'out_hive_view_members' => 'required|boolean',
                'out_hive_invite_event' => 'required|boolean',
                'out_hive_view_my_profile' => 'required|boolean',
                'out_hive_view_my_hive_members' => 'required|boolean',

            ];
        }

            if ($request->path() == 'api/set_account_privacy') {
                $rules = [

                    'account_privacy' => 'required|boolean',

                ];
            }
   
        if ($request->path() == 'api/set_notification_settings') {
            $rules = [

                'pause_all_notifications' => 'required|boolean',
                'pause_direct_message_notification' => 'required|boolean',
                'pause_hive_notification' => 'required|boolean',
                'pause_event_notification' => 'required|boolean',

            ];
        }
        return $rules;
    }
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
