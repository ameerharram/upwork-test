<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class HiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [];


        if ($request->path() == 'api/add_to_hive') {
            $rules = [

                'user_id' => 'required'
            ];
        }
        if ($request->path() == 'api/send_hive_request') {
            $rules = [

                'user_id' => 'required|array',
            ];
        }
        if ($request->path() == 'api/remove_from_hive') {

            $rules = [

                'user_id' => 'required'
            ];
        }
        if ($request->path() == 'api/get_my_hive') {

            $rules = [

                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ];
        }
        if ($request->path() == 'api/get_other_hive') {

            $rules = [

                'hive_owner' => 'required|numeric',
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ];
        }
        if ($request->path() == 'api/search_other_hive') {

            $rules = [

                'hive_owner' => 'required|numeric',
                'search_term' => 'required|string',
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ];
        }
        if ($request->path() == 'api/search_my_hive') {

            $rules = [

                'search_term' => 'required|string',
                'limit' => 'required|numeric',
                'page' => 'required|numeric'
            ];
        }
        return $rules;
    }
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {

        $message = $validator->errors()->first();
        $rescode = \Config::get('constants.response.ResponseCode_precondition_required');
        $param = 'Data';
        $values = new \stdClass();

        $response = new JsonResponse([
            'ResponseHeader' => [
                'ResponseCode' => $rescode,

            ],
            'ResponseBody' => [

                'ResponseMessage' =>  $message,
                $param =>  $values
            ]
        ], \Config::get('constants.response.ResponseCode_precondition_required'));

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
