<?php

namespace App\Repositories;

use App\Models\BackgroundColor;

//use Your Model

/**
 * Class EventRepository.
 */
class BackgroundColorRepository
{
    
    /**
     * This funtion will create an new bg color 
     */
    public function addNewBgColor($input)
    {
            return BackgroundColor::updateOrCreate([
                'color_code'       => '#'.$input['color_code']],[
                'color_code'  => '#'.$input['color_code']
            ]);


    }
    /**
     * This funtion will get all bg color
     */
    public function getNewBgColor()
    {
            return BackgroundColor::get(['id','color_code']);
    }
    /**
     * This funtion will delete a bg color 
     */
    public function deleteNewBgColor($input)
    {
            return BackgroundColor::where('id',$input['color_id'])->delete();
    }
    
}
