<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Settings;
use Auth;
/**
 * Class SettingRepository.
 */
class SettingRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Settings::class;
    }
    public function setSettings($input)
    {
        $user = Auth::user();
        $user_id = $user->id;
        if(array_key_exists('user_id',$input)){
            $user_id = $input['user_id'];
        }
        Settings::updateOrCreate(['user' => $user_id],
        [
            'in_hive_view_members' =>  $input['in_hive_view_members'],
            'out_hive_view_members' => $input['out_hive_view_members'],
            'out_hive_invite_event' => $input['out_hive_invite_event'],
            'out_hive_view_my_profile'=> $input['out_hive_view_my_profile'],
            'out_hive_view_my_hive_members' => $input['out_hive_view_my_hive_members']
        ]);
        $settings = Settings::with('user:id,first_name,last_name,biography,phone,address')->where('user',$user_id)
                  ->get(['id', 'user', 'in_hive_view_members', 'out_hive_view_members', 'out_hive_invite_event', 'out_hive_view_my_profile', 'out_hive_view_my_hive_members']);
        return $settings;
    }
    public function getSettings($input)
    {
        $user = Auth::id();
        // following code is For admin to Change a user's setting //
        if(array_key_exists('user_id',$input)){
            $user = $input['user_id'];
        }
       
        //end//
        $settings = Settings::where('user', $user)
                  ->first(['id', 'in_hive_view_members', 'out_hive_view_members', 'out_hive_invite_event',
                         'out_hive_view_my_profile', 'out_hive_view_my_hive_members']);
        
        return $settings;
    }
    
    public function setAccountPrivacy($input)
    {
        $user = Auth::user();
    
       if($input['account_privacy']==1){
        $account_privacy = Settings::where('user', Auth::id())
                ->update([
                    'account_privacy' => 1
         ]);
       }else{
            $account_privacy = Settings::where('user', Auth::id())
                ->update([
                    'account_privacy' => 0
                ]);
       }
        $settings = Settings::with('user:id,first_name,last_name,biography,phone,address')->where('user', Auth::id())
                    ->get(['id', 'user', 'account_privacy']);

        return $settings;
    }
    public function setNotificationSettings($input)
    {   
        
            $notification_settings = Settings::where('user', Auth::id())
                ->update(
            array(
                "pause_all_notifications" => $input['pause_all_notifications'],
                "pause_direct_message_notification" =>  $input['pause_all_notifications'],
                "pause_hive_notification" =>  $input['pause_all_notifications'],
                "pause_event_notification" =>  $input['pause_all_notifications'],
                )
                );
            if($notification_settings)
            return true;
            else return false;
    }
}
