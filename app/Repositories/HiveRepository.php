<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Hive;
use App\Models\User;
use App\Models\Settings;
use App\Models\HiveMember;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Jobs\SendNotification;

/**
 * hive repository to handle queries about hive
 */
class HiveRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Hive::class;
    }
    /**
     * create hive function being used to send hive requsest 
     */
    public function sendRequest($input)
    {
        
        $user = Auth::id();
        $members =  $input['user_id'];
        $request_sent=false;
        // following code is For admin to add single member to Hive //
        if (array_key_exists('is_admin', $input)) {
            return HiveMember::firstOrCreate(
                ['hive_member' => $input['user_id'], 'hive_owner' => $input['owner_id'],'is_active' => 1]
            );
        }
        //end//

        foreach ($members as $member) {
            $user_exist = User::find($member);
            if ($user_exist) {
                HiveMember::firstOrCreate([
                    'hive_member' =>  $member, 'hive_owner' => Auth::id(), 'is_active' => 0, 'is_sender' => 1
                ]);
                HiveMember::firstOrCreate(
                    ['hive_member' =>  Auth::id(), 'hive_owner' => $member, 'is_active' => 0,'is_sender' => 0]
                );
                $checkNotyResponce = SendNotification::dispatch($member, 'hive_request');
                $request_sent = true;
            } else return false;
        }
        if ($request_sent) {
            return true;
        } else return false;
    }

    /**
     * add to  hive function being used to add bizee/contact members to hive 
     */
    public function addToHive($input)
    {
                              
        if ($input['confirmation'] == 1) {
            $added_member = HiveMember::where(function ($query) use ($input) {
                $query->where('hive_member', Auth::id());
                $query->where('hive_owner', $input['user_id']);
            })->orwhere(function ($query) use ($input) {
                $query->where('hive_member', $input['user_id']);
                $query->where('hive_owner', Auth::id());
            })->update(['is_active' => 1]);
            
            if ($added_member) {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('hive.added_success'),
                    'response_param' => 'Hive',
                    'response_values' => new \stdClass()
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('hive.added_fail'),
                    'response_param' => 'Hive',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            HiveMember::where(function ($query) use ($input) {
                $query->where('hive_member', Auth::id());
                $query->where('hive_owner', $input['user_id']);
            })->orwhere(function ($query) use ($input) {
                $query->where('hive_member', $input['user_id']);
                $query->where('hive_owner', Auth::id());
            })->delete();
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.declined_users'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
    }


    /**
     * add to  hive function being used to remove bizee/contact members from hive 
     */
    public function removeFromHive($input)
    {
        $user = Auth::user();
        /// for admin //
        $owner_id = Auth::id();
        if (array_key_exists('owner_id', $input)) {
            $user = User::where('id', $input['owner_id'])->first();
            $owner_id = $input['owner_id'];
        }
        if ($user->hive) {
            $hive_member = HiveMember::where(function ($query) use ($input) {
                $query->where('hive_member', Auth::id());
                $query->where('hive_owner', $input['user_id']);
            })->orwhere(function ($query) use ($input) {
                $query->where('hive_member', $input['user_id']);
                $query->where('hive_owner', Auth::id());
            })->delete();
       
            if ($hive_member) {
                if ($hive_member) {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('hive.removed_success'),
                        'response_param' => 'Hive',
                        'response_values' => new \stdClass()
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('hive.empty_hive'),
                        'response_param' => 'Hive',
                        'response_values' => new \stdClass()
                    );
                }
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('hive.not_in_hive'),
                    'response_param' => 'Hive',
                    'response_values' => new \stdClass()
                );
            }
        } else
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.hive_not_exist'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
    }
    public function getMyHive($input)
    {
        $user_to_view = Auth::id();
        if (array_key_exists('is_admin', $input)) {
            $user_to_view = $input['user_id'];
        }
        if($user_to_view){
            return $hive_member = HiveMember::with('favorites', 'hive_member:id,first_name,phone,last_name,profile_picture')
            ->where('hive_owner', $user_to_view )->where('is_active', \Config::get('constants.hive_user_types.is_active'))
            ->take($input['limit'])->skip($input['page'])
            ->get(['id','hive_member']);
        }else{
            return false;
        }
    }
    /**
     * get hive function being used to get hive by id
     */
    public function getOtherHive($input)
    {
        $user = User::find($input['hive_owner']);
        $auth_user = Auth::user();
    
        $obj_array=[];
        //for admin
        if (array_key_exists('is_admin', $input)) {
            $authuser = $input['user_id'];
        }
        if (empty($user)) {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.get_hive_user_not_exists'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
        if ($user->hive) {
            $settings = Settings::where('user', $input['hive_owner'])->first();
            $hive_member = HiveMember::where('hive_member', $input['hive_owner'])->where('hive_owner',Auth::id())->first();
            if ($hive_member) {
                if ($settings->in_hive_view_members) {
                    $allow_to_view = true;
                } else {
                    $allow_to_view = false;
                }
            } else {
                if ($settings->out_hive_view_members) {
                    $allow_to_view = true;
                } else {
                    $allow_to_view = false;
                }
            }
            if ($allow_to_view) {
                $user_hive = HiveMember::with('hive_member:id,first_name,last_name,profile_picture')->where('hive_owner', $input['hive_owner']);
                if ($input['limit'] == -1 && $input['page'] == -1) {
                    $user_hive = $user_hive->get(['id', 'hive_member', 'is_active']);
                 
                } else {
                    $user_hive = $user_hive->take($input['limit'])->skip($input['page'])->get(['id','hive_member','is_active']);
                }
                //status
                foreach ($user_hive as $hive_member){
                    $obj = new \stdClass();
                    $obj = $hive_member;
                    $response = HiveMember::where('hive_owner', Auth::id())
                    ->where('hive_member', $hive_member->hive_member)
                   ->first();
                    if($response){
                        // $hive_member 
                        if($response->is_active){
                            $obj->status = 'in hive';
                        }else{
                            $obj->status = 'hive request sent';
                        }
                    }else{
                        $obj->status = 'not in hive';
                    }
                    array_push($obj_array, $obj);
                }
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('hive.get_hive_success'),
                    'response_param' => 'Hive',
                    'response_values' => $obj_array
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('hive.cant_get_hive'),
                    'response_param' => 'Hive',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.hive_not_exist'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
    }

    /**
     * will return hive members in other's hive based on search
     */
    public function searchInOtherHive($input)
    {
        $search_term =  $input['search_term'];
        $user_id =  $input['hive_owner'];
        $user = User::find($user_id);

        if ($user) {
            if ($user->hive) {
                
                $hive_mmebers = HiveMember::where('hive_owner',$user_id)
                    ->with('hive_member:id,first_name,last_name,profile_picture')
                    ->where('is_active', \Config::get('constants.hive_user_types.is_active'))
                    ->whereHas('hive_member',  function ($query) use ($search_term) {
                        $query->where('first_name', 'LIKE', "%{$search_term}%")
                            ->orWhere('last_name', 'LIKE', "%{$search_term}%");
                    })->with(
                        ['favorites' => function ($q) {
                            $q->where('type', 'hive');
                        }]
                    );
                if ($input['limit'] == -1 && $input['page'] == -1) {
                    $hive_mmebers = $hive_mmebers->get(['id', 'hive_member'])->toArray();
                } else {
                    $hive_mmebers = $hive_mmebers->take($input['limit'])->skip($input['page'])->get(['id', 'hive_member'])->toArray();
                }

                if (array_key_exists(0, $hive_mmebers)) {

                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('hive.search_result_found'),
                        'response_param' => 'Hive',
                        'response_values' => $hive_mmebers
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('hive.search_fail'),
                        'response_param' => 'Hive',
                        'response_values' => new \stdClass()
                    );
                }
            } else {

                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('hive.no_user_hive'),
                    'response_param' => 'Hive',
                    'response_values' => new \stdClass()
                );
            }
        } else {

            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.no_user_hive'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
    }


    /**
     * get all hive function being used to get hive all hives
     */
    public function getAllHive()
    {

        $all_hives = HiveMember::with('hive_owner')
         ->get()->groupBy('hive_owner');

        if ($all_hives) {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('hive.get_hive_success'),
                'response_param' => 'Hive',
                'response_values' => $all_hives
            );
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.get_hive_fail'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
    }
    /**
     * get Not in hive users
     */
    public function notInHiveUsers($input)
    {
        $queryString = Input::get('filter');
        $sortcol = Input::get('sortcol');
        $sort = Input::get('sort');

        if (empty($sortcol)) {
            $sortcol = 'first_name';
        }
        if (empty(Input::get('sort'))) {
            $sort = 'asc';
        }
        $notInHiveUsers = HiveMember::where('hive_owner', $input['owner_id'])->pluck('hive_member');
        $users = User::where('role_id', 3)->whereNotIn('id', $notInHiveUsers);
        if (Input::has('filter') && $queryString != '') {
            $users->where('first_name', 'LIKE', "%$queryString%");
            $users->orWhere('last_name', 'LIKE', "%$queryString%");
            $users->orWhere('phone', 'LIKE', "%$queryString%");
        }
        return $users->orderBy($sortcol, $sort)->get();
    }
    public function searchInMyHive($input)
    {
        $search_term =  $input['search_term'];
        $user = Auth::user();
       
            if ($user->hive) {
            $hive_owner=$user->hive->hive_owner;
                $hive_mmebers = HiveMember::where('hive_owner', $hive_owner)
                    ->with('hive_member:id,first_name,last_name,profile_picture')
                    ->where('is_active', \Config::get('constants.hive_user_types.is_active'))
                    ->whereHas('hive_member',  function ($query) use ($search_term) {
                        $query->where('first_name', 'LIKE', "%{$search_term}%")
                            ->orWhere('last_name', 'LIKE', "%{$search_term}%");
                    })->with(
                        ['favorites' => function ($q) {
                            $q->where('type', 'hive');
                        }]
                    );
                if ($input['limit'] == -1 && $input['page'] == -1) {
                    $hive_mmebers = $hive_mmebers->get(['id','hive_member'])->toArray();
                } else {
                    $hive_mmebers = $hive_mmebers->take($input['limit'])->skip($input['page'])->get(['id','hive_member'])->toArray();
                }
              
                if (array_key_exists(0, $hive_mmebers)) {

                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('hive.search_result_found'),
                        'response_param' => 'Hive',
                        'response_values' => $hive_mmebers
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('hive.search_fail'),
                        'response_param' => 'Hive',
                        'response_values' => new \stdClass()
                    );
                }
       
            } else {

            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('hive.no_user_hive'),
                'response_param' => 'Hive',
                'response_values' => new \stdClass()
            );
        }
        }
    }
