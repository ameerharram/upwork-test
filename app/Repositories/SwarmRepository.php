<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use App\Jobs\SendNotification;
use App\Models\UserEventBizee;
use App\Models\SwarmMembers;
use App\Models\UserCalender;
use App\Models\SwarmMedia;
use App\Models\Swarm;
use App\Models\User;
use App\Models\UserFavorites;
use Carbon\Carbon;
use DateTime;
use Auth;



/**
 * Class SwarmRepository.
 */
class SwarmRepository extends BaseRepository
{

    public function model()
    {
        return Swarm::class;
    }
    /**
     * Created swarm which will add info about swarm and add members.
     */
    public function createSwarm($input)
    {
        $user         = Auth::user();
        $userId      = Auth::id();
        $message      = "Swarm Created Successufully by " . $user->first_name;

        if ($user->is_activate == 1) {
            $swarm = Swarm::create([
                'creator_id' => $userId,
                'title' => $input['title'],
                'about_us' => $input['about_us'],
            ]);
            if ($swarm) {
                $val = $input['members'];
                $swarmId = $swarm->id;

                foreach ($val as $v) {
                    $swarmMember = SwarmMembers::create([
                        'swarm_id'  => $swarmId,
                        'user_id'   => $v,
                        'role'      => 2,
                    ]);
                    // $checkNotyResponce = SendNotification::dispatch($v, 'add_in_swarm');
                }
                $swarmMember = SwarmMembers::create([
                    'swarm_id'  => $swarmId,
                    'user_id'   => $userId,
                    'role'      => 1,

                ]);
            }
            /**
             * This will check swarm and swarm members and then add swarm media.
             */

            if (isset($input['color']) && $input['color'] != "") {

                $image_empty = "";
                $data = SwarmMedia::create([
                    'swarm_id'        => $swarmId,
                    'background_color' => $input['color'],
                    'profile_picture' => $image_empty
                ]);

                $allData = new \stdClass();
                $allData->Swarm  = $swarm;
                $allData->Swarm->swarm_member = $swarmMember;
                $allData->Swarm->swarm_media = $data;

                return $allData;
            } else {

                $emptyColor = "";
                $file       = $input['profile_picture'];
                $extension  = $input['profile_picture']->extension();
                $fileName   = time() . "-" . "swarm" . $userId . "." . $extension;
                $imageSaved = Storage::disk('media')->put($fileName, File::get($file));
                $data       = SwarmMedia::create([

                    'swarm_id'        => $swarmId,
                    'background_color' => $emptyColor,
                    'profile_picture' => $fileName
                ]);

                $allData                       = new \stdClass();
                $allData->Swarm                = $swarm;
                $allData->Swarm->swarm_member  = $swarmMember;
                $allData->Swarm->swarm_media   = $data;

                return true;
            }
        } else {
            return false;
        }
    }
    /**
     * This function will get details of swarms owner.
     */
    public function getSwarmCreator($input)
    {
        $user_id = Auth::id();
        $data = Swarm::select('id', 'creator_id', 'title', 'about_us')
            ->with(
                'creator:id,first_name,last_name,profile_picture',
                'members.swarmusers:id,first_name,last_name,profile_picture,phone'
            )
            ->where('creator_id', $user_id)->get();
        if ($data->isEmpty()) {
            return false;
        }
        return $data;
    }
    public function getSwarms($input)
    {
        $userId = Auth::id();
        // for admin
        if (array_key_exists('user_id', $input)) {
            $userId = $input['user_id'];
        }
        //for admin without pagination
        if (!array_key_exists('is_admin', $input)) {
            $data = SwarmMembers::select('id', 'swarm_id', 'user_id', 'role')
                ->where('user_id', $userId)
                ->with(
                    'Swarm.members.user:id,first_name,last_name,profile_picture',
                    'user:id,first_name,last_name,profile_picture',
                    'media:id,swarm_id,background_color,foreground_color,profile_picture'
                )
                ->take($input['limit'])->skip($input['page'])
                ->get();
        } else {
            $data = SwarmMembers::select('id', 'swarm_id', 'user_id', 'role')
                ->where('user_id', $userId)
                ->with(
                    'Swarm.members.user:id,first_name,last_name,profile_picture',
                    'user:id,first_name,last_name,profile_picture',
                    'media:id,swarm_id,background_color,foreground_color,profile_picture'
                )
                ->get();
        }


        if ($data->isEmpty()) {
            return false;
        }
        foreach ($data as $key => $value) {
            $value->load('userFavorites:id,user_id,type_id,type');
        }
        return $data;
    }
    public function getSwarmsByid($input)
    {
        //returs the object of swarm by id
        return Swarm::with(
            'swarmMedia:swarm_id,background_color,foreground_color,profile_picture',
            'creator:id,user_name,first_name,last_name,profile_picture',
            'swarmMembers.user:id,user_name,first_name,last_name,profile_picture',
            'swarmEvents.eventbizee:id,host_notes,cover_photo,title',
            'swarmMembers.user.hive_members'
        )->where('id', $input['swarm_id'])->select('id', 'creator_id', 'title', 'about_us')->get();
    }
    /**
     * The add swarm member will add new member to the existing swarms.
     */
    public function addSwarmMember($input)
    {
        $err          = 2;
        $user         = Auth::user();
        $user_id      = Auth::id();
        $find_member  = User::find($input['member']);
        $get_swarm    = Swarm::find($input['swarm_id']);
        $swarm_id     = SwarmMembers::where('swarm_id', $input['swarm_id'])->get();
        $checkuserid  = SwarmMembers::where('user_id', $input['member'])->where('swarm_id', $input['swarm_id'])->first();

        if ($checkuserid['user_id'] == $input['member']) {

            return false;
        }
        if ($find_member && count($swarm_id) > 0) {
            $message        = $find_member->first_name . " added to swarm " . $get_swarm->title;
            $swarm_member   = SwarmMembers::create([
                'swarm_id'  => $input['swarm_id'],
                'user_id'   => $input['member'],
                'role'      => 2,
            ]);
            if (array_key_exists('is_admin', $input)) {
                return $swarm_member;
            }
            $notification = NotificationService::notify($user, $type = 'Member Added', $message);
            return $swarm_member;
        }
        if (count($swarm_id) <= 0) {

            return false;
        } else {
            return false;
        }
    }
    /**
     * This function will remove a member from swarm.
     */
    public function removeSwarmMember($input)
    {
        $user_id      = Auth::id();
        $checkuserid  = SwarmMembers::where('user_id', $input['member'])->where('swarm_id', $input['swarm_id'])->delete();
        if ($checkuserid) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * This function will delete swarm.(only owner can delete it).
     */
    public function deleteSwarm($input)
    {
        $userId      = Auth::id(); //gets login user id
        $getSwarmById   = Swarm::where('id', $input['swarm_id'])->first(); //gets swarms details
        if ($getSwarmById) {
            $cretorId = $getSwarmById->creator_id;
            if (($userId == $cretorId) || $input['role_id'] == (2 || 1)) {
                $delete = Swarm::where('id', $input['swarm_id'])->delete();
                $delete = SwarmMembers::where('swarm_id', $input['swarm_id'])->delete();
                return true;
            }
        }
        return false;
    }
    /**
     * This function will allow a user to leave swarm.
     */
    public function leaveSwarm($input)
    {
        $userId         = Auth::id();
        $getSwarmById   = Swarm::where('id', $input['swarm_id'])->first();
        if (empty($getSwarmById)) {
            return false;
        }
        $creatorId      = $getSwarmById->creator_id;
        if ($creatorId != $userId) { //if creator id is not equal to user_id 
            $checkUserId  = SwarmMembers::where('user_id', $userId)->where('swarm_id', $input['swarm_id'])->delete();
            return $checkUserId;
        } else {
            return false;
        }
    }
    /**
     * A search swarm function will allow users to search swarms.
     * 
     */
    public function searchSwarm($input)
    {

        $userId     = Auth::id();
        $searchItem = $input['search'];

        $checkCredentials = SwarmMembers::select('id', 'swarm_id', 'user_id', 'role')
            ->where('user_id', $userId)
            ->with(
                'swarms.members.user:id,user_name,first_name,last_name,profile_picture',
                'user:id,user_name,first_name,last_name,profile_picture',
                'media:id,swarm_id,background_color,foreground_color,profile_picture'
            )
            ->whereHas('swarms', function ($query)  use ($searchItem) {
                $query->where('title', 'like', "%{$searchItem}%");
            })->take($input['limit'])->skip($input['page'])->get();

        if ($checkCredentials->isEmpty()) {
            return false;
        }
        foreach ($checkCredentials as $key => $value) {
            $value->load('userFavorites:id,user_id,type_id,type');
        }
        return $checkCredentials;
    }
    /**
     * Get Mutual event with swarm
     */
    public function getMutualEventWithSwarm($input)
    {
        $userId = Auth::id();
        $data    = UserCalender::where('is_swarm', 1)->where('swarm_id', $input['swarm_id'])->get();

        if ($data->isEmpty()) {
            return false;
        } else {
            $getSwarm = SwarmMembers::where('swarm_id', $input['swarm_id'])->where('user_id', $userId)->get();

            if ($getSwarm->isEmpty()) {
                return false;
            } else {
                $eventId  = $data[0]['event_id'];

                $getEvent = UserEventBizee::select('id', 'date', 'from_time', 'title', 'cover_photo')
                    ->where('id', $eventId)
                    ->with(['events' => function ($query)  use ($userId) {
                        $query->where('guest_id', $userId);
                    }])->get();
                return $getEvent;
            }
        }
    }


    /**
     * admin panel
     */
    public function getSwarmMembers($input)
    {
        $usersData = Swarm::select('id', 'creator_id', 'title', 'about_us')->with('members.swarmusers:id,user_name,first_name,last_name,profile_picture,biography')
            ->where('id', $input['swarm_id'])
            ->get();

        if ($usersData[0]->members->first() != null) {
            foreach ($usersData as $key => $value) {
                $value->load('userFavorites:id,user_id,type_id,type');
            }
            return $usersData;
        } else {
            return false;
        }
    }
    public function editSwarm($input)
    {
        $userId = Auth::id();
        //This will get swarm owner 
        $checkOwner = Swarm::where('id', $input['swarm_id'])->where('creator_id', $userId)->get();
        if ($checkOwner->isEmpty()) {
            return false;
        }
        //This checks if swarm members or swarms exists.
        $getSwarm = SwarmMembers::where('swarm_id', $input['swarm_id'])->get();
        if ($getSwarm->isEmpty()) {
            return false;
        }
        $swarm = Swarm::updateOrCreate(
            ['id' => $input['swarm_id']],
            ['creator_id' => $userId, 'title'      =>  $input['title'], 'about_us' => $input['about_us'],]

        );
        if ($swarm) {
            $val = $input['members'];
            $swarmId = $swarm->id;
            foreach ($val as $v) {
                $getUsers = SwarmMembers::where('swarm_id', $input['swarm_id'])->where('user_id', $v)->first();
                if (empty($getUsers)) {
                    $swarmMember = SwarmMembers::create([
                        'swarm_id'  => $swarmId,
                        'user_id'   => $v,
                        'role'      => \Config::get('constants.swarm_roles.member'),
                    ]);
                }
            }
            $swarmMember = SwarmMembers::updateOrCreate([
                'swarm_id'  => $swarmId,
                'user_id'   => $userId,
                'role'      => \Config::get('constants.swarm_roles.owner'),
            ]);
        }
        if (isset($input['color']) && $input['color'] != "") {

            $image_empty = "";
            $data = SwarmMedia::updateOrCreate([
                'swarm_id'         => $swarmId,
                'background_color' => $input['color'],
                'profile_picture'  => $image_empty
            ]);

            $allData = new \stdClass();
            $allData->Swarm  = $swarm;
            $allData->Swarm->swarm_member = $swarmMember;
            $allData->Swarm->swarm_media = $data;



            return $allData;
        } else {

            $emptyColor = "";
            $file       = $input['profile_picture'];
            $extension  = $input['profile_picture']->extension();
            $fileName   = time() . "-" . "swarm" . $userId . "." . $extension;
            $imageSaved = Storage::disk('media')->put($fileName, File::get($file));
            $data       = SwarmMedia::updateOrCreate([

                'swarm_id'         => $swarmId,
                'background_color' => $emptyColor,
                'profile_picture'  => $fileName
            ]);

            $allData                       = new \stdClass();
            $allData->Swarm                = $swarm;
            $allData->Swarm->swarm_member  = $swarmMember;
            $allData->Swarm->swarm_media   = $data;



            return true;
        }
    }
    /**
     * 
     */
    public function getFavorites($input)
    {
        $userId = Auth::id();

        $getUserFav   = UserFavorites::select('id', 'user_id', 'type_id', 'type')
            ->where('user_id', $userId)
            ->where('type', 'user')
            ->with('users:id,first_name,last_name,profile_picture')
            ->get();

        $getFavorites = UserFavorites::select('id', 'user_id', 'type_id', 'type')
            ->where('user_id', $userId)
            ->where('type', 'swarm')
            ->with(
                'users:id,first_name,last_name,profile_picture',
                'swarms.swarmMembers.user'
            )
            ->get();

        $allData         = new \stdClass();
        $allData->users  = $getUserFav;
        $allData->swarm  = $getFavorites;

        return $allData;
    }

    public function notInSwarmUsers($input)
    {
        $queryString = Input::get('filter');
        $sortcol = Input::get('sortcol');
        $sort = Input::get('sort');
        if (empty($sortcol)) {
            $sortcol = 'first_name';
        }
        if (empty(Input::get('sort'))) {
            $sort = 'asc';
        }
        $notInSwarmUsers = SwarmMembers::where('swarm_id', $input['swarm_id'])->pluck('user_id');
        $users = User::where('role_id', 3)->whereNotIn('id', $notInSwarmUsers);
        if (Input::has('filter') && $queryString != '') {
            $users->where('first_name', 'LIKE', "%$queryString%");
            $users->orWhere('last_name', 'LIKE', "%$queryString%");
            $users->orWhere('phone', 'LIKE', "%$queryString%");
        }
        return $users->orderBy($sortcol, $sort)->get();
    }
    /**
     * to get all swarms for admin panel listing
     */
    public function getAllSwarms()
    {

        $queryString = Input::get('filter');
        $sortcol = Input::get('sortcol');
        $sort = Input::get('sort');
        if (empty($sortcol) || $sortcol == 'undefined') {
            $sortcol = 'title';
        }
        if (empty($sort) || $sort == 'undefined') {
            $sort = 'asc';
        }
        return Swarm::with('creator', 'members')->orderBy($sortcol, $sort)->paginate(20);
    }
    public function swarmTotal()
    {
        return  Swarm::all()->count();
    }
    public function swarmGraph()
    {
        $today = Carbon::now();
        $files = Swarm::where('created_at', '>', $today->subDays(7))->get();
        $totalCount = $files->count();
        $response = array();
        $i = 0;
        while ($i < 7) {
            $counterperday = 0;
            $dayOfWeek = Carbon::now()->subDays($i);
            foreach ($files as $file) {
                $createDate = new DateTime($file['created_at']);
                $createDatedaysofweek = new DateTime($dayOfWeek);
                $singleday = $createDatedaysofweek->format('Y-m-d');
                $strip = $createDate->format('Y-m-d');
                if ($singleday == $strip) {
                    $counterperday = $counterperday + 1;
                }
            }
            $response[$i] = $counterperday;
            $i++;
        }
        $object = new \stdClass();
        $object->counterperday = $response;
        return response()->json($object);
    }
}
