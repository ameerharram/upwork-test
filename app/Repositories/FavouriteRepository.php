<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\UserFavorites;
use App\Models\UserCalender;
use App\Models\HiveMember;
use App\Models\Swarm;
use Auth;


/**
 * Class FavouriteRepository.
 */
class FavouriteRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return UserFavorites::class;
    }

    /**
     * This function will add events, swarms and contact to favorites.
     */
    public function favorites($input)
    {
        $userId = Auth::id();

        if ($input['type'] == 'user' && $input['type_id'] == $userId) {
            return false;
        }

        $checkFavId = UserFavorites::where('type', $input['type'])
            ->where('type_id', $input['type_id'])
            ->where('user_id', $userId)
            ->get();

        $swarmData = Swarm::find($input['type_id']);
        $eventData = UserCalender::where('event_id', $input['type_id'])->first();
        $hiveData  = HiveMember::where('hive_member', $input['type_id'])->first();
        /**
         * This function checks if both are empty or not
         */
        if (!$swarmData && !$eventData && !$hiveData) {
            return false;
        }
        /**
         * This function checks if data is already in favorites.
         */
        if (!count($checkFavId) == 0) {

            return false;
        }
        /**
         * This function adds data into favorites.
         */
        else {
            $favorites = UserFavorites::create([
                'user_id'       => $userId,
                'type_id'       => $input['type_id'],
                'type'          => $input['type']

            ]);
            return true;
        }
    }
    /**
     * This api will unfavorites the favorites id.
     */
    public function unFavorite($input)
    {
        $userId = Auth::id();
        $checkFavId = UserFavorites::where('type', $input['type'])
            ->where('type_id', $input['type_id'])
            ->where('user_id', $userId)
            ->delete();
            if( $checkFavId ) return true;
            else  return false;
    }
    public function userFavorites($input)
    {   
        $fav = [];
        $user_favorites = UserFavorites::where('user_id', Auth::id())->get();
        foreach ($user_favorites as $record) {
            if($record->type=='hive'){
                $record = $record->load('users');
            }else{
                $record = $record->load('swarms');
            }
            array_push($fav, $record);
        }
        if(count($user_favorites)>0)
        return $fav;
        else return false;
    }
}
