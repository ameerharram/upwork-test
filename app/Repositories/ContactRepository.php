<?php

namespace App\Repositories;

use App\Models\HiveMember;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\UserContact;
use App\Models\User;
use Auth;

/**
 * Class ContactRepository.
 */

class ContactRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return UserContact::class;
    }

    /*
     *  A sync function will sync all phone contacts tp bizee app. 
      */
    public function syncContacts($input)
    {   
        $user = Auth::user();
        $user_id = $user->id;
        $contacts = $input['contact'];
        $user_contacts = UserContact::where('my_user_id', $user_id)->get(['id', 'name']);

        $checkOwner = UserContact::where('my_user_id', $user_id)->get();
       
        if ($checkOwner) {
           foreach($checkOwner as $val){
               $val = UserContact::where('my_user_id', $val['my_user_id'])->delete();

           }
       }
        foreach ($contacts as $contact) {
            $inserted = false;
            $name = $contact['name'];
            $phone_number = $contact['phone_number'];
            $is_phone = $contact['is_phone'];
            $user = User::where('phone', $contact['phone_number']);
            if($user)
                $is_bizee = true;
            else
                $is_bizee = false;
            if ($is_bizee == true){
                UserContact::where('phone_number', $contact['phone_number'])->where('is_bizee', false)
                    ->update(['is_bizee' => true]);
            }
            $inserted = false;
            if($user->phone != $phone_number){
                UserContact::firstOrCreate([
                    'my_user_id' => $user_id,
                    'name' => $name,
                    'phone_number' => $phone_number,
                    'is_bizee' => $is_bizee,
                    'is_phone' => $is_phone
                ]);
            }
            $inserted = true;
        }
        if ($inserted) {
            $user->is_contact_sync = \Config::get('constants.user.contact_synced');
            $user->save();
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('contact.sync_success'),
                'response_param' => 'Contact',
                'response_values' =>  new \stdClass()
            );
        } else {
            UserContact::where('my_user_id', $user_id)->delete();
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('contact.sync_fail'),
                'response_param' => 'Contact',
                'response_values' =>  new \stdClass()
            );
        }
    }

    /*
*   this function will get user contacts. 
*/
    public function getUserContacts($input)
    {
        $user_id =Auth::id();
        $user = User::find($user_id);

        if ($user) {
            $all_user_contacts = UserContact::where('my_user_id', $user_id)->get(['id','name','phone_number']);
            if (count($all_user_contacts) > 0) {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('contact.get_contacts_success'),
                    'response_param' => 'Contact',
                    'response_values' =>  $all_user_contacts
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('contact.get_contacts_fail'),
                    'response_param' => 'Contact',
                    'response_values' =>  new \stdClass()
                );
            }
        } else {
               return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('contact.no_user_found'),
                'response_param' => 'Contact',
                'response_values' =>  new \stdClass()
            );
        }
    }
    /*
*  A get user phone function will get user's phone contacts. 
*/
    public function getUserPhoneContacts($input)
    {
        $user = Auth::user();
        if ($user) {
        $user_id = $user->id;

        $all_user_contacts = UserContact::select('id','if_contact_id','name','phone_number','is_bizee','is_phone')
        ->where([
            'my_user_id' => $user_id,
            'is_phone' => 1
        ])->take($input['limit'])->skip($input['page'])->get();
        
        //whether hive request has been sent or user in hive
            if($user->hive){
                foreach ($all_user_contacts as $user_contact) {
                    if ($user_contact['if_contact_id']) {
                        $hive_member = HiveMember::where('hive_owner', Auth::id())->where('hive_member', $user_contact['if_contact_id'])->first();
                        if ($hive_member) {
                            if ($hive_member->is_active) {
                                $user_contact->status = 'hive_member';
                            } else {
                                $user_contact->status = 'hive_request_sent';
                            }
                        } else $user_contact->status = 'send hive request';
                    } else $user_contact->status = 'not a bizee user';
                }
            } else {
                foreach ($all_user_contacts as $user_contact) {
                    if ($user_contact['if_contact_id']) {
                        $user_contact->status = 'send hive request';
                    } else $user_contact->status = 'not a bizee user';
                }
            }
            if (count($all_user_contacts) > 0) {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('contact.get_phone_success'),
                    'response_param' => 'Contact',
                    'response_values' =>  $all_user_contacts
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('contact.get_phone_fail'),
                    'response_param' => 'Contact',
                    'response_values' =>  new \stdClass()
                );
            }
        } else {
           return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('contact.no_user_found'),
                'response_param' => 'Contact',
                'response_values' =>  new \stdClass()
            );
        }
    }
    /*
*  this function will get user's bizee contacts. 
*/
    public function getUserBizeeContacts($input)
    {
        $user_id = Auth::id();
        $all_user_contacts = UserContact::where([
            'my_user_id' => $user_id,
            'is_bizee' => \Config::get('constants.contact.is_bizee')
        ])->get(['id','name']);
        $user = User::find($user_id);
        if ($user) {
            if (count($all_user_contacts)>0) {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('contact.get_bizee_success'),
                    'response_param' => 'Contact',
                    'response_values' =>  $all_user_contacts
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('contact.get_bizee_fail'),
                    'response_param' => 'Contact',
                    'response_values' =>  new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('contact.no_user_found'),
                'response_param' => 'Contact',
                'response_values' =>  new \stdClass()
            );
        }
    }

    public function addContact($input)
    {
        $if_contact_id = NULL;

        if($input['is_bizee']==1){
            $user_is_bizee = User::where('phone_number', $input['phone_number'])->first();
            if($user_is_bizee){
                $if_contact_id = $user_is_bizee->id;
            }
        }
        $usr_contact = UserContact::firstOrCreate([
            'my_user_id'=>Auth::user()->id,
            'if_contact_id'=> $if_contact_id,
            'name'=> $input['name'],
            'phone_number'=>$input['phone_number'],
            'is_bizee'=> $input['is_bizee'],
            'is_phone'=>$input['is_phone'],
            ]);
        if($usr_contact) return $usr_contact;
        else return false;
    }

    public function changeContact($input)
    {
     $user = Auth::user();
     $user->phone=  $input['phone'];
     $user->save();
     $user = User::where('phone', $user->phone)->get(['id','first_name','last_name','phone','biography','address']);
      return $user;
    }


    public function searchPhoneContacts($input)
    {
     $search_term = $input['search_term'];
     $user = Auth::user();

        $contacts = UserContact::select('id', 'name', 'phone_number', 'is_bizee', 'is_phone')->where('my_user_id', Auth::id())->where('name', 'LIKE', "%{$search_term}%");
        if ($input['limit'] == -1 && $input['page'] == -1)
        $contacts = $contacts->get();
        else
        $contacts = $contacts->take($input['limit'])->skip($input['page'])->get();
        if ($user->hive) {
    
            foreach ($contacts as $user_contact) {
                if ($user_contact['if_contact_id']) {
                    $hive_member = HiveMember::where('hive_owner', Auth::id())->where('hive_member', $user_contact['if_contact_id'])->first();
                    if ($hive_member) {
                        if ($hive_member->is_active) {
                            $user_contact->status = 'hive_member';
                        } else {
                            $user_contact->status = 'hive_request_sent';
                        }
                    } else $user_contact->status = 'send hive request';
                } else $user_contact->status = 'not a bizee user';
            }
        } else {
            foreach ($contacts as $user_contact) {
                if ($user_contact['if_contact_id']) {
                    $user_contact->status = 'send hive request';
                } else $user_contact->status = 'not a bizee user';
            }
        }
        return $contacts;
    }
}
