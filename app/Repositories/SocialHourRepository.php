<?php

namespace App\Repositories;

use App\Models\UserSocialHour;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Auth;
use DateTime;
use App\Models\User;
use App\Models\SocialHourSlot;

/**
 * Class SocialHourRepository.
 */
class SocialHourRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return UserSocialHour::class;
    }
    /**
     * function handling setting social hours 
     */
    public function setSocialHours($input)
    { // andling 0 status
        $user_slots = $input['slot'];
        $id = Auth::id();
        if(!empty($input['user_id'])){
            $id = $input['user_id'];
        }
        foreach ($user_slots as $slot) {
            if(empty($slot)){
                $inserted = true;
            }
            elseif(array_key_exists('todelete',$slot)) {
                $exist = UserSocialHour::where('user_id',$id)->where('slot',$slot['slot_id'])->where('day',$slot['day'])->delete();
            }
            else{
                $inserted = false;
                $slot_id = $slot['slot_id'];
                $day = $slot['day'];
    
                UserSocialHour::firstOrCreate([
    
                    'user_id' => $id,
                    'slot' => $slot_id,
                    'day' => $day
    
                ]);
                $inserted = true;
            }
        }
        if ($inserted) {

            $user_slots = UserSocialHour::with('user:id,first_name,last_name,address,biography')->where('user_id', $id)->get(['id','user_id','slot','day']);
            return true;
        } else {

            UserSocialHour::where('user_id', $id)->delete();
            return false;
        }
    }
    /**
     * function handling getting social hours 
     */
    public function getSocialHours($input)
    {   
        $day_slots=[];
        $user_id = Auth::id();
        if(array_key_exists("user_id",$input)){
            $user_id = $input['user_id'];       
        }
        $social_hours = UserSocialHour::with('slot:id,slot_name,slot_start_time,slot_end_time')->where('user_id', $user_id)->pluck('day')->toArray();
        $social_hours = array_unique($social_hours);
        foreach ($social_hours as $slot) {
            $obj = new \stdClass();
            $obj->day = $slot;
            $obj->slots = UserSocialHour::with('slot:id,slot_name,slot_start_time,slot_end_time')->where('user_id', $user_id)->where('day', $slot)->get(['slot'])->toArray();
            array_push($day_slots,$obj);
        }
        return  $day_slots;
    }

    public function getUserSocialHours($day, $guest_ids, $i)
    {
        $user_id = $guest_ids[$i];
        if (strcasecmp($day, 'Monday') == 0 || strcasecmp($day, 'Tuesday') == 0 || strcasecmp($day, 'Wednesday') == 0 || strcasecmp($day, 'Thursday') == 0) {
            $user_hours = UserSocialHour::where('user_id', $user_id)->where('day', 'Monday-Thursday')->orderBy('slot')->get()->toArray();
            return $user_hours;
        } else {

            $user_hours = UserSocialHour::where('day', $day)->where('user_id', $guest_ids[$i])->orderBy('slot')->get()->toArray();;
            return $user_hours;
        }
    }
    public function suggestScore($input)
    {
        $auth_user = Auth::user();
        $start_time = $input['from_time'];
        $event_start_time =  $start_time;
        $end_time = $input['to_time'];
        $guest_ids = $input['guests'];
        $scores = [];
        $user_with_score = [];
        $total_score = 0;
        $event_difference = ceil(((abs(strtotime($event_start_time) - strtotime($end_time))) / 60) / 60);
        $days = array("Monday", "Tuesday" , "Wednesday" , "Thursday" , "Friday" , "Saturday" , "Sunday");
        $insert = false;
        $current_slot = new \stdClass();
        $current_slot->start_time = $input['from_time'];
        $current_slot->end_time = $input['to_time'];

        $slots_to_suggest = array($current_slot);

        $not_hive_userObject=new \stdClass();
        $date = new DateTime($input['date']);
        $day = $date->format('l');

        /**
         * loop handling multiple users
         */
            //change user slot according to current event timings and calculate score in that slot  

            $time_to_compare = strtotime($slots_to_suggest[0]->start_time) + 1 * 60;            
            $time_to_compare = date('H:i:s', $time_to_compare);
            
            for ($i = 0; $i < count($guest_ids); $i++) {
                $available_hours = [];
                $no_of_hours_in_active_slot = 0;
                $score = 0;
                $user = User::find($guest_ids[$i]);

                if ($user) {

                    if (($auth_user->hive->hiveMembers && ($auth_user->hive->hiveMembers->contains('hive_member', $user->id)))) {
                        if ($user->is_calendar_sync) {
                            $user_hours = $this->getUserSocialHours($day, $guest_ids, $i);

                            if (count($user_hours) > 0) {
                                /**
                                 * social hours against user
                                 */
                                for ($j = 0; $j < count($user_hours); $j++) {
                                    
                                    $slot = $this->getSlot($user_hours, $j);
                                   
                                    $slot_start_time = $slot['slot_start_time'];
                                    $slot_end_time = $slot['slot_end_time'];
                               
                                    /**
                                     * calculating hours by available slots
                                     */

                                    if (strtotime($time_to_compare) > strtotime($slot_start_time) && strtotime($time_to_compare) < strtotime($slot_end_time))
                                         $start_time = $time_to_compare;
                                    else $start_time = date('H:i:s', strtotime($slot_start_time) + 1 * 60);
                             
                                    if (strtotime($time_to_compare) > strtotime($slot_start_time) && strtotime($time_to_compare) < strtotime($slot_end_time)) {
                                       if(strtotime($end_time) <=strtotime($slot_end_time)){
                                        while (strtotime($start_time) > strtotime($slot_start_time) && strtotime($start_time) < strtotime($end_time)) {

                                            if (date('H:i:s', strtotime($input['from_time']) + 1 * 60) >= date('H:i:s', strtotime($start_time)) && date('H:i:s', strtotime($input['from_time']) + 1 * 60) < date('H:i:s', strtotime($start_time) + 60 * 60))

                                                $insert = true;
                                            $insert == true ? $no_of_hours_in_active_slot += 1 : $no_of_hours_in_active_slot;
                                            $start_time = date('H:i:s', strtotime($start_time) + 60 * 60);
                                            $slots_to_suggest = $this->populateArray($slot, $slots_to_suggest);
                                        }
                                        break 1;
                                       }
                                       
                                    } else if (strtotime($time_to_compare) > strtotime($slot_start_time) && strtotime($time_to_compare) < strtotime($slot_end_time) && (strtotime($end_time) > strtotime($slot_end_time))) {

                                        while (strtotime($start_time) > strtotime($slot_start_time) && strtotime($start_time) < strtotime($slot_end_time)) {

                                        if (date('H:i:s', strtotime($input['from_time'])) >= date('H:i:s', strtotime($start_time)) && date('H:i:s', strtotime($input['from_time']) + 1 * 60) < date('H:i:s', strtotime($start_time) + 60 * 60))
                                            $insert = true;
                                            ($insert) ? $no_of_hours_in_active_slot += 1: $no_of_hours_in_active_slot;
                                            $start_time = date('H:i:s', strtotime($start_time) + 60 * 60);
                                            $slots_to_suggest = $this->populateArray( $slot, $slots_to_suggest);
                                            
                                        }
                                    }
                                    
                                    $time_to_compare = date('H:i:s', strtotime($slot_end_time) + 1 * 60);
                                   
                                }
                                
                                array_push($available_hours, $no_of_hours_in_active_slot);
                                $score = $this->calculateScores($available_hours[0], $event_difference);
                                $userObject = $this->mapUserWithScore($user, $score);
                                $total_score += $score;
                                array_push($user_with_score, $userObject);
                                array_push($scores, $score);
                            } else {

                                return $response_object = array(
                                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                                    'response_message' => ('user hours of id ' . $user->id . ' are missing'),
                                    'response_param' => 'User',
                                    'response_values' =>  $not_hive_userObject
                                );
                            }
                        } else {

                            $not_sync_userObject = $this->handleNonSyncGuests($user);
                            array_push($user_with_score, $not_sync_userObject);
                        }
                    } else {
                        $not_hive_userObject = $this->handleNonHivenGuests($user);
                        array_push($user_with_score, $not_hive_userObject);
                    }
                } else return $response_object = array(

                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('social_hour.user_not_exist'),
                    'response_param' => 'User',
                    'response_values' =>  new \stdClass()
                );
            }
            
            
            $users = $this->getUsers($guest_ids);
            $all_users = new \stdClass();
            $all_users->users_with_scores = $user_with_score;
            $all_users->average_score = $this->calculateAverageScore($total_score, count($users));

            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('social_hour.score_generate_success'),
                'response_param' => 'Score',
                'response_values' =>  $all_users
            );
    }

    public function calculateScores($available_hours, $event_difference)
    {
        return $score = ($available_hours / $event_difference) * 100;
    }
    
    public function getUsers($guest_ids)
    {
        return $users = User::whereIn('id', $guest_ids)->get(['id', 'first_name']);
    }

    public function getSlot($user_hours, $j)
    {    
        
        return $slot = SocialHourSlot::where('id', $user_hours[$j]['slot'])->first();
        
    }
    
    public function calculateAverageScore($total_score,$user_count)
    {
        return $average = $total_score / $user_count;
    }
    public function mapUserWithScore($user,$score)
    {
        $userObject = new \stdClass();
        $userObject->id = $user->id;
        $userObject->name = $user->first_name;
        $userObject->profile_picture = $user->profile_picture;
        $userObject->score = $score;
        return $userObject;
    }
    public function handleNonHivenGuests($user)
    {
        $userObject = new \stdClass();
        $userObject->id = $user->id;
        $userObject->name = $user->first_name;
        $userObject->profile_picture = $user->profile_picture;
        $userObject->score = -2;
        return $userObject;
    }
    public function handleNonSyncGuests($user)
    {
        $userObject = new \stdClass();
        $userObject->id = $user->id;
        $userObject->name = $user->first_name;
        $userObject->profile_picture = $user->profile_picture;
        $userObject->score = -1;
        return $userObject;
    }
    public function getSuggestionSlot($slot_id)
    {

        return $slot = SocialHourSlot::where('id', $slot_id)->first();
    }
    public function populateArray($slot,$slots_to_suggest)
    {   
      
        $slot_id = $slot[0]['slot']; 
        if ($slot_id== 1 || $slot_id == 4){
            $array_length = 2;
            if($slot_id==1){
                $next_slot = new \stdClass();
                $next_slot->start_time = $this->getSuggestionSlot($slot_id + 1) ? $this->getSuggestionSlot($slot_id + 1)['slot_start_time'] : $this->getSuggestionSlot($slot_id)['slot_start_time'];
                $next_slot->end_time = $this->getSuggestionSlot($slot_id + 1) ? $this->getSuggestionSlot($slot_id + 1)['slot_end_time'] : $this->getSuggestionSlot($slot_id)['slot_end_time'];
                if (count($slots_to_suggest) < $array_length) {
                    array_push($slots_to_suggest,$next_slot);
                }
            }else{
                $previous_slot = new \stdClass();
                $previous_slot->start_time = $this->getSuggestionSlot($slot_id - 1) ? $this->getSuggestionSlot($slot_id - 1)['slot_start_time'] : $this->getSuggestionSlot($slot_id)['slot_start_time'];
                $previous_slot->end_time = $this->getSuggestionSlot($slot_id - 1) ? $this->getSuggestionSlot($slot_id - 1)['slot_end_time'] : $this->getSuggestionSlot($slot_id)['slot_end_time'];
                if (count($slots_to_suggest) < $array_length) {
                    array_push($slots_to_suggest, $previous_slot);
                }
            }
        }else if($slot_id == 2 || $slot_id == 3){
            $array_length = 3;
            $previous_slot = new \stdClass();
            $previous_slot->start_time = $this->getSuggestionSlot($slot_id - 1) ? $this->getSuggestionSlot($slot_id - 1)['slot_start_time'] : $this->getSuggestionSlot($slot_id)['slot_start_time'];
            $previous_slot->end_time = $this->getSuggestionSlot($slot_id - 1) ? $this->getSuggestionSlot($slot_id - 1)['slot_end_time'] : $this->getSuggestionSlot($slot_id)['slot_end_time'];

            $next_slot = new \stdClass();
            $next_slot->start_time = $this->getSuggestionSlot($slot_id + 1) ? $this->getSuggestionSlot($slot_id + 1)['slot_start_time'] : $this->getSuggestionSlot($slot_id)['slot_start_time'];
            $next_slot->end_time = $this->getSuggestionSlot($slot_id + 1) ? $this->getSuggestionSlot($slot_id + 1)['slot_end_time'] : $this->getSuggestionSlot($slot_id)['slot_end_time'];
            if (count($slots_to_suggest) < $array_length) {
                array_push($slots_to_suggest, $previous_slot, $next_slot);
            }
        }
       
        return $slots_to_suggest;
    }
}
