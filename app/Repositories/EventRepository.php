<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\UserSocialHour;
use App\Models\UserEventPhone;
use App\Models\UserEventBizee;
use App\Models\UserCalender;
use App\Models\EventComment;
use App\Models\HiveMember;
use App\Models\UserEvent;
use App\Models\UserContact;
use App\Models\Swarm;
use App\Models\Hive;
use App\Models\User;
use Carbon\Carbon;
use datetime;
use stdClass;
use Auth;
use DB;

/**
 * Class EventRepository.
 */
class EventRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return UserEventPhone::class;
    }
    /**
     * The create phone event function will create event based on phone or bizee calender sync.
     * Phone event.
     * Bizee event.
     */
    public function createEvent($input)
    {
        if (isset($input['is_bizee']) && isset($input['is_phone'])) {
            return false;
        }
        if (isset($input['is_bizee'])) {

            $eventCreateBizee = UserEventBizee::create([
                'date'       => $input['date'],
                'from_time'  => $input['from_time'],
                'to_time'    => $input['to_time'],
                'title'      => $input['title'],
                'location'   => $input['location'],
                'host_notes' => $input['host_notes'],

            ]);
            $eventCreateBizee->is_bizee = $input['is_bizee'];
            if (isset($input['members'])) {
                $eventCreateBizee->members = $input['members'];
            }
            $eventCreateBizee->duration = $input['duration'];

            /**
             *  This function will check the two parameters and return them to another function.
             */
            if (isset($input['is_swarm']) && isset($input['swarm_id'])) {
                $eventCreateBizee->is_swarm = $input['is_swarm'];
                $eventCreateBizee->swarm_id = $input['swarm_id'];
            }
            // $this->sendInvitationNotification($input['members']);
            // return true;
            return $eventCreateBizee;
        }
        // else if(isset($input['is_phone'])){

        //     $event_create_phone = UserEventPhone::create([
        //         'date'       => $input['date'],
        //         'from_time'  => $input['from_time'],
        //         'to_time'    => $input['to_time'],
        //         'title'      => $input['title'],
        //         'location'   => $input['location'],
        //         'host_notes' => $input['host_notes'],

        //      ]);
        //       $event_create_phone->is_phone = $input['is_phone'];
        //       if(isset($input['members'])){
        //         $event_create_phone->members  = $input['members'];
        //     }
        //     $event_create_phone->duration = $input['duration'];
        //     /**
        //      *  This function will check the two parameters and return them to another function.
        //      */
        //     if (isset($input['is_swarm']) && isset($input['swarm_id'])) {
        //         $event_create_phone->is_swarm = $input['is_swarm'];
        //         $event_create_phone->swarm_id = $input['swarm_id'];
        //     }
        //     return $event_create_phone;
        // } 
        else {
            return false;
        }
    }
    /**
     * The check calender function checks type of calender sync process.
     * Adds detail to calender.
     */
    public function checkCalender($input)
    {

        $userId = Auth::id();

        if ($input['is_bizee']) {

            $userCalender = UserCalender::create([
                'event_owner' => $userId,
                'events_type' => 'Bizee',
                'event_id'    => $input['id'],
                'duration'    => $input['duration'],
                'is_swarm'    => $input['is_swarm'],
                'swarm_id'    => $input['swarm_id']
            ]);
            /**
             * If there are multiple members the foreach loop will add all of them against event id.
             */
            if (isset($input['members'])) {

                $val = $input['members'];
                foreach ($val as $v) {
                    $members = UserEvent::create([
                        'guest_id'          => $v,
                        'event_id'          => $input['id'],
                    ]);
                }
            }

            $members = UserEvent::create([
                'guest_id'          => $userId,
                'event_id'          => $input['id'],
                'response_status'   => 1,
            ]);
        } else if ($input['is_phone']) {
            $userCalender = UserCalender::create([
                'event_owner'   => $userId,
                'events_type'   => 'Phone',
                'event_id'      => $input['id'],
                'duration'      => $input['duration'],
            ]);
        } else {
            return false;
        }
    }
    /**
     * This function gets all the data from user calender.
     */
    public function getUserCalender($input)
    {      
        $checkData = UserCalender::where('events_type', 'Phone')->get();
        if ($checkData->isEmpty()) {
            return false;
        }
        $userId = Auth::id();
        $data = UserCalender::select('id','event_owner','events_type','event_id','is_swarm','comments_id', 'cover_photo')
                            ->with('users:id,user_name,first_name,last_name,profile_picture,social_profile_picture_url')
                            ->where('event_owner', $userId)
                            ->take($input['limit'])
                            ->skip($input['page'])
                            ->get();
        foreach ($data as $v => $val) {
            if ($val['events_type'] == 'Bizee') {
                $val->load('eventbizee:id,date,from_time,to_time,title,location,host_notes,cover_photo');
            }
            if ($val['events_type'] == 'Phone') {
                $val->load('eventphone:id,date,from_time,to_time,title,location,host_notes,cover_photo');
            }
        }
       return $data;
    }
    /**
     * This function will get all the events with bizee.
     */
    public function getAllBizeeEvents($input)
    {
        $data = UserCalender::select('id', 'event_owner', 'events_type', 'event_id', 'is_swarm', 'comments_id', 'cover_photo')
            ->with('eventbizee:id,date,from_time,to_time,title,location,host_notes,cover_photo', 'EventOwner:id,user_name,first_name,last_name,profile_picture,social_profile_picture_url')
            ->where('events_type', 'Bizee')
            ->get();
        return $data;
    }
    /**
     * This function will get all the events of phone.
     */
    public function getAllPhoneEvents($input)
    {
        $data = UserCalender::select('id', 'event_owner', 'events_type', 'event_id', 'is_swarm', 'comments_id', 'cover_photo')
            ->with('eventphone:id,date,from_time,to_time,title,location,host_notes,cover_photo', 'EventOwner:id,user_name,first_name,last_name,profile_picture,social_profile_picture_url')
            ->where('events_type', 'Phone')
            ->get();
        return $data;
    }
    public function syncCalender($input)
    {
        $user = Auth::user();
        $userId = $user->id;
        $events = $input['event'];
        $checkOwner = UserCalender::where('event_owner', $userId)->first();
        if ($checkOwner) {
            $idCheckOwner = $checkOwner->event_id;
            $checkOwner = UserCalender::where('event_owner', $userId)->delete();
            $checkOwner = UserEventPhone::where('id', $idCheckOwner)->delete();
        }

        foreach ($events as $event) {
            $inserted    = false;
            $thisEvent = UserEventPhone::firstOrCreate([
                'date'          => $event['date'],
                'from_time'     => $event['from_time'],
                'to_time'       => $event['to_time'],
                'title'         => $event['title'],
                'location'      => $event['location'],
                'host_notes'    => $event['host_notes'],

            ]);
            // print_r($event['duration']);die();
            UserCalender::create([
                'event_owner'   => $userId,
                'events_type'   => 'Phone',
                'event_id'      => $thisEvent->id,
                'duration'      => $event['duration']
            ]);

            // $file = $event['cover_photo'];
            // $extension = $file->extension();
            // $fileName = $this_event->id . $event['title'] .'.'. $user->id . "." . $extension;
            // $imageSaved = Storage::disk('event_photos')->put($fileName, File::get($file));
            $Inserted = true;
        }

        return UserCalender::select('id', 'event_owner', 'events_type', 'event_id', 'is_swarm', 'comments_id', 'cover_photo')
            ->with('eventphone:id,date,from_time,to_time,title,location,host_notes,cover_photo')
            ->where('event_owner', $userId)
            ->get();
    }
    /**
     * This function will get all the events.
     */
    public function getEvent($input)
    { 
        /*
         * Rsvps
         * 'not_going' =>0,   dark_blue
         * 'going'=>1,        yellow
         * 'may_be'=>2,       light grey
         * 'no_response'=>3   white
         */
        $get_calendar = UserCalender::all();
        if ($get_calendar->isEmpty()) {
            return false;
        }
        $data = UserCalender::select('id', 'event_owner', 'events_type', 'event_id', 'is_swarm', 'swarm_id', 'comments_id', 'cover_photo')->with(
            'EventDetail:id,title,date,from_time,to_time,location,host_notes',
            'events:id,guest_id,event_id,response_status',
            'EventComment.commentfromuser:id,first_name,last_name,profile_picture,created_at',
            'EventOwner:id,first_name,last_name'
        )
            ->where('event_id', $input['event_id'])
            ->where('events_type', $input['events_type'])
            ->get();
      
        print_r($data);die();
        if ($data->isEmpty()) {
            return false;
        }
        if ($data[0]['events']) {
            $data[0]['events']->load('guest:id,first_name,last_name,profile_picture');
            $not_going   = $data[0]['events']->where('response_status', 0)->count();
            $going       = $data[0]['events']->where('response_status', 1)->count();
            $may_be      = $data[0]['events']->where('response_status', 2)->count();
            $no_response = $data[0]['events']->where('response_status', 3)->count();
        }
        $allData = new \stdClass();
        $allData->going = $going;
        $allData->may_be = $may_be;
        $allData->not_going = $not_going;
        $allData->no_response = $no_response;

        $list = new \stdClass();
        $list->eventDetails = $data;
        $list->allData = $allData;
        // return $list;
        return $list;
    }
    /**
     * This function will return all the event of given date.
     */

    public function getEventByDate($input)
    {
        $user_id = Auth::id();
        $get_event_date = UserEvent::where('guest_id', $user_id)->with('event')->whereHas('event', function ($query)  use ($input) {
            $query->where('date', $input['date']);
        })->get();
        return $get_event_date;
    }
    /**
     * This function will return all the upcoming events of login user.
     */
    public function getUpcomingEvent($input)
    {
        $id         = Auth::id();
        $date       = Carbon::now()->toDateTimeString();
        $get_member = UserEvent::select('id', 'guest_id', 'event_id', 'response_status', 'type')
            ->where('guest_id', $id)
            ->with('event:id,title,date,from_time,to_time,location,host_notes')
            ->whereHas('event', function ($query)  use ($date) {
                $query->where('date', '>=', $date);
            })->get();
        return $get_member;
    }
    /**
     * This function will adds member to event
     */
    public function addMemberToEvent($input)
    {
        $this->isSwarmEvent($input['member_id'], $input['event_id']); //function call
        $user_id            = Auth::id();
        $find_id            = User::find($input['member_id']);
        $find_event_id      = UserEventBizee::where('id', $input['event_id'])->get();
        $check_event_member = UserEvent::where('guest_id', $input['member_id'])->where('event_id', $input['event_id'])->get();

        if ($find_id && count($find_event_id) > 0) {
            if ($check_event_member->isEmpty()) {
                $add_member = UserEvent::create([
                    'guest_id'       => $input['member_id'],
                    'event_id'      => $input['event_id']
                ]);
                return $add_member;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function isSwarmEvent($user_id, $event_id)
    {
        echo $user_id;
        echo "   " . $event_id;
        $data = UserCalender::where('event_id', $event_id)->where('is_swarm', 1)->first();
        if ($data) {
            UserCalender::where('event_id', '=', $event_id)
                ->update([
                    'is_swarm' => '(NULL)',
                    'swarm_id' => '(NULL)'
                ]);
            return true;
        }
        return true;
    }
    /**
     * This will remove existing member from event.
     */
    public function removeMemberFromEvent($input)
    {
        $user_id       = Auth::id();
        $remove_member = UserEvent::where('guest_id', $input['member_id'])->where('event_id', $input['event_id'])->delete();
        return $remove_member;
    }
    public function sendInvitationNotification($members)
    {
        foreach ($members as $member) {
            $checkNotyResponce = SendNotification::dispatch($member, 'event_invite');
        }
    }
    /**
     * This will delete an event.
     */
    public function deleteEvent($input)
    {
        $remove_member = UserCalender::where('id', $input['cal_id'])->delete();
        $members = UserEvent::where('event_id', 5)->pluck('guest_id')->toArray();
        foreach ($members as $member) {
            $checkNotyResponce = SendNotification::dispatch($member, 'event_cancel');
        }
        $remove_member = UserEvent::where('event_id', $input['event_id'])->delete();
        $remove_member = UserEventBizee::where('id', $input['event_id'])->delete();

        return $remove_member;
    }
    /**
     * This function will allow members to comment on event.
     * Members means that this user belongs to given event.
     */
    public function addComment($input)
    {
        $checkMember = UserEvent::where('guest_id', $input['user_id'])->where('event_id', $input['event_id'])->get();
        $findId      = User::find($input['user_id']);
        if ($findId && count($checkMember) > 0) {
            $addComment = EventComment::create([
                'event_id'          => $input['event_id'],
                'comment_from_user' => $input['user_id'],
                'comment'           => $input['comment']
            ]);
            return true;
        } else {
            return false;
        }
    }
    /**
     * This function will delete comments based on user id.
     */
    public function deleteComment($input)
    {
        $deleteComment = EventComment::where('event_id', $input['event_id'])->where('id', $input['comment_id'])->delete();
        return $deleteComment;
    }

    /**
     * This api will get the user guests to invite them for his event.
     */
    public function getGuests()
    {
        $user_id = Auth::id();
        $swarm  = Swarm::where('creator_id', $user_id)->select('id', 'title', 'about_us', 'created_at')
            ->with('swarmMembers.user:id,first_name,last_name,profile_picture,social_profile_picture_url')->get();
        $hive_member = Hive::where('hive_owner', $user_id)->select('id', 'hive_owner')->with('hiveMemberForEvent.hive_member:id,first_name,last_name,profile_picture,social_profile_picture_url')->get();
        $phone_members = UserContact::where('my_user_id', $user_id)->select('id', 'name', 'phone_number', 'profile_picture', 'is_bizee', 'is_phone', 'if_contact_id', 'phone_number')->get();

        $list = new stdClass();
        $list->swarms = $swarm;
        $list->hive = $hive_member;
        $list->phone_contacts = $phone_members;
        return $list;
    }
    /*
     * This function will search data from events.
     */
    public function searchEvent($input)
    {
        $user_id     = Auth::id();
        $search_item = $input['search'];
        return $check_credentials = UserEvent::select('id', 'guest_id', 'event_id', 'response_status')
            ->where('guest_id', $user_id)
            ->with('event:id,title,date,from_time,to_time,location,host_notes')
            ->whereHas('event', function ($query)  use ($search_item) {
                $query->where('title', 'like', "%{$search_item}%");
            })->get();
    }
    public function sendInvite($input)
    {
        $user    = Auth::user();
        $userId = $user->id;
        $getEventDetails = UserCalender::select('id', 'event_owner', 'events_type', 'event_id', 'is_swarm', 'swarm_id', 'comments_id', 'cover_photo')
            ->with(
                'events.guest:id,first_name,last_name,profile_picture',
                'EventOwner:id,first_name,last_name,profile_picture',
                'eventbizee:id,date,from_time,to_time,title,location,host_notes'
            )
            ->where('event_owner', $userId)
            ->where('events_type', 'Bizee')
            ->where('event_id', $input['event_id'])
            ->get();
        if ($getEventDetails->isEmpty()) {
            return false;
        } else {
            return $getEventDetails;
        }
    }
    /**
     * This function will get hive member based on events.
     * After getting hive member i will check if member exist or not.
     * if member exists then we will get their upcoming events on the date of newly created event.
     * And then i have to check if there is any conflict of that user with the events.
     */
    public function checkAvailability($input)
    {
        $today                = date("Y-m-d");
        $m                    = 1;
        $get_conflicts_events = [];
        $get_event            = [];
        $arrival_probability  = [];
        $bizee_suggestion     = [];
        $user_id              = Auth::id();
        $month                = explode('-', $input['date']);
        $month_numb           = $month[$m];

        // echo date('Y-m-d', strtotime($today. ' + 1 days'));die();
        $user_event_bizee     = UserEventBizee::all();
        $Hive = Hive::where('hive_owner', $user_id)->first();
        /**
         * This will check if hive member exist.
         */
        if ($Hive) {
            foreach ($input['guest_id'] as $key => $value) {
                $data = HiveMember::where('hive_id', $Hive->id)->where('hive_member', $value)->with('users')->first();
                if ($data) {
                    $get_event[$key] = UserEvent::with('event')->where('guest_id', $data->hive_member)->get();
                }
            }
        }
        if (empty($data)) {
            return false;
        } else {
            if ($user_event_bizee->isEmpty()) {
                return false;
            }
            /**
             * This will get event id's of events
             * And store them in array
             * Foreach will also gets conflicted events. In which there is conflicts of event according to time date and day.
             */
            $i = 0;
            foreach ($get_event as $key2 => $value2) {
                foreach ($value2 as $key3 => $value3) {
                    $single_user_other_events[$i] = $value3->event_id;

                    $get_conflicts_events[$i] = DB::table('user_events_bizee')
                        ->where('id', $single_user_other_events[$i])
                        ->where('date', '>=', $today)
                        ->where('date', $input['date'])
                        ->whereMonth('date', '=', $month_numb)
                        ->where(function ($query)  use ($value, $input) {
                            $query
                                ->where('from_time', '<', $input['from_time'])
                                ->where('to_time', '>', $input['to_time'])
                                ->orwhereBetween('from_time', [$input['from_time'], $input['to_time']])
                                ->orwhereBetween('to_time', [$input['from_time'], $input['to_time']]);
                        })->get();

                    if (count($get_conflicts_events[$i]) > 0)
                        $get_conflicts_events[$i]['user_id'] = $value2[$key2]['guest_id'];
                    $i = $i + 1;
                }
            }
            // print_r($single_user_other_events);die('yas');
            // return $get_conflicts_events;
            if (count($get_conflicts_events) == 0) {
                die('yasir');
                return false;
            } else {
                foreach ($get_conflicts_events as $key => $valuee) {
                    die('hi');
                    $static_time      = '12:00:00';
                    $static_time1     = '24:00:00';
                    $get_data         = $this->getEventSlots($input['from_time'], $input['to_time'], $get_conflicts_events, $input);
                    // return $get_data;
                    // print_r($get_data);
                    print_r($get_data->slot1[0]['Date']);


                    $check_conflicts[$key] = DB::table('user_events_bizee')
                        ->where('id', $single_user_other_events[$key])
                        ->where('date', '>=', $today)
                        ->where('date', $input['date'])
                        ->whereMonth('date', '=', $month_numb)
                        ->where(function ($query)  use ($value, $input, $start_time, $end_time) {
                            $query
                                ->where('from_time', '>=', $start_time)
                                ->where('to_time', '<', $end_time);
                            // ->where('from_time', '<', $input['from_time'])
                            // ->where('to_time', '>', $input['to_time'])
                            // ->orwhereBetween('from_time', [$input['from_time'], $input['to_time']])                                         
                            // ->orwhereBetween('to_time', [$input['from_time'], $input['to_time']]);        
                        })->get();

                    if (count($check_conflicts) > 0) {
                        //No conflict for this user.

                        $bizee_suggestion[$key]['Suggestion'] = "Display suggestion";
                        $bizee_suggestion[$key]['Date'] = $input['date'];
                        $bizee_suggestion[$key]['Start Time'] = $start_time;
                        $bizee_suggestion[$key]['End Time'] = $end_time;
                        $bizee_suggestion[$key]['Arrival'] = "100%";
                    } else {
                        die('not empty');
                    }
                }
                return $bizee_suggestion;

                die();
            }
            /**
             * This foreach will get all the events and put them in array.
             */
            $our_events = [];
            foreach ($get_conflicts_events as $events) {
                if ($events->isNotEmpty()) {
                    $our_events[] = $events;
                }
            }
            /**
             * If our events exist then there are certain condition
             * which are going to use to get an probability of an user to join an event.
             */
            if ($our_events) {
                $get_result =  $our_events;
                foreach ($get_result as $key => $value) {
                    if ($get_result[$key][0]->from_time == $input['from_time'] && $get_result[$key][0]->to_time == $input['to_time']) {
                        $arrival_probability[$key]['arrival_probability'] = 0;
                        $arrival_probability[$key]['UserIds'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time < $input['from_time'] && $get_result[$key][0]->to_time > $input['to_time']) {
                        //This function will calculate difference of another event from_time with our event from_time.
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($input['from_time']));

                        //This function will calculate difference of another event to_time with our event to_time.
                        $calculated_time_diff1    = $this->calculateTimeDiff(strtotime($get_result[$key][0]->to_time), strtotime($input['to_time']));

                        //This will calculate our time difference
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));

                        //This function will add both times differences 
                        $get_added_value          = $this->addValues($calculated_time_diff, $calculated_time_diff1);

                        // $arrival_percentage       = $arrival_percentage-100;
                        $arrival_probability[$key]['Arrival Percentage ( % )'] = 0;
                        $arrival_probability[$key]['UserId']   = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time > $input['from_time'] && $get_result[$key][0]->to_time == $input['to_time']) {
                        $arrival_probability[$key]['arrival_probability'] = 0;
                        $arrival_probability[$key]['UserId']   = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time > $input['from_time'] && $get_result[$key][0]->to_time < $input['to_time']) {
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($input['from_time']), strtotime($get_result[$key][0]->from_time));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);
                        $arrival_percentage       = $arrival_percentage - 100;
                        $arrival_probability[$key]['arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time == $input['from_time'] && $get_result[$key][0]->to_time < $input['to_time']) {
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->to_time), strtotime($input['to_time']));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_our_time_diff, $calculated_time_diff);
                        $arrival_percentage       = $arrival_percentage - 100;
                        $arrival_probability[$key]['arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time < $input['from_time'] && $get_result[$key][0]->to_time == $input['to_time']) {
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($input['from_time']), strtotime($get_result[$key][0]->from_time));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);

                        $arrival_probability[$key]['arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time < $input['from_time'] && $get_result[$key][0]->to_time < $input['to_time']) {
                        $calculated_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($input['from_time']));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);
                        $arrival_probability[$key]['arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time >= $input['to_time'] && $get_result[$key][0]->to_time > $input['to_time']) {
                        $calculated_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($input['from_time']));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);

                        $arrival_probability[$key]['Arrival_probability'] = abs(arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($get_result[$key][0]->from_time == $input['from_time'] && $get_result[$key][0]->to_time > $input['to_time']) {
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($get_result[$key][0]->to_time), strtotime($input['to_time']));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);
                        $arrival_percentage       = $arrival_percentage - 100;
                        $arrival_probability[$key]['arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    }
                    if ($input['from_time'] < $get_result[$key][0]->from_time && $input['to_time'] < $get_result[$key][0]->to_time) {
                        $calculated_time_diff     = $this->calculateTimeDiff(strtotime($get_result[$key][0]->to_time), strtotime($input['to_time']));
                        $calculated_our_time_diff = $this->calculateTimeDiff(strtotime($get_result[$key][0]->from_time), strtotime($get_result[$key][0]->to_time));
                        $arrival_percentage       = $this->calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff);
                        $arrival_percentage       = $arrival_percentage - 100;
                        $arrival_probability[$key]['Arrival_probability'] = abs($arrival_percentage);
                        $arrival_probability[$key]['UserId'] = $value['user_id'];
                        $arrival_probability[$key]['event_id'] = $value[0]->id;
                    } //inner if ends here
                } //foreach ends here

            } //main if ends here  
            //  die('yb');
        }

        /**
         * Created an empty object to store one or more object data.
         */
        if (empty($arrival_probability)) {
            return false;
        }
        $allData = new \stdClass();
        $allData = $arrival_probability;
        return $allData;
    }

    /**
     * This function will add up two values
     */
    public function addValues($a, $b)
    {

        $exp_data  = explode(':', $a);
        $exp_data1 = explode(':', $b);
        $result    = $exp_data[0] + $exp_data1[0];
        return $result;
    }
    public function addTime($time, $time2)
    {
        $secs = strtotime($time2) - strtotime("00:00:00");
        $result = date("H:i:s", strtotime($time) + $secs);
        return $result;
    }
    public function calculateArrivalPercentage($calculated_time_diff, $calculated_our_time_diff)
    {
        // print_r($calculated_our_time_diff);
        // die('hello');
        $exp_data = explode(':', $calculated_time_diff);
        $exp_data1 = explode(':', $calculated_our_time_diff);
        $result = $exp_data[0] / $exp_data1[0] * 100;
        return $result;
    }
    public function calculateTimeDiff($other_event_time, $my_event_time)
    {
        // Formulate the Difference between two dates 
        $diff = abs($other_event_time - $my_event_time);


        // To get the hour, subtract it with years,  
        // months & seconds and divide the resultant 
        // date into total seconds in a hours (60*60) 
        $years = floor($diff / (365 * 60 * 60 * 24));


        // To get the month, subtract it with years and 
        // divide the resultant date into 
        // total seconds in a month (30*60*60*24) 
        $months = floor(($diff - $years * 365 * 60 * 60 * 24)
            / (30 * 60 * 60 * 24));


        // To get the day, subtract it with years and  
        // months and divide the resultant date into 
        // total seconds in a days (60*60*24) 
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
            $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


        $hours = floor(($diff - $years * 365 * 60 * 60 * 24
            - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24)
            / (60 * 60));

        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24
            - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24
            - $hours * 60 * 60) / 60);

        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24
            - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24
            - $hours * 60 * 60 - $minutes * 60));

        //  $calculated_time = $hours.":" .$minutes;
        // print_r($minutes);die();
        if ($hours > 9) {
            return $hours . ":" . '0' . $minutes . ":" . '0' . $seconds;
        } else {
            return '0' . $hours . ":" . '0' . $minutes . ":" . '0' . $seconds;
        }
        //  echo $time;die();
    }
    public function getEventSlots($start_time, $end_time, $data, $input)
    {

        $our_event_total_time   = $this->calculateTimeDiff(strtotime($end_time), strtotime($start_time));

        foreach ($data as $key => $value) {
            $slot1[$key]['Suggestion'] = "Suggested Time";
            $slot1[$key]['Date'] = $input['date'];
            $slot1[$key]['Start_Time']  = $this->calculateTimeDiff(strtotime($data[0][$key]->from_time), strtotime($our_event_total_time));
            $slot1[$key]['End_Time']    = $data[0][$key]->from_time;

            $slot2[$key]['Suggestion'] = "Suggested Time";
            $slot2[$key]['Date'] = $input['date'];
            $slot2[$key]['Start_Time']  = $data[0][$key]->to_time;
            $slot2[$key]['End_Time']    = $this->addTime($data[0][$key]->to_time, $our_event_total_time);
        }
        $data = new \stdClass();
        $data->slot1 = $slot1;
        $data->slot2 = $slot2;
        return $data;
    }
    public function responseEventInvite($input)
    {
        $guest_id = UserContact::select('id')
            ->where('my_user_id', $input['owner_id'])
            ->where('is_phone', 1)
            ->where('phone_number', '+' . $input['phone'])
            ->first();
        if ($guest_id) {
            return UserEvent::updateOrCreate([
                'guest_id'          => $guest_id->id,
                'event_id'          => $input['event_id'],
                'type'              => $input['type']
            ], [
                'response_status'   => $input['response_status'],
            ]);
        }
    }
    public function getEventResponse($input)
    {
        /*
         * Rsvps
         * 'not_going' =>0,   dark_blue
         * 'going'=>1,        yellow
         * 'may_be'=>2,       light grey
         * 'no_response'=>3   white
         */
        $userId = Auth::id();
        $data = UserEvent::where('guest_id', $userId)
                                    ->where('event_id', $input['event_id'])
                                    ->with('guest:id,first_name,last_name,profile_picture')
                                    ->get();
        // return $data;



        if ($data->isEmpty()) {
            return false;
        } 
        if ($data[0]['events']) {
            
            $data[0]['events']->load('guest:id,first_name,last_name,profile_picture');
            
            $not_going   = $data[0]['events']->where('response_status', 0)->count();
            $going       = $data[0]['events']->where('response_status', 1)->count();
            $may_be      = $data[0]['events']->where('response_status', 2)->count();
            $no_response = $data[0]['events']->where('response_status', 3)->count();
        }
    
        $allData              = new \stdClass();
        $allData->going       = $going;
        $allData->may_be      = $may_be;
        $allData->not_going   = $not_going;
        $allData->no_response = $no_response;


        $list = new \stdClass();
        $list->eventDetails = $data;
        $list->allData = $allData;
       
        return $list;
    }
    /**
     * This function will allow user to set there event response
     */
    public function setEventResponse($input)
    {
        $userId = Auth::id();
        $setResponse = UserEvent::where('guest_id', $userId)->where('event_id', $input['event_id'])->get();
        if(empty($setResponse[0])){
            return false;
        }
        if($setResponse[0]['response_status'] == 0 || $setResponse[0]['response_status'] == 1 || $setResponse[0]['response_status'] == 2 || $setResponse[0]['response_status'] == 3)
        { 
            UserEvent::where('event_id', '=', $input['event_id'])
                ->update([
                    'response_status' => $input['status']                
            ]);          
        }
        
        return true;
    }
}
