<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use App\Models\BucketItem;
use App\Models\HiveMember;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Permissions;
use App\Models\Settings;
use App\Models\UsersBlocked;
use Illuminate\Support\Facades\Input;
use App\Models\UserContact;
use App\Models\UserEvent;
use App\Models\Hive;
use Hash;

class UserRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }
    /**
     * signup function handling user sign up
     */
    public function userSignUp($input)
    {
        $code = mt_rand(100000, 999999);
        $recipient = [];
        $message = __('user.activate_account');
        // $activation_code = $code . $message;
        $activation_code = '123321';
        $user = User::select('id', 'first_name', 'last_name', 'user_name', 'biography', 'phone', 'address', 'profile_picture', 'is_activate', 'activation_code')->where('phone', $input['phone'])->first();

        //if user exists
        if ($user) {
            $recipient = $user->phone;
            if ($user->is_activate == \Config::get('constants.user.user_not_active')) {

                // update
                $user->first_name = $input['first_name'];
                $user->last_name = $input['last_name'];
                $user->user_name = $input['first_name'] . ' ' . $input['last_name'];
                $user->phone = $input['phone'];
                // $user->activation_code = $code;
                $user->activation_code = '123321';

                if ($user->save()) {
                    // Messagebird::createMessage('Bizee App', $recipient, $activation_code);                
                    return $user;
                } else {
                    return false;
                }
            }
        } else {
            //create whole record
            $user = User::create($input);
            // $user->activation_code = $code;
            $user->activation_code = '123321';
            $user->save();
            if ($user) {
                //if user was in contacts before
                $user_in_contacts = UserContact::where('phone_number', $input['phone'])->first();
                if ($user_in_contacts) {
                    $user_in_contacts->if_contact_id = $user->id;
                    $user_in_contacts->is_bizee =true;
                    $user_in_contacts->save();
                    $user_in_contacts->delete();
                }
                $id = $user->id;
                $user = User::find($id, ['id', 'first_name', 'last_name', 'biography', 'phone', 'address', 'profile_picture', 'is_activate', 'activation_code']);
                //Messagebird::createMessage('Bizee App', $recipient, $activation_code);
                return $user;
            } else {
                return false;
            }
        }
    }

    /**
     * this function is handling activating user account if user has done with signup
     */

    public function activateUserAccount($input)
    {
        $id = $input['user_id'];
        $userDetails = User::find($id, ['id', 'first_name', 'last_name', 'biography', 'phone', 'address', 'profile_picture', 'is_activate', 'activation_code']);

        if ($userDetails) {
            if ($userDetails->is_activate == \Config::get('constants.user.user_not_active')) {
                if ($input['code'] == $userDetails->activation_code) {
                    $userDetails->is_activate = \Config::get('constants.user.user_Active');
                    Auth::attempt(['phone' => $userDetails->phone, 'password' => $userDetails->password]);
                    $userDetails->token = $userDetails->createToken($userDetails->full_name)->accessToken;
                    $userDetails->player_id = $input['player_id'];
                    $userDetails->save();

                    Settings::firstOrCreate([
                        'user' => $userDetails->id,
                        'in_hive_view_members' => 1,
                        'out_hive_view_members' => 1,
                        'out_hive_invite_event' => 1,
                        'out_hive_view_my_profile' => 1,
                        'out_hive_view_my_hive_members' => 1,
                        'account_privacy' => 0,
                        'allow_suggest' => 1,
                        'pause_all_notifications'=>0,
                        'pause_direct_message_notification' => 0,
                        'pause_hive_notification' => 0,
                        'pause_event_notification' => 0,
                    ]);
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('user.account_activated'),
                        'response_param' => 'User',
                        'response_values' =>  $userDetails
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('user.enter_valid_code'),
                        'response_param' => 'User',
                        'response_values' =>  new \stdClass()
                    );
                }
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('user.already_active'),
                    'response_param' => 'User',
                    'response_values' =>  new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_no_content'),
                'response_message' => __('user.nouserfound'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }
    /**
     * Logut user profile 
     */
    public function logOutUserProfile()
    {
        if (Auth::check()) {
            $token = Auth::user()->token();
            $token->revoke();
            User::where('id', Auth::id())
                ->update([
                    'token' => ''
                ]);
            return true;
        } else {
            return false;
        }
    }
    /**
     * edit user profile is handling user's basic details with bucket list
     */

    public function editUserProfile($input)
    {   $last_name='';
        $user = Auth::user();
        $user_id = $user->id;
        $sent_bucket_list = $input['bucket_list'];

        if ($user->is_activate = \Config::get('constants.user.user_Active')) {
            $bucket_items = $user->bucketItems;
            $sent_bucket_list_count = count($sent_bucket_list);
            $existing_bucket_list = count($bucket_items);
            $diff = $sent_bucket_list_count - $existing_bucket_list;
            if ($diff > 0) {
                $updated_count = 0;
                //update then create remaining
                for ($i = 0; $i < $existing_bucket_list; $i++) {
                    $bucket_item = $sent_bucket_list[$i];
                    $bucket_items[$i]['value'] = $bucket_item;
                    $bucket_items[$i]->save();
                    $updated_count += 1;
                }
                for ($i = 0; $i < $diff; $i++) {
                    $response = BucketItem::create([
                        'user_id' => $user->id,
                        'value' => $sent_bucket_list[$updated_count],
                    ]);
                    $updated_count += 1;
                }
            } else if ($diff < 0) {
                //update then delete remaining
                $updated_count = 0;
                for ($i = 0; $i < $existing_bucket_list; $i++) {
                    if (array_key_exists($i, $sent_bucket_list)) {
                        $bucket_item = $sent_bucket_list[$i];
                        $bucket_items[$i]['value'] = $bucket_item;
                        $bucket_items[$i]->save();
                        $updated_count += 1;
                    }
                }
                for ($i = 0; $i > $diff; $i--) {
                  
                        $bucket_items[$updated_count]->delete();
                        $updated_count += 1;
                   
                }
            } else if ($diff == 0) {
                // update all
                for ($i = 0; $i < $existing_bucket_list; $i++) {
                    $bucket_item = $sent_bucket_list[$i];
                    $bucket_items[$i]['value'] = $bucket_item;
                    $bucket_items[$i]->save();
                }
            }
            $user->user_name = $input['full_name'];
            $arr = explode(' ', trim($input['full_name']));
            if(sizeof($arr)>0){
                $user->first_name = $arr[0];
                unset($arr[0]);
                foreach ($arr as $val) {
                    $last_name = $last_name . $val . ' ';
                }
            }
            $user->last_name = $last_name;
            $user->biography = $input['bio'];
            $user->address = $input['address'];
            //profile picture process
              if(isset($input['profile_picture'])){
                $file = $input['profile_picture'];
                $extension = $file->extension();
                $fileName = time() . "-" . $user->id . "." . $extension;
                $imageSaved  = Storage::disk('users')->put($fileName, File::get($file));
                if ($imageSaved) {
                    $imagePathAndName = $fileName;
                    $deleteImage = $user['profile_picture'];
                    $user->profile_picture =  $imagePathAndName;
                    if ($deleteImage != "") {
                        $response = Storage::disk('users')->delete($deleteImage); //Delete prvious image from storage
                    }
                }
              }
            if ($user->save()) {
                $user_object = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address')->with('bucketItems:id,user_id,value')->find($user_id);
                return  $user_object;
            }
        } else {
            return false;
        }
    }

    /**
     * this function handling getting other user's profile
     */

    public function getUserProfile($input)
    {  
        $is_hive_member = null;
        $requ_sent_to_auth = null;
        $to_user = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address')->with('bucketItems')->with(
            ['favorites' => function ($q) {
                $q->where('type', 'hive')->Where('user_id', Auth::id());
            }]
        )->find($input['user_id']);
        if ($to_user->hive)
            $is_hive_member = HiveMember::where('hive_member', $input['user_id'])->where('hive_owner', Auth::id())->first();
        if (Auth::user()->hive)
            $requ_sent_to_auth = HiveMember::where('hive_member', Auth::id())->where('hive_owner', $input['user_id'])->first();
        $user_settings = Settings::where('user', $input['user_id'])->first();
        $can_out_hive_view_my_profile = $user_settings->out_hive_view_my_profile;
        if ($to_user) {
            $blocked_user = UsersBlocked::where(function ($query) use ($input) {
                $query->where('from_user', Auth::id());
                $query->where('to_user', $input['user_id']);
            })->orwhere(function ($query) use ($input) {
                $query->where('from_user', $input['user_id']);
                $query->where('to_user', Auth::id());
            })->first();
            //request sent shoulld appear insead of accept decline
            if (Auth::id() != $input['user_id']) {
                if ((((!$can_out_hive_view_my_profile) && ($to_user->hive && $is_hive_member)) || ($can_out_hive_view_my_profile == 1)) && (!($blocked_user))) {

            if($requ_sent_to_auth && $requ_sent_to_auth->is_sender){
                      
                        if ($is_hive_member->is_active) {
                            $to_user->status = 'in hive';
                        } else if (!($is_hive_member->is_active)) {
                            $to_user->status = 'request arrived';
                        }
            } else if ($is_hive_member && $is_hive_member->is_sender) {
                        if ($is_hive_member->is_active) {
                            $to_user->status = 'in hive';
                        } else if (!($is_hive_member->is_active)) {
                            $to_user->status = 'request sent';
                        }
            }else{
                        $to_user->status = 'add to hive';
            }
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('user.user_detail'),
                        'response_param' => 'User',
                        'response_values' =>  $to_user
                    );
                } else {

                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('user.cant_get_user'),
                        'response_param' => 'User',
                        'response_values' => new \stdClass()
                    );
                }
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('user.cant_get_himself'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.user_not_exist'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }

    /**
     * this function handling getting my user's profile
     */

    public function getMyProfile()
    {

        $user = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address')
            ->with('bucketItems')->where('id', Auth::id())->first();

        if ($user) {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('user.user_detail'),
                'response_param' => 'User',
                'response_values' =>  $user
            );
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.cant_get_user'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }

    /**
     * this function is handling sending login verification code in the process of user login
     */

    public function sendLoginVerificationCode($input)
    {
        $code = mt_rand(100000, 999999);
        $phone = $input['phone'];
        $message = __('user.activate_account');
        $user = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address', 'phone', 'is_activate', 'activation_code')->where('phone', $phone)->first();

        if ($user) {
            if ($user->is_activate == \Config::get('constants.user.user_Active')) {
                // $user->activation_code = $code;
                if ($user->save()) {
                    // Messagebird::createMessage('Bizee App', $recipient, $activation_message);                
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('user.activate_account_succeeded'),
                        'response_param' => 'User',
                        'response_values' => $user
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('user.activate_account_failed'),
                        'response_param' => 'User',
                        'response_values' => new \stdClass()
                    );
                }
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('user.user_not_activated'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.no_user_found'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }


    /**
     * this function is providing login token in the process of user login , if user has entered correct verification code
     */

    public function verifyLoginCode($input)
    {
        $entered_code = $input['code'];
        $user_id = $input['user_id'];
        $user = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address', 'phone', 'is_activate', 'activation_code')->find($input['user_id']);

        if ($user) {
            if ($user->is_activate == \Config::get('constants.user.user_Active')) {
                if ($user->activation_code == $entered_code) {
                    Auth::attempt(['phone' => $user->phone, 'password' => $user->password]);
                    $user->token = $user->createToken($user->full_name)->accessToken;
                    $user->player_id = $input['player_id'];
                    $user->save();
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_success'),
                        'response_message' => __('user.login_success'),
                        'response_param' => 'User',
                        'response_values' => $user
                    );
                } else {
                    return $response_object = array(
                        'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                        'response_message' => __('user.login_none_code'),
                        'response_param' => 'User',
                        'response_values' => new \stdClass()
                    );
                }
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('user.user_not_activated'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.no_user_found'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }
    public function getAllUsers($input)
    {
    
        $user_id     =  Auth::id();
        $blocked_ids = [];
        $blocked_users =  UsersBlocked::where('from_user', $user_id)->get(['id', 'from_user', 'to_user'])->toArray();

        foreach ($blocked_users as $blocked_user) {
            array_push($blocked_ids, $blocked_user['to_user']);
        }
        $hive_bizee_users = User::select('id', 'first_name', 'last_name', 'profile_picture')->where('id', '!=', $user_id)
            ->with(
                ['hive_members' => function ($q) use ($user_id) {
                    $q->where('hive_owner', $user_id);
                }]
            )->with(
                ['favorites' => function ($q) use ($user_id) {
                    $q->where('type', 'user')->where('user_id', $user_id);
                }]
            )->where('is_activate', \Config::get('constants.user.user_Active'))
            ->whereNotIn('id', $blocked_ids);
        $not_hive_bizee_users = User::select('id', 'first_name', 'last_name', 'profile_picture')->with(
            ['favorites' => function ($q) use ($user_id) {
                $q->where('type', 'user')->where('user_id', $user_id);
            }]
        )->where('is_activate', \Config::get('constants.user.user_Active'))->whereNotIn('id', $blocked_ids);
        //pagination
        if ($input['limit'] == -1 && $input['page'] == -1) {
            if (User::find($user_id)->hive) {
                return $hive_bizee_users = $hive_bizee_users->get()->toArray();
            } else {
                $empty = array();
                $not_hive_bizee_users = $not_hive_bizee_users->get()->toArray();
                foreach ($not_hive_bizee_users as $key => $users) {
                    $not_hive_bizee_users[$key]['hive_members'] = $empty;
                }
                return $not_hive_bizee_users;
            }
        } else {
            if (User::find($user_id)->hive) {
                return $hive_bizee_users = $hive_bizee_users->take($input['limit'])->skip($input['page'])->get()->toArray();
            } else {
                $empty = array();
                $not_hive_bizee_users = $not_hive_bizee_users->take($input['limit'])->skip($input['page'])->get()->toArray();
                foreach ($not_hive_bizee_users as $key => $users) {
                    $not_hive_bizee_users[$key]['hive_members'] = $empty;
                }
                return $not_hive_bizee_users;
            }
        }
    }
    /**
     * this function is handling blocking a user
     */

    public function blockAUser($input)
    {

        $user = Auth::user();
        $user_to_be_blocked = $input['to_user'];
        $if_user_exists = User::find($user_to_be_blocked);

        if ($if_user_exists) {
            $user_blocked = UsersBlocked::firstOrCreate([
                'from_user' => $user->id,
                'to_user' => $user_to_be_blocked,
            ]);
            if ($user_blocked) {
                // $user_blocked =  UsersBlocked::with('to_user:id,first_name,last_name,address,biography', 'from_user:id,first_name,last_name,address,biography')->where('from_user', $user->id)->where('to_user', $user_to_be_blocked)->first(['id', 'from_user', 'to_user']);
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('user.user_blocked_success'),
                    'response_param' => 'User',
                    'response_values' =>  new \stdClass()
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('user.user_blocked_fail'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            }
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.blocking_user_not_exist'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }

    /**
     * this function is handling blocking a user
     */

    public function completeUserDetails($input)
    {

        $user = Auth::user();
        $user->address = $input['address'];
        $user->biography = $input['biography'];
        $user->is_user_info = \Config::get('constants.user.details_added');

        if ($user->save()) {
            $user = User::select('id', 'first_name', 'last_name', 'profile_picture', 'biography', 'address', 'phone')->find($user->id);
            if ($user) {
                return $user;
            } else {
                return false;
            }
        }
    }
    /**
     * this function handling un blocking a user
     */

    public function unBlockAUser($input)
    {

        $user = Auth::user();
        $user_to_be_blocked = $input['to_user'];
        $unBlockedUser = UsersBlocked::with('from_user:id,first_name,last_name,address,biography', 
        'to_user:id,first_name,last_name,address,biography')
        ->where('from_user', $user->id)->where('to_user', $user_to_be_blocked)
        ->first(['id', 'from_user', 'to_user']);

        if ($unBlockedUser) {
            $response = $unBlockedUser->delete();
            if ($response) {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_success'),
                    'response_message' => __('user.user_unblocked_success'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            } else {
                return $response_object = array(
                    'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                    'response_message' => __('user.user_unblocked_fail'),
                    'response_param' => 'User',
                    'response_values' => new \stdClass()
                );
            } 
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.un_blocking_user_not_exist'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }
    /**
     * this function handling getting user by phone
     */

    public function getUser($request)
    {
        $user = User::where('phone', $request['phone'])->first();
        return $user;
    }
    /**
     * this function is returning all user's blocked accounts
     */

    public function getBlockedAccounts($input)
    {
        $user = Auth::user();

        $to_user =  UsersBlocked::with('to_user:id,first_name,last_name,profile_picture')->where('from_user', $user->id)->get(['id', 'to_user'])->toArray();;
    
        if (count($to_user) > 0) {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('user.get_blocked_success'),
                'response_param' => 'User',
                'response_values' => $to_user
            );
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.get_blocked_fail'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }

    /**
     * this function is returning all searched blocked accounts of user
     */

    public function searchBlockedAccounts($input)
    {
        $search_term = $input['search_term'];

        $searched_users = UsersBlocked::with('from_user:id,first_name', 'to_user:id,first_name')->where('from_user',  Auth::id())->whereHas('to_user', function ($query)  use ($search_term) {
            $query->where('first_name', 'like', "%{$search_term}%")->orWhere('last_name', 'like', "%{$search_term}%");
        })->get(['id', 'from_user', 'to_user']);
        return $searched_users;
    }
    /**
     * this function is returning all searched blocked accounts of user
     */

    public function searchBizeeUsers($input)
    {
        $search_term = $input['search_term'];
        $private_ids = Settings::where('account_privacy', 1)->get()->pluck('user')->toArray();

        array_push($private_ids, Auth::id());
        $searched_users = User::whereNotIn('id', array_unique($private_ids))
                          ->where('first_name', 'like', "%{$search_term}%")
                          ->where('last_name', 'like', "%{$search_term}%")
                          ->where('is_activate', \Config::get('constants.user.user_Active'))   
                          ->with('favorites', 'hive_members');
        if ($input['limit'] == -1 && $input['page'] == -1) {
            $searched_users =  $searched_users->get(['id', 'first_name', 'last_name', 'profile_picture']);
        } else {
            $searched_users = $searched_users->take($input['limit'])->skip($input['page'])->get(['id', 'first_name', 'last_name', 'profile_picture']);
        }
        return $searched_users;
    }
    /**
     * this function handling uploading profile picture
     */
    public function uploadProfilePicture($input)
    {
        $user = Auth::user();
        if (array_key_exists('admin_id', $input)) {
            $user->id = $input['admin_id'];
        }
        $file = $input['image'];
        $extension = $file->extension();
        $fileName = time() . "-" . $user->id . "." . $extension;
        $imageSaved = Storage::disk('users')->put($fileName, File::get($file));

        if ($imageSaved) {
            $imagePathAndName = $fileName;
            $userDetails = User::find($user->id, ['id']);
            $deleteImage = $userDetails['profile_picture'];
            $userDetails->profile_picture = $imagePathAndName;
            $userDetails->save();
            if ($deleteImage != "") {
                Storage::disk('users')->delete($deleteImage); //Delete previous image from storage
            }
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_success'),
                'response_message' => __('user.profile_img_uploaded'),
                'response_param' => 'User',
                'response_values' => $userDetails

            );
        } else {
            return $response_object = array(
                'response_code' => \Config::get('constants.response.ResponseCode_fail'),
                'response_message' => __('user.profile_img_error'),
                'response_param' => 'User',
                'response_values' => new \stdClass()
            );
        }
    }
    /**
     * this function is getting mutual events with nother user
     */
    public function getMutualEventsWithAnotherUser($input)
    {
        $events = [];

        $mutual_events = UserEvent::with('event:id,date,from_time,to_time,title,location,host_notes,cover_photo')->select('id', 'guest_id', 'event_id')
            ->whereIn('guest_id', [Auth::id(), $input['user_id']])->take($input['limit'])->skip($input['page'])
            ->get(['id'])->groupBy('event_id')->toArray();
        $array_keys = array_keys($mutual_events);
        foreach ($array_keys as $key) {

            if (count($mutual_events[$key]) < 2) {
                unset($mutual_events[$key]);
            } else {
                array_push($events, $mutual_events[$key][0]['event'][0]);
            }
        }
        if (count($mutual_events) > 0) {
            return $events;
        } else {
            return false;
        }
    }

    public function updatePlayerId($input)
    {
        $player_id = $input['player_id'];
        $user = User::find(Auth::id());
        $user->update(['player_id' => $input['player_id']]);
        $updated_user = $user::select('id', 'first_name', 'last_name', 'player_id');

        if ($updated_user) {
            return $updated_user;
        } else return false;
    }

    /**
     * Admin Services
     */
    /**
     *  all users listing
     */
    public function userCount()
    {
        return  User::all()->where('role_id', 3)->count();
    }
    public function employeeCount()
    {
        return  User::all()->where('role_id', 2)->count();
    }
    public function recentUsers()
    {
        return $users = User::where('role_id', 3)->where('created_at', '>=',  Carbon::now()->subDays(7))->paginate(50);
    }

    public function recentEmployee()
    {
        return $users = User::where('role_id', 2)->where('created_at', '>=',  Carbon::now()->subDays(7))->paginate(50);
    }

    public function allUsers()
    {
        $queryString = Input::get('filter');
        $sortcol = Input::get('sortcol');
        $sort = Input::get('sort');
        if (empty($sortcol)) {
            $sortcol = 'first_name';
        }
        if (empty(Input::get('sort'))) {
            $sort = 'asc';
        }
        $builder = User::query();
        $builder->where('role_id', 3);
        if (Input::has('filter') && $queryString != '') {
            $builder->where('first_name', 'LIKE', "%$queryString%");
            $builder->orWhere('city', 'LIKE', "%$queryString%");
            $builder->orWhere('phone', 'LIKE', "%$queryString%");
        }
        return $users = $builder->orderBy($sortcol, $sort)->paginate(20);
    }

    public function destroyUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
    }
    public function editUser($id)
    {
        return  User::with('bucketItems')->findOrFail($id);
    }
    public function phoneExistWithId($id, $phone)
    {
        if (User::where('Id', '!=', $id)->wherePhone($phone)->first()) {
            return 'false';
        } else {
            return 'true';
        }
    }
    public function phoneExist($phone)
    {
        if (User::wherePhone($phone)->first()) {
            return 'false';
        } else {
            return 'true';
        }
    }
    public function updateUser($req)
    {
        $user = User::find($req['Id']);
        try {
            $user->first_name = $req['first_name'];
            $user->last_name = $req['last_name'];
            $user->phone = $req['phone'];
            $user->biography = $req['biography'];
            $user->address = $req['address'];
            $user->user_name = $req['first_name'] . ' ' . $req['last_name'];
            $user->save();
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 1,
                    'ResponseMessage' => 'User updated successfully',
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 0,
                    'ResponseMessage' => $e->getMessage(),
                ]
            ], 200);
        }
    }
    public function block($id)
    {
        $user = User::find($id);
        $user->is_blocked = 1;
        $user->save();
    }
    public function unblock($id)
    {
        $user = User::find($id);
        $user->is_blocked = 0;
        $user->save();
    }

    public function allEmployees()
    {
        $queryString = Input::get('filter');
        $sortcol = Input::get('sortcol');
        $builder = User::query();
        $builder->where('role_id', 2)->get();

        if (Input::has('filter') && $queryString != '') {
            $builder->where('first_name', 'LIKE', "%$queryString%");
            $builder->orWhere('city', 'LIKE', "%$queryString%");
            $builder->orWhere('phone', 'LIKE', "%$queryString%");
        }
        return $users = $builder->orderBy($sortcol, Input::get('sort'))->paginate(20);
    }
    public function EditGetEmployees($id)
    {
        $user = User::find($id);
        $permission = Permissions::where('Emp_id', $id)->first();
        $userObj = array();
        $userObj['user'] = $user;
        $userObj['permission'] = $permission;
        return $userObj;
    }


    public function EmployeeUpdate($req)
    {
        try {
            $user = User::where('id', $req['data']['ID'])->Update([
                'first_name' => $req['data']['first_name'],
                'last_name' => $req['data']['last_name'],
                'phone' => $req['data']['phone'],
            ]);

            if (isset($req['data']['ReadPermissions']))
                $Read = $req['data']['ReadPermissions'];
            if (isset($req['data']['WritePermissions']))
                $Write = $req['data']['WritePermissions'];
            if (isset($req['data']['DeletePermissions']))
                $Delete = $req['data']['DeletePermissions'];

            $permission = Permissions::where('Emp_id', $req['data']['ID'])->Update([
                'Read' => $Read,
                'write' => $Write,
                'Delete' => $Delete,
            ]);
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 1,
                    'ResponseMessage' => 'Employee Updated successfully',
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 0,
                    'ResponseMessage' => $e->getMessage(),
                ]
            ], 200);
        }
    }
    public function addEmployee($req)
    {
        try {
            $user = User::create([
                'first_name' => $req['first_name'], 'last_name' => $req['last_name'],
                'phone' => $req['phone'], 'password' => bcrypt($req['password']), 'is_activate' => 1, 'role_id' => 2
            ]);

            $userPermission = Permissions::create([
                'Emp_id' => $user->id, 'Read' => $req['ReadPermissions'], 'Write' => $req['WritePermissions'],
                'Delete' => $req['DeletePermissions']
            ]);

            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 1,
                    'ResponseMessage' => 'Employee created successfully',
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 0,
                    'ResponseMessage' => $e->getMessage(),
                ]
            ], 200);
        }
    }

    public function userBucketList($id)
    {
        return BucketItem::where('user_id', $id)->get();
    }
    public function updateBucketList($bucketList)
    {
        if (!empty($bucketList)) {
            try {
                foreach ($bucketList as $item) {
                    $userBucketList = BucketItem::find($item['id']);
                    $userBucketList->value = $item['value'];
                    $userBucketList->save();
                }
                return response()->json([
                    'ResponseHeader' => [
                        'ResponseCode' => 1,
                        'ResponseMessage' => 'User updated successfully',
                    ]
                ], 200);
            } catch (\Exception $e) {
                return response()->json([
                    'ResponseHeader' => [
                        'ResponseCode' => 0,
                        'ResponseMessage' => $e->getMessage(),
                    ]
                ], 200);
            }
        }
    }
    public function graph()
    {
        $today = Carbon::now();
        $files = User::where('created_at', '>', $today->subDays(7))->get();
        $totalCount = $files->count();
        $response = array();
        $i = 0;
        while ($i < 7) {
            $counterperday = 0;
            $dayOfWeek = Carbon::now()->subDays($i);
            foreach ($files as $file) {
                $createDate = new DateTime($file['created_at']);
                $createDatedaysofweek = new DateTime($dayOfWeek);
                $singleday = $createDatedaysofweek->format('Y-m-d');
                $strip = $createDate->format('Y-m-d');

                if ($singleday == $strip) {
                    $counterperday = $counterperday + 1;
                }
            }


            $response[$i] = $counterperday;
            $i++;
        }
        $object = new \stdClass();
        $object->counterperday = $response;
        return response()->json($object);
    }
    public function currentPassword($req)
    {
        $pass = $req['currentpassword'];
        $id = $req['Id'];
        $user = User::find($id);

        if (Hash::check($pass, $user->password)) {

            return 'true';
        } else {

            return 'false';
        }
    }

    public function resetPassAdmin($req)
    {
        $req = $req['data'];
        $user = User::find($req['Id']);
        try {
            $user->password = bcrypt($req['password']);
            $user->save();
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 1,
                    'ResponseMessage' => 'Admin updated successfully',
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 0,
                    'ResponseMessage' => $e->getMessage(),
                ]
            ], 200);
        }
    }
    public function adminProfileUpdate($req)
    {
        $input = $req['data'];
        try {
            User::updateOrCreate([
                'id'       => $input['Id']
            ], [
                'first_name'       => $input['first_name'],
                'last_name'       => $input['last_name'],
                'phone'       => $input['phone']
            ]);
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 1,
                    'ResponseMessage' => 'Admin updated successfully',
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'ResponseHeader' => [
                    'ResponseCode' => 0,
                    'ResponseMessage' => $e->getMessage(),
                ]
            ], 200);
        }
    }
    public function getUserContacts($req)
    {
        $abc = UserContact::where('my_user_id', $req['user_id'])
            ->where('is_phone', 1)->get();
        if ($abc) {
            return $abc;
        } else {
            return false;
        };
    }
    public function allNonBizeeUsers()
    {
        return UserContact::paginate(20);
    }
}
