<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_owner');
            $table->string('events_type');
            $table->unsignedInteger('event_id');
            $table->boolean('is_swarm');
            $table->unsignedInteger('swarm_id')->nullable();
            $table->unsignedInteger('comments_id')->nullable();
            $table->string('duration')->nullable();
            $table->string('cover_photo')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('user_calendars', function (Blueprint $table) {

            $table->foreign('event_owner')->references('id')->on('users')->onDelete('cascade');
            // being deleted due to circular dependancy
            // $table->foreign('comments_id')->references('id')->on('event_comments')->onDelete('cascade');
            // $table->foreign('swarm_id')->references('id')->on('swarms')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_calendars');
    }
}
