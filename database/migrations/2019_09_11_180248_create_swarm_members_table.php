<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwarmMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swarm_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('swarm_id');
            $table->unsignedInteger('user_id');
            $table->smallInteger('role');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('swarm_members', function (Blueprint $table) {

            //   $table->foreign('swarm_id')->references('id')->on('swarms')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swarm_members');
    }
}
