<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name',50)->default('');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('activation_code')->default('0');
            $table->integer('is_activate')->default('0');
            $table->string('password')->default('0');
            $table->string('player_id', 255)->default('0');
            $table->unsignedInteger('role_id')->default('3');
            $table->string('address', 100)->default(' ');
            $table->string('profile_picture')->default(' ');
            $table->boolean('is_blocked')->default('0');
            $table->string('biography')->default(' ');
            $table->string('phone', 20)->default('0');
            $table->string('recovery_code')->default('0');
            $table->string('token', 2056)->default('');
            $table->integer('is_contact_sync')->default('0');
            $table->integer('is_calendar_sync')->default('0');
            $table->integer('is_user_info')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('users')->insert([

            [   'user_name' => 'aqib younus' ,
                'first_name' => 'aqib',
                'last_name' => 'younus',
                'password' => Hash::make('devbatch@123'),
                'is_activate'=>1,
                'phone' => '923315035877',
                'activation_code'=>'12345',
                'is_activate' => '1',
                'player_id' => '66ac50ee-5a26-4f13-aabd-afb4fa12c35d',
                'role_id' => '1',
                'address' => 'lahore',
                'profile_picture' => 'aqib.jpg',
                'is_blocked' => '0',
                'biography' => 'programmer',
                'recovery_code' => '12345',
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUyZWRkOWJiNTk5NmU2Y2EyOTM5NjU4ZjEzMGUxYzViN2M1N2MyYjg3Mzc3N2YxMDQxMTYzZjVjYmE2OWM3MDUxMjEwZjQ2MjkyZmY2YWNlIn0.eyJhdWQiOiI5IiwianRpIjoiZTJlZGQ5YmI1OTk2ZTZjYTI5Mzk2NThmMTMwZTFjNWI3YzU3YzJiODczNzc3ZjEwNDExNjNmNWNiYTY5YzcwNTEyMTBmNDYyOTJmZjZhY2UiLCJpYXQiOjE1NzI0MjE2NjEsIm5iZiI6MTU3MjQyMTY2MSwiZXhwIjoxNjA0MDQ0MDYxLCJzdWIiOiIzMCIsInNjb3BlcyI6W119.anidcd9Ej4cMCzw2QEmZj4Wn5ojzIp9rnjZE8fCy-DfeQMRrsxtP7lHzY6Dm1IZ22lBPdnoh8QPF_zhkc14Z-Ui29Fm8sqGS8kzpmccaZdzmKxdTtAfBiqmiFM1SzZxOHMt2-iBScaCMTCNMFCPcqRAhCfEWKBdktwkEWA1RMCwtJ_SdIWrCiW-1PNe-SI2_axJvnsUMtnNOPKOzihJ4yZWNm1VQ-gH_KTXMBJHudykEjOQK1fw1Y_OiKy0kpm8qrq3N8MNOqyDu0rZEAefslyTcZQUS-zlFOP_j3zSG5JHuvz9ZbwcTw-vn_B1tV_wycxgY6UtES0wyr-f6cn1yxIGRAlmerAwnV81y1sY2zDyuz28-pa4mJveFfRk87_Q_j-KYva7gnUzX2bHQysGbklt9G1qucsampZEC3MDEFzJ4K6EcnuSsWNErSDvTp-hla28aN6KyTvH3BQe8_jjBYOtXNrMzrrXX0GGUyg7sMAx_QTSoe4UHYWJ2sFxSBWQ7U2YcxYKwQe0wQE7bD0r28erqfCN_Fp63_wdxFOaVDhxhABucVz0t5GQH2nJ7HntjLp649mqliObEMYoRJY0EzZpAUDs63TTb8gAXi_gMh9uUTj9a8-k4IrkiW0xkf89_8sE-x4mZf0kDg-hu0G4',
                'is_contact_sync'=>'1',
                'is_calendar_sync'=>'1',
                'is_user_info'=>'1',
                'created_at' => null, 
                'updated_at' => null,
                'deleted_at'=>null
                ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
