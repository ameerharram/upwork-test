<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwarmMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swarm_media', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('swarm_id');
            $table->string('background_color');
            $table->string('foreground_color')->default('');
            $table->string('profile_picture')->default('');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('swarm_media', function (Blueprint $table) {

            $table->foreign('swarm_id')->references('id')->on('swarms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swarm_media');
    }
}
