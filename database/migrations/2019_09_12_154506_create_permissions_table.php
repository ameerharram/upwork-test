<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('Emp_id')->default('1');
            $table->integer('Read')->default('1');
            $table->integer('Write')->default('1');
            $table->integer('Delete')->default('1');
            $table->timestamps();
            $table->softDeletes();
        
        });
        Schema::table('permissions', function ($table) {
            $table->foreign('Emp_id')->references('id')->on('users')->onDelete('cascade');
        });
        DB::table('permissions')->insert([

            [
                'Emp_id' => 1,
                'Read' => 1, 
                'Write' => 1,
                'Delete'=>1,
                'created_at' => null,
                'updated_at' => null,
                'deleted_at' => null],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
