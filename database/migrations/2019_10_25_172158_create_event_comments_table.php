<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->unsignedInteger('comment_from_user');
            $table->string('comment', 1024);
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('event_comments', function (Blueprint $table) {
            $table->foreign('comment_from_user')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('event_id')->references('event_id')->on('user_calendars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_comments');
    }
}
