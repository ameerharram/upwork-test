<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEventsPhoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_events_phone', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('from_time');
            $table->time('to_time');
            $table->string('title');
            $table->string('location', 100);
            $table->string('host_notes', 1024);
            $table->string('cover_photo', 255);
            $table->string('profile_picture', 50)->default('');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_events_phone');
    }
}
