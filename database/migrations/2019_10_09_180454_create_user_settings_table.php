<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user');
            $table->boolean('in_hive_view_members')->default('1');
            $table->boolean('out_hive_view_members')->default('1');
            $table->boolean('out_hive_invite_event')->default('1');
            $table->boolean('out_hive_view_my_profile')->default('1');
            $table->boolean('out_hive_view_my_hive_members')->default('1');
            $table->boolean('account_privacy')->default('0');
            $table->boolean('allow_suggest')->default('1');
            $table->boolean('pause_all_notifications')->default('0');
            $table->boolean('pause_direct_message_notification')->default('0');
            $table->boolean('pause_hive_notification')->default('0');
            $table->boolean('pause_event_notification')->default('0');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::table('user_settings', function (Blueprint $table) {

            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
        });
        DB::table('user_settings')->insert([

            [
                'user' => 1,
                'in_hive_view_members' => 1,
                'out_hive_view_members' => 1,
                'out_hive_invite_event' => 1,
                'out_hive_view_my_profile' => 1,
                'out_hive_view_my_hive_members' => 1,
                'account_privacy' => 0,
                'allow_suggest' => 1,
                'pause_all_notifications' => 0,
                'pause_direct_message_notification' => 0,
                'pause_hive_notification' => 0,
                'pause_event_notification' =>0,
                'created_at' => null,
                'updated_at' => null,
                'deleted_at' => null
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settins');
    }
}
