<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swarms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('creator_id');
            $table->string('title')->default('title');
            $table->string('about_us')->default('about_us');
            $table->timestamps();
            $table->softDeletes();
        });
        // Schema::table('swarms', function (Blueprint $table) {

        //     $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');

        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swarms');
    }
}
