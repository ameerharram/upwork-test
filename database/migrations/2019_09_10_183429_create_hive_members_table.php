<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiveMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hive_members', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('hive_member');
            $table->boolean('is_active')->default('0');
            $table->unsignedInteger('hive_owner');
            $table->boolean('is_sender');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('hive_members', function (Blueprint $table) {

            $table->foreign('hive_member')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('hive_owner')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hive_members');
    }
}
