<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_events', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('guest_id');
            $table->unsignedInteger('event_id');
            $table->smallInteger('response_status');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('user_events', function (Blueprint $table) {

            $table->foreign('event_id')->references('id')->on('user_calendars')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_events');
    }
}
