<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contacts', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('my_user_id');
            $table->unsignedInteger('if_contact_id')->default('0');
            $table->string('name');
            $table->string('phone_number');
            $table->boolean('is_bizee')->default('0');
            $table->boolean('is_phone')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('user_contacts', function (Blueprint $table) {

            $table->foreign('my_user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('if_contact_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_contacts');
    }
}
