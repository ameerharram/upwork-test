<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateSocialHoursSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('social_hours_slots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slot_name');
            $table->time('slot_start_time');
            $table->time('slot_end_time');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('social_hours_slots')->insert([

            [
                'slot_name' => 'Morning',
                'slot_start_time' => '06:00:00',
                'slot_end_time' => '12:00:00',
                'created_at'=>Carbon::now(),
                'updated_at'=> Carbon::now(),
                'deleted_at'=> null
            ],
            [
                'slot_name' => 'Afternoon',
                'slot_start_time' => '12:00:00',
                'slot_end_time' => '18:00:00',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => null
            ],
            [
                'slot_name' => 'Night',
                'slot_start_time' => '18:00:00',
                'slot_end_time' => '24:00:00',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => null
            ],
            [
                'slot_name' => 'Late Night',
                'slot_start_time' => '24:00:00',
                'slot_end_time' => '06:00:00',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => null
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_hours_slots');
    }
}
