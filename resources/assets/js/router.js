import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from './services/auth'

/*
 |--------------------------------------------------------------------------
 | Admin Views
 |--------------------------------------------------------------------------|
 */

// Dashboard
import Basic from './views/admin/dashboard/Basic.vue'

// Layouts
import LayoutBasic from './views/layouts/LayoutBasic.vue'
import MyLayout from './views/layouts/MyLayout.vue'
import MyNavLayout from './views/layouts/MyNavLayout.vue'
import LayoutLogin from './views/layouts/LayoutLogin.vue'

// Users
import Users from './views/admin/users/Users.vue'
import Profile from './views/admin/users/Profile.vue'
import Newuser from './views/admin/users/Newuser.vue'
import Edituser from './views/admin/users/Edituser.vue'
import Favourites from './views/admin/users/Favourites.vue'

//employees
import Employees from './views/admin/employees/Employees.vue'
import Newemployees from './views/admin/employees/Newemployees.vue'
import Editemployees from './views/admin/employees/Editemployees.vue'

//hive
import Hive from './views/admin/hive/Hive.vue'

//swarm
import Swarm from './views/admin/swarm/Swarm.vue'
import swarmProfile from './views/admin/swarm/Profile.vue'

//events
import BizeeEvents from './views/admin/events/bizeeEvents.vue'
import PhoneEvents from './views/admin/events/phoneEvents.vue'
import NewEvent from './views/admin/events/NewEvent.vue'

//cms
import userInvitation from './views/admin/cms/userInvitation.vue'

// import employeesDetails from './views/admin/employees/employeesDetails.vue'
//roles permissions


// Settings
import Settings from './views/admin/Settings.vue'
import BackgroundColors from './views/admin/settings/backgroundColors.vue'


/*
 |--------------------------------------------------------------------------
 | Other
 |--------------------------------------------------------------------------|
 */

// Auth
import Login from './views/auth/Login.vue'
import Forgot from './views/auth/Forgot.vue'
import Reset from './views/auth/Reset.vue'
import Verify from './views/auth/Verify.vue'
import Register from './views/auth/Register.vue'

import NotFoundPage from './views/errors/404.vue'

/*
 |--------------------------------------------------------------------------
 | Frontend Views
 |--------------------------------------------------------------------------|
 */


Vue.use(VueRouter)

const routes = [

  /*
   |--------------------------------------------------------------------------
   | Layout Routes for DEMO
   |--------------------------------------------------------------------------|
   */

  /*
   |--------------------------------------------------------------------------
   | Frontend Routes
   |--------------------------------------------------------------------------|
   */

  {
    path: '/',
    component: LayoutBasic,
    redirect: '/dashboard',
  },
  {
    path: '/',
    component: MyNavLayout, // Change the desired Layout here
    meta: { requiresAuth: false },
    children: [
      {
        path: 'cms/userInvitation/:eventId',
        component: userInvitation,
        name: 'guestinvite',
        props: true,
      },
      
    ]
  },


  /*
   |--------------------------------------------------------------------------
   | Admin Backend Routes
   |--------------------------------------------------------------------------|
   */
  {
    path: '/',
    component: LayoutBasic, // Change the desired Layout here
    meta: { requiresAuth: true },
    children: [
      // Dashboard
      {
        path: '/dashboard',
        component: Basic,
        name: 'dashboard'
      },

      //******* USers **********/

      {
        path: 'users/profile/:userId',
        component: Profile,
        name: 'viewprofile',
        props: true,
      },
      {
        path: 'users/favourites/:userId',
        component: Favourites,
        name: 'viewUserFavourite',
        props: true,
      },
      
      {
        path: 'users/add',
        component: Newuser
      },
      {
        path: 'users/edit/:userId',
        component: Edituser,
        name: 'edituser',
        props: true,
      },
      {
        path: 'users',
        component: Users
      },

      //******* Employees **********/

      {
        path: 'employees/add',
        component: Newemployees
      },
      {
        path: 'employees/edit/:employeesId',
        component: Editemployees,
        name: 'editemployees',
        props: true,
      },
      {
        path: 'employees',
        component: Employees
      },

      //******* Hive **********/
      {
        path: 'hive',
        component: Hive
      },

      //******* Swarm **********/
      {
        path: 'swarm',
        component: Swarm
      },
      {
        path: 'swarm/profile/:swarmId',
        component: swarmProfile,
        name: 'viewswarmprofile',
        props: true,
      },
      
      //******* Events **********/
      {
        path: 'events/bizeeEvents',
        component: BizeeEvents
      },

      {
        path: 'events/phoneEvents',
        component: PhoneEvents
      },

      {
        path: '/Events/create',
        component: NewEvent
      },
      
      // Settings
      {
        path: 'settings',
        component: Settings
      },
      {
        path: 'backgroundColors',
        component: BackgroundColors
      },

    
    ]
  },

  /*
   |--------------------------------------------------------------------------
   | Auth & Registration Routes
   |--------------------------------------------------------------------------|
   */

  {
    path: '/',
    meta: { requiresAuthlogin: true },
    component: LayoutLogin,
    children: [
      {
        path: 'login',
        component: Login,
        name: 'login'
      },
      {
        path: 'forgot',
        component: Forgot,
        name: 'forgot'
      },
      {
        path: 'reset',
        component: Reset,
        name: 'reset'
      },
      {
        path: 'verify',
        component: Verify,
        name: 'verify'
      },
      {
        path: 'register',
        component: Register,
        name: 'register'
      }
    ]
  },

  // Demo Pages


  //  DEFAULT ROUTE
  { path: '*', component: NotFoundPage }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.requiresAuth)) {
    return AuthService.check().then(authenticated => {
      if (!authenticated) {

        return next({ path: '/login' })
      }

      return next()
    })
  }
  else if (to.matched.some(m => m.meta.requiresAuthlogin)) {

    return AuthService.check().then(authenticated => {
      if (authenticated) {

        return next({ path: '/dashboard' })
      }

      return next()
    })
  }

  return next()
})
export default router