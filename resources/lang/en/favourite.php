<?php

return [

    'favorites'                     => "The content is successfully added to favorites.",
    'unfav'                         => "The content is successfully removed from favorites.",
    'content'                       => "The content not found",
    'user_favourites'               => "got all user favourites successfully",
    'user_favourites_fail'          => "user have not favourites"
];
