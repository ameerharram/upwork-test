<?php

return [

  'get_contacts_success'=>'congratulations , all user contacts found',
  'get_contacts_fail'=>'sorry :( no contacts found',
  'get_bizee_success'=> 'congratulations , all your contacts on bizee network found',
  'get_bizee_fail' => 'sorry :( no contacts correspond to bizee found',
  'get_phone_success'=> 'congratulations , all phone contacts found',
  'get_phone_fail' => 'sorry :( no your phone contacts found',
  'no_user_found'=>'sorry :( no user found',
  'sync_success'=>'all contacts synced successfully',
  'sync_fail'=>'sync failed, user has already these contacts',
  'contact_added'=>'contact has been added',
  'contact_added_fail'=>'sorry contct not been added.',
  'searched_contact'=>'search result found',
  'searched_contact_failed'=>'search result not found',
  'contact_changed' => 'contact has been added',
  'contact_changed_fail' => 'sorry contct not been added.',
  

];
