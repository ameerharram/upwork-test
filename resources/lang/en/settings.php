<?php
return[
    'set_settings_success'=>'settings have been made successfully',
    'set_settings_fail'=>'settings cant be saved',
    'set_privacy_success'=>'account is private now',
    'set_privacy_fail'=>'error in making account private',
    'setting_details'=>'your settings are',
    'settings_details_none'=>'cant retreive setting details',
    'set_notif_setting_success'=>'notification settings has been set successfully',
    'set_notif_setting_fail'=>'sorry settings not being saved'
    
    ]
;