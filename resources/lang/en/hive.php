<?php

return [

    'added_success' => 'contact has been added to hive successfully',
    'added_fail' => 'sorry :( contact cannot be added to hive',
    'removed_success' => 'contact has been removed from hive successfully ',
    'removed_fail' => 'sorry :( contact cannot be removed to hive',
    'get_hive_success' => 'got all hive members successfully',
    'get_hive_user_not_exists' => 'User not found!',
    'get_hive_fail' => 'sorry :( no hive members in your hive',
    'hive_created_success' => 'congrats hive created successfully',
    'hive_created_fail' => 'sorry :( hive creation failed',
    'nouserfound'  => 'user you trying to add doesnt exist',
    'empty_hive' => 'hive members removed successfully and NOW Hive is empty',
    'assign_hive_rank_success' => 'hive rank assigned successfully',
    'assign_hive_rank_fail' => 'sorry :( hive rank cannot be assigned ho hive member',
    'hive_not_exist' => 'user does nt belongs to any hive',
    'hive_notify_success' => 'Hive Request Sent Successfully',
    'not_in_hive' => 'this hive member does not belong to your hive',
    'cant_add_himself' => 'you cant add yourself in hive',
    'search_success' => 'search result found',
    'search_fail' => 'search result not found',
    'no_user_hive' => 'no hive belongs to user',
    'no_user' => 'user doesnt exist',
    'search_result_found' => 'search result found',
    'hive_notify_fail' => 'notification cant be sent',
    'cant_get_hive'=>'cant get hive of this user',
    'declined_users'=>'user have been declined'

];
